# API Cooperapp

Api para a Cooperativa!

## ENDPOINTS

**Obs: todas as rotas contém api**

- Método Get

**GET**

```
http://localhost:8000/api/[parametros]
```

```
GET api/logout -> saída do usuário

GET api/pontos -> retorna todos os pontos entre as linhas

GET api/linhas -> retorna todas as linhas cadastradas

GET api/linha/:id -> retorna uma linha

GET api/horario/:id -> retorna um horário

GET api/itinerario/:id -> retorna um itinerário
```

**Consulta com sucesso**

BUSCA PELO ID

**GET** ```http://localhost:8000/api/linhas```

```
[
  {
    "id": 1,
    "nome_linha": "AMONTADA"
  }
]
```


---

- Método POST

```
POST api/register -> cria o login do usuário

POST api/login -> loga o usuário

POST api/empresa -> cria uma empresa

POST api/horario -> cria um novo horário

POST api/itinerario -> cria um novo itinerário

POST api/cooperado -> cria um novo cooperado
```

**Consulta com sucesso**

CRIAR USUARIO

**POST** ```http://localhost:8000/api/register```

### QUERY

```
{
    "nome_completo": "Renan Pereira Da Silva",
	"email": "renanmol@gmail.com",
	"CPF": "876543210912",
	"telefone": "(85)40028922",
	"endereco": "Rua tampico",
	"senha": "Renan##109878@@"
}
```
