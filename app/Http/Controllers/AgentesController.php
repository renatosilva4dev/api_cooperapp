<?php

namespace App\Http\Controllers;

use App\Models\Agencias;
use App\Models\Linha;
use App\Models\PagamentoAgente;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AgentesController extends Controller
{
    //

    public function indexAdm(){
        try{
          $agencias = Agencias::where('empresa_id', Auth::user()->empresa_id)->get();
    
          return view('admin.agente.manter', compact('agencias'));
        }catch(\Exception $erro){
          dd($erro);
          return response()->json($erro, 500);
        }
      }
    
      public function formcadastro(){
        try{

          return view('admin.agente.cadastrar');

        }catch(\Exception $erro){
          dd($erro);
          return response()->json($erro, 500);
        }
      }
    
      public function save(Request $request){
        try{
    
          Agencias::create([
            'nome' => $request->nome,
            'endereço' => $request->endereço,
            'saldo' => 0,
            'empresa_id' => Auth::user()->empresa_id
          ]);
    
          return redirect()->route('admin.home');
        }catch(\Exception $erro){
          dd($erro);
          return response()->json($erro, 500);
        }
      }
    
      public function edit($id){
        try{
          $agente = Agencias::find($id);
          return view('admin.agente.editar', compact('agente'));
        }catch(\Exception $error){
            return response()->json($error, 500);
        }
      }
    
      public function update(Request $request){
        try{
    
          $agencia = Agencias::find($request->agente_id);
    
          $agencia->update([
            'nome' => $request->nome,
            'endereço' => $request->endereço
          ]);
    
          return redirect()->route('admin.home');
        }catch(\Exception $erro){
          dd($erro);
          return response()->json($erro, 500);
        }
      }
    
      public function delete($id){
        try{
          $agente = Agencias::find($id);
    
          $agente->delete();
    
          return response()->json(['text' => "Agente excluido!", "title" => "Sucesso!", "class"=>'success']);
    
        }catch(\Exception $erro){
          return response()->json($erro, 500);
        }
      }
    
      public function usuarios($id){
        try{
          $agente = Agencias::find($id);
    
          $usuarios = User::where('agencia_id', $agente->id)->get();
    
          
          return view('admin.agente.usuarios.index', compact('usuarios', 'agente'));
        }catch(\Exception $erro){
          dd($erro);
          return response()->json($erro, 500);
        }
      }
    
      public function form_usuarios($id){
        try{
          $agencia = Agencias::find($id);
    
          return view('admin.agente.usuarios.form', compact('agencia'));
        }catch(\Exception $error){
          dd($error);
          return response()->json($error, 500);
        }
      }
    
      public function saveuser(Request $request){
        try{
            
          User::create([
            'nome_completo' => $request->nome,
            'email' => $request->email,
            'CPF' => $request->cpf,
            'telefone' => 'ag',
            'endereco' => 'ag', 
            'cidade' => 'ag',
            'senha' => bcrypt($request->senha),
            'tipo' => 2,
            'saldo' => 0,
            'saldo_cashback' => 0,
            'empresa_id' => Auth::user()->empresa_id,
            'api_token' => hash('sha256', $request->email),
            'cep' => 'ag',
            'bairro' => 'ag',
            'data_nascimento' => '2001-01-01',
            'aceite_termos' => 1,
            'agencia_id' => $request->agente_id
          ]);
    
          return redirect()->route('admin.home');
        }
        catch(\Exception $error){
          dd($error);
        }
      }
    
      public function edituser($id){
        try{
          $usuario = User::find($id);
          return view('admin.agente.usuarios.edit', compact('usuario'));
        }catch(\Exception $error){
    
        }
      }
    
      public function updateuser(Request $request){
        try{
    
          $usuario = User::find($request->user_id);
    
          $usuario->update([
            'nome_completo' => $request->nome,
            'email' => $request->email,
            'cpf' => $request->cpf
          ]);
    
          return redirect()->route('admin.home');
        }catch(\Exception $erro){
          dd($erro);
          return response()->json($erro, 500);
        }
      }
    
      public function deleteuser($id){
        try{
          $user = User::find($id);
    
          $user->delete();
    
          return response()->json(['text' => "Usuário excluido!", "title" => "Sucesso!", "class"=>'success']);
    
        }catch(\Exception $erro){
          return response()->json($erro, 500);
        }
      }
    
      public function pay($id){
        try{
          $agencia = Agencias::find($id);
    
          return view('admin.agente.pagar', compact('agencia'));
        }catch(\Exception $error){
    
        }
      }
    
      public function confirm_pay(Request $request){
        try{
          $agencia = Agencias::find($request->cooperado_id);
    
          $saldo = $agencia->saldo - $request->valor;
    
          $agencia->update([
            'saldo' => $saldo
          ]);

          PagamentoAgente::create([
            'valor' => $request->valor,
            'agente_id' => $agencia->id,
            'data' => date('Y-m-d')
          ]);
    
          return redirect()->route('admin.home');
        }catch(\Exception $error){
          dd($error);
        }
      }

      public function RelatorioPagamenoIndex(){
        try{
          $agentes = Agencias::where('empresa_id', Auth::user()->empresa_id)->get();
    
          return view('admin.relatorios.agentes.pagamentos.index', compact('agentes'));
    
        }catch(\Exception $error){
          return response()->json(['errors' => ['exception'=> 'Houve um erro inesperado, entre em contato com o suporte']], 422);
        }
      }
    
      public function RelatorioPagamentoConsulta(Request $request){
        try{
    
          $request->data_in = date('Y-m-d', strtotime(str_replace("/", "-",$request->data_in)));
          $request->data_fi = date('Y-m-d', strtotime(str_replace("/", "-",$request->data_fi)));
    
          if($request->agente_id == "0"){
            $agente = "todos";
            $pagamentos = PagamentoAgente::whereBetween("data", [$request->data_in, $request->data_fi])
                                    ->get();
          }else{
            $agente = Agencias::find($request->agente_id);
    
            $pagamentos = PagamentoAgente::where("agente_id", $agente->id)
                                            ->whereBetween("data", [$request->data_in, $request->data_fi])
                                            ->get();
          }
    
          $data_in = date('d/m/Y', strtotime($request->data_in));
          $data_fi = date('d/m/Y', strtotime($request->data_fi));
    
          $tipo = "Relatório de pagamentos por agente";
    
          return view('admin.relatorios.agentes.pagamentos.relatorio', compact('agente', 'data_in', 'data_fi', 'tipo', 'pagamentos'));
        }catch(\Exception $error){
          return response()->json(['errors' => ['exception'=> 'Houve um erro inesperado, entre em contato com o suporte']], 422);
        }
      }
    
}
