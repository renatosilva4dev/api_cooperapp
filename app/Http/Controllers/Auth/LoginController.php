<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
  public $rules = [
    'CPF' => 'required|min:11|max:11',
    'senha' => 'required',
  ];

  public $messages = [
    'CPF.required' => 'Insira um CPF válido.',
    'senha.required' => 'Insira sua senha',
  ];

  public function login(Request $request)
  {
    try {
      $users = User::where('email', $request->email)->get();

      if($users->count() == 0){
        return response()->json("user_not_found");
      }

      foreach($users as $user){
        if(!empty($user)){
          $senha_user = $user->senha;
          $senha_recebida = $request->senha;
          if(Hash::check($senha_recebida, $senha_user)){
            $id = $user->id;
            $nome_completo = $user->nome_completo;
            return response()->json($user, 200);
          }
          else{
            return response()->json("wrong_password");
          }
        }
      }
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function loginAdmView(){
    $erro_user = false;
    $erro_senha = false;
    return view('admin.login', compact('erro_user', 'erro_senha'));
  }

  public function loginAdm(Request $request)
  {
    try {
      $users = User::where('email', $request->email)->get();
      $erro_senha = false;
      $erro_user = false;

      if($users->count() == 0){
        $erro_user = true;
        return view('admin.login', compact('erro_user', 'erro_senha'));
      }

      foreach($users as $user){
        if(!empty($user)){
          $senha_user = $user->senha;
          $senha_recebida = $request->senha;
          if(Hash::check($senha_recebida, $senha_user)){
            Auth::login($user);
            return redirect('/');
          }
          else{
            $erro_senha = true;
            return view('admin.login', compact('erro_user', 'erro_senha'));
          }
        }
      }
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  
  public function logoutAdmin(){
    try{
      Auth::logout();
      return redirect('/admin/login');
    }
    catch(\Exception $error){
      dd($error);
    }
  }
}

