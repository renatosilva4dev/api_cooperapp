<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Cooperados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginCooperadoController extends Controller
{
  public $rules = [
    'nome' => 'required|min:15|max:100',
  ];

  public $messages = [
    'nome.required' => 'Insira um nome válido.',
  ];

  public function login(Request $request)
  {
    $request->validate($this->rules, $this->messages);
    try {
      $nome = Cooperados::where('nome', $request->nome);
      if (empty($nome)) {
        return response()->json(["nome inválido ou não cadastrado."], 401);
      }

      $credentials = ["nome" => $nome->nome];

      if (Auth::attempt($credentials)) {
        return response()->json(["Logado!"], 200);
      } else {
        return response()->json(["Algo deu errado."], 400);
      }
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }
}
