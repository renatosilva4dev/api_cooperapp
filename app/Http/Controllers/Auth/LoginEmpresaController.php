<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginEmpresaController extends Controller
{
  public $rules = [
    'cnpj' => 'required|min:14|max:14',
  ];

  public $messages = [
    'cnpj.required' => 'Insira um cnpj válido.',
  ];

  public function login(Request $request)
  {
    $request->validate($this->rules, $this->messages);
    try {
      $cnpj = Empresa::where('cnpj', $request->cnpj);
      if (empty($cnpj)) {
        return response()->json(["cnpj inválido ou não cadastrado."], 401);
      }

      $credentials = ["cnpj" => $cnpj->cnpj, "senha" => $request->senha];

      if (Auth::attempt($credentials)) {
        return response()->json(["Logado!"], 200);
      } else {
        return response()->json(["Algo deu errado."], 400);
      }
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }
}
