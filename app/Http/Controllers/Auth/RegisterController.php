<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
class RegisterController extends Controller
{
  public $validations = [
    'nome_completo' => 'required|min:12|max:255',
    'email' => 'required|unique:users,email',
    'CPF' => 'required|min:11|unique:users,CPF',
    'telefone' => 'required|min:6|max:20',
    'endereco' => 'required|min:7|max:30',
    'senha' => 'required|confirmed|min:6',
    'senha_confirmacao' => 'required',
  ];

  public $messages = [
    'nome_completo.required' => 'Informe seu nome completo.',
    'nome_completo.max' => 'Número máximo de caracteres permitido é 255.',
    'email.required' => 'Informe o seu e-mail.',
    'email.unique' => 'Ops! Este e-mail já foi cadastrado.',
    'CPF.unique' => 'Ops! Seu CPF já foi utilizado.',
    'senha.required' => 'Informe uma senha.',
    'senha.confirmed' => 'As senhas não são iguais.',
    'senha_confirmacao.required' => 'informe uma senha.',
    'senha.min' => 'A senha deve conter pelo menos 6 caracteres.',
    'telefone.required' => 'Por favor, informe seu telefone'
  ];

  public function register(Request $request)
  {
    $request->validate($this->messages, $this->validations);
    try {
      $data = new User();
      $data = $request->all();
      $data['senha'] = bcrypt($data['senha']);

      User::create($data);

      return response()->json(['success' => true], 201);
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }
}
