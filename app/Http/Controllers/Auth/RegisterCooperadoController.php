<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Cooperados;
use Illuminate\Http\Request;

class RegisterCooperadoController extends Controller
{
  public $validations = [
    'nome' => 'required|min:12|max:255',
    'email' => 'required|unique:users,email',
    'telefone' => 'required|min:6|max:20',
    'saldo' => 'required|confirmed|min:6',
  ];

  public $messages = [
    'nome_completo.required' => 'Informe seu nome completo.',
    'nome_completo.max' => 'Número máximo de caracteres permitido é 255.',
    'saldo.required' => 'Informe um saldo.',
    'telefone.required' => 'Por favor, informe seu telefone'
  ];

  public function register(Request $request)
  {
    $request->validate($this->messages, $this->validations);
    try {
      $data = new Cooperados();
      $data = $request->all();
      Cooperados::create($data);

      return response()->json(['success' => true], 201);
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }
}
