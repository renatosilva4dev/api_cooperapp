<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Empresa;
use Illuminate\Http\Request;
class RegisterEmpresaController extends Controller
{
  public $validations = [
    'nome' => 'required|min:12|max:255',
    'cnpj' => 'required|min:11|unique:empresas,cnpj',
    'telefone' => 'required|min:6|max:20',
    'endereco' => 'required|min:7|max:30',
  ];

  public $messages = [
    'nome.required' => 'Informe seu nome completo.',
    'nome.max' => 'Número máximo de caracteres permitido é 255.',
    'cnpj.unique' => 'Ops! Seu cnpj já foi utilizado.',
    'cnpj.min' => 'Ops! Insira um cnpj válido.',
    'telefone.required' => 'Por favor, informe seu telefone'
  ];

  public function register(Request $request)
  {
    $request->validate($this->messages, $this->validations);
    try {
      $data = new Empresa();
      $data = $request->all();

      Empresa::create($data);

      return response()->json(['success' => true], 201);
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }
}
