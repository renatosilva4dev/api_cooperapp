<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsuarioController extends Controller
{
  public function index()
  {
    try {
      $user = User::all();

      return response()->json($user);
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }
  
  public function form()
  {
    try{
      $emails = User::pluck('email');

      $aviso_email = '0';
      $aviso_cpf = '0';
      return view('user.form', compact('aviso_email', 'aviso_cpf', 'emails'));
    }
    catch (\Exception $error){
      return response()->json($error, 400);
    }
  }

  public function create(Request $request){
    try{
      
      $user = User::where('email', $request->email)->first();

      if(!empty($user)){
        $aviso_email = "Email já cadastrado";
        return view('user.form', compact('aviso_email'));
      }

      $data_nascimento = date('Y-m-d', strtotime(str_replace("/", "-", $request->data_nascimento)));


      if($request->termos == "on"){
        $aceite = 1;
      }

      User::create([
        'nome_completo' => $request->nome . ' ' . $request->sobrenome,
        'email' => $request->email,
        'CPF' => $request->cpf,
        'telefone' => $request->telefone,
        'endereco' => $request->rua . ' ' . $request->casa, 
        'cidade' => $request->cidade,
        'senha' => bcrypt($request->senha),
        'tipo' => 0,
        'saldo' => 0,
        'saldo_cashback' => 0,
        'empresa_id' => 1,
        'api_token' => hash('sha256', $request->email),
        'cep' => $request->cep,
        'bairro' => $request->bairro,
        'data_nascimento' => $data_nascimento,
        'aceite_termos' => $aceite,
      ]);

      return view('user.status');
    }
    catch(\Exception $error){
      dd($error);
    }
  }

  public function find($id)
  {
    try{
      $user = User::find($id);

      return response()->json($user);
    }
    catch (\Exception $error){
      return response()->json($error, 400);
    }
  }

  public function indexAdm(){
    try {
      $users = User::where('tipo', 3)->where('empresa_id', Auth::user()->empresa_id)->get();

      return view('admin.users.index', compact('users'));
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function formAdm(){
    return view('admin.users.form');
  }

  public function saveuser(Request $request){
    try{

      User::create([
        'nome_completo' => $request->nome,
        'email' => $request->email,
        'CPF' => $request->cpf,
        'telefone' => 'adm',
        'endereco' => 'adm', 
        'cidade' => 'adm',
        'senha' => bcrypt($request->senha),
        'tipo' => 3,
        'saldo' => 0,
        'saldo_cashback' => 0,
        'empresa_id' => Auth::user()->empresa_id,
        'api_token' => hash('sha256', $request->email),
        'cep' => 'adm',
        'bairro' => 'adm',
        'data_nascimento' => '2001-01-01',
        'aceite_termos' => 1
      ]);

      return redirect()->route('admin.home');
    }
    catch(\Exception $error){
      dd($error);
    }
  }

  public function edituser($id){
    try{
      $user = User::find($id);
      return view('admin.users.edit', compact('user'));
    }catch(\Exception $error){

    }
  }

  public function updateuser(Request $request){
    try{

      $usuario = User::find($request->user_id);

      $usuario->update([
        'nome_completo' => $request->nome,
        'email' => $request->email,
        'cpf' => $request->cpf
      ]);

      return redirect()->route('admin.home');
    }catch(\Exception $erro){
      dd($erro);
      return response()->json($erro, 500);
    }
  }

  public function deleteuser($id){
    try{
      $user = User::find($id);

      $user->delete();

      return response()->json(['text' => "Usuário excluido!", "title" => "Sucesso!", "class"=>'success']);

    }catch(\Exception $erro){
      return response()->json($erro, 500);
    }
  }
}
