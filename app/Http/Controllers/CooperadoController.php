<?php

namespace App\Http\Controllers;

use App\Models\Cooperados;
use App\Models\Linha;
use App\Models\PagamentoCooperado;
use App\Models\Passagens;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CooperadoController extends Controller
{
  public function index()
  {
    try {
      $cooperados = Cooperados::all();

      return response()->json($cooperados);
    } catch (\Exception $error) {
      return response()->json($error, 500);
    }
  }

  public function find($id)
  {
    try{
      $cooperado = Cooperados::find($id);

      $passagens = Passagens::where('cooperado_id', $cooperado->id)->where('utilizacao', date('Y-m-d'))->count();


      $cooperado->passagens_hoje = $passagens;

      return response()->json($cooperado);

    }catch(\Exception $error){
      return response()->json($error, 500);
    }
  }

  public function indexAdm(){
    try{
      $cooperados = Cooperados::where('empresa_id', Auth::user()->empresa_id)->get();

      return view('admin.cooperado.manter', compact('cooperados'));
    }catch(\Exception $erro){
      dd($erro);
      return response()->json($erro, 500);
    }
  }

  public function formcadastro(){
    try{
      $linhas = Linha::where('empresa_id', Auth::user()->empresa_id)->get();

      return view('admin.cooperado.cadastrar', compact('linhas'));
    }catch(\Exception $erro){
      dd($erro);
      return response()->json($erro, 500);
    }
  }

  public function save(Request $request){
    try{

      $cooperado = Cooperados::create([
        'nome' => $request->nome,
        'matricula' => $request->matricula,
        'telefone' => $request->telefone,
        'saldo' => 0,
        'empresa_id' => Auth::user()->empresa_id,
        'linha_id' => $request->linha_id 
      ]);

      User::create([
        'nome_completo' => $cooperado->nome,
        'email' => $cooperado->matricula . '@cooperita.com.br',
        'CPF' => $cooperado->matricula,
        'telefone' => 'coop',
        'endereco' => 'coop', 
        'cidade' => 'coop',
        'senha' => bcrypt($cooperado->matricula . 'coop'),
        'tipo' => 4,
        'saldo' => 0,
        'saldo_cashback' => 0,
        'empresa_id' => Auth::user()->empresa_id,
        'api_token' => hash('sha256', $cooperado->matricula . '@cooperita.com.br'),
        'cep' => 'coop',
        'bairro' => 'coop',
        'data_nascimento' => '2001-01-01',
        'aceite_termos' => 1,
        'cooperado_id' => $cooperado->id
      ]);

      return redirect()->route('admin.home');
    }catch(\Exception $erro){
      dd($erro);
      return response()->json($erro, 500);
    }
  }

  public function edit($id){
    try{
      $cooperado = Cooperados::find($id);
      $linhas = Linha::all();
      return view('admin.cooperado.editar', compact('cooperado','linhas'));
    }catch(\Exception $error){

    }
  }

  public function update(Request $request){
    try{

      $cooperado = Cooperados::find($request->cooperado_id);

      $cooperado->update([
        'nome' => $request->nome,
        'matricula' => $request->matricula,
        'telefone' => $request->telefone,
        'linha_id' => $request->linha_id 
      ]);

      return redirect()->route('admin.home');
    }catch(\Exception $erro){
      dd($erro);
      return response()->json($erro, 500);
    }
  }

  public function delete($id){
    try{
      $cooperado = Cooperados::find($id);

      $cooperado->delete();

      return response()->json(['text' => "Cooperado excluido!", "title" => "Sucesso!", "class"=>'success']);

    }catch(\Exception $erro){
      return response()->json($erro, 500);
    }
  }

  public function usuarios($id){
    try{
      $cooperado = Cooperados::find($id);

      $usuarios = User::where('cooperado_id', $cooperado->id)->get();


      return view('admin.cooperado.usuarios.index', compact('usuarios', 'cooperado'));
    }catch(\Exception $erro){
      dd($erro);
      return response()->json($erro, 500);
    }
  }

  public function form_usuarios($id){
    try{
      $cooperado = Cooperados::find($id);

      return view('admin.cooperado.usuarios.form', compact('cooperado'));
    }catch(\Exception $error){
      dd($error);
      return response()->json($error, 500);
    }
  }

  public function saveuser(Request $request){
    try{

      User::create([
        'nome_completo' => $request->nome,
        'email' => $request->email,
        'CPF' => $request->cpf,
        'telefone' => 'coop',
        'endereco' => 'coop', 
        'cidade' => 'coop',
        'senha' => bcrypt($request->senha),
        'tipo' => 1,
        'saldo' => 0,
        'saldo_cashback' => 0,
        'empresa_id' => Auth::user()->empresa_id,
        'api_token' => hash('sha256', $request->email),
        'cep' => 'coop',
        'bairro' => 'coop',
        'data_nascimento' => '2001-01-01',
        'aceite_termos' => 1,
        'cooperado_id' => $request->cooperado_id
      ]);

      return redirect()->route('admin.home');
    }
    catch(\Exception $error){
      dd($error);
    }
  }

  public function edituser($id){
    try{
      $usuario = User::find($id);
      return view('admin.cooperado.usuarios.edit', compact('usuario'));
    }catch(\Exception $error){

    }
  }

  public function updateuser(Request $request){
    try{

      $usuario = User::find($request->user_id);

      $usuario->update([
        'nome_completo' => $request->nome,
        'email' => $request->email,
        'cpf' => $request->cpf
      ]);

      return redirect()->route('admin.home');
    }catch(\Exception $erro){
      dd($erro);
      return response()->json($erro, 500);
    }
  }

  public function deleteuser($id){
    try{
      $user = User::find($id);

      $user->delete();

      return response()->json(['text' => "Usuário excluido!", "title" => "Sucesso!", "class"=>'success']);

    }catch(\Exception $erro){
      return response()->json($erro, 500);
    }
  }

  public function pay($id){
    try{
      $cooperado = Cooperados::find($id);

      return view('admin.cooperado.pagar', compact('cooperado'));
    }catch(\Exception $error){

    }
  }

  public function confirm_pay(Request $request){
    try{
      $cooperado = Cooperados::find($request->cooperado_id);

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL,"https://coopersoft.net.br/api/pagar_cooperado");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,
                "data_pag=". date('d/m/Y'). 
                "&vencimento=" . date('d/m/Y') .
                "&valor=". $request->valor .
                "&nome_cooperado=". $cooperado->nome .
                "&observacao=");

      // In real life you should use something like:
      // curl_setopt($ch, CURLOPT_POSTFIELDS, 
      //          http_build_query(array('postvar1' => 'value1')));

      // Receive server response ...
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      $resposta = json_decode(curl_exec($ch), true);

      curl_close ($ch);

      if($resposta['class'] == "success"){
        $saldo = $cooperado->saldo - $request->valor;

        $cooperado->update([
          'saldo' => $saldo
        ]);

        PagamentoCooperado::create([
          'valor' => $request->valor,
          'cooperado_id' => $cooperado->id,
          'data' => date('Y-m-d')
        ]);

        return redirect()->route('admin.home');
      }
      else{
        return response()->json(['errors' => ['exception'=> 'Houve um erro inesperado, entre em contato com o suporte']], 422);
      }
    }catch(\Exception $error){
      dd($error);
    }
  }

  public function RelatorioPagamenoIndex(){
    try{
      $cooperados = Cooperados::where('empresa_id', Auth::user()->empresa_id)->get();

      return view('admin.relatorios.cooperados.pagamentos.index', compact('cooperados'));

    }catch(\Exception $error){
      return response()->json(['errors' => ['exception'=> 'Houve um erro inesperado, entre em contato com o suporte']], 422);
    }
  }

  public function RelatorioPagamentoConsulta(Request $request){
    try{

      $request->data_in = date('Y-m-d', strtotime(str_replace("/", "-",$request->data_in)));
      $request->data_fi = date('Y-m-d', strtotime(str_replace("/", "-",$request->data_fi)));

      if($request->cooperado_id == "0"){
        $cooperado = "todos";
        $pagamentos = PagamentoCooperado::whereBetween("data", [$request->data_in, $request->data_fi])
                                ->get();
      }else{
        $cooperado = Cooperados::find($request->cooperado_id);

        $pagamentos = PagamentoCooperado::where("cooperado_id", $cooperado->id)
                                        ->whereBetween("data", [$request->data_in, $request->data_fi])
                                        ->get();
      }

      $data_in = date('d/m/Y', strtotime($request->data_in));
      $data_fi = date('d/m/Y', strtotime($request->data_fi));

      $tipo = "Relatório de pagamentos por cooperado";

      return view('admin.relatorios.cooperados.pagamentos.relatorio', compact('cooperado', 'data_in', 'data_fi', 'tipo', 'pagamentos'));
    }catch(\Exception $error){
      return response()->json(['errors' => ['exception'=> 'Houve um erro inesperado, entre em contato com o suporte']], 422);
    }
  }

  

}
