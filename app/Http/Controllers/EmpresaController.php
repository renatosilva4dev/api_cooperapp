<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{
  public function index()
  {
    try {
      $empresas = Empresa::all();

      return response()->json($empresas);
    } catch (\Exception $error) {
      return response()->json($error, 500);
    }
  }
}
