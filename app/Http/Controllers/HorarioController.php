<?php

namespace App\Http\Controllers;

use App\Models\Horario;
use App\Models\Linha;
use Illuminate\Http\Request;

class HorarioController extends Controller
{
  public function store(Request $request)
  {
    try {
      Horario::create([
        'linha_id' => $request->linha_id,
        'horario_chegada' => $request->horario_chegada,
        'horario_saida' => $request->horario_saida
      ]);

      return response()->json(['success' => true], 201);
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function index($id)
  {
    try {
      $linha = Linha::find($id);
      $horario = $linha->horarios;
      return $horario;
    } catch (\Exception $error) {
      return response()->json($error, 500);
    }
  }

  public function view($id)
  {
    try{
      $horario = Horario::find($id);
      return response()->json($horario);
    }
    catch(\Exception $error){
      return response()->json($error, 500);
    }
  }
}
