<?php

namespace App\Http\Controllers;

use App\Models\Linha;
use App\Models\Pontos;
use App\Models\Seccionamento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LinhasController extends Controller
{
  public function index()
  {
    $linhas = Linha::all('id', 'nome_linha', 'empresa_id');

    return response()->json($linhas);
  }

  public function store(Request $request)
  {
    try {
      $linha = new Linha();
      Linha::create([
        'empresa_id' => $request->empresa_id,
        'nome_linha' => $request->nome_linha
      ]);
      return response()->json(["success" => true], 201);
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function show($id)
  {
    $linha = Linha::find($id);
    return $linha;
  }

  public function destroy($id)
  {
    try {
      $linha = Linha::find($id);
      $linha->delete();
      return response()->json(['success' => true], 200);
    } catch (\Exception $error) {
      return response()->json($error, 500);
    }
  }

  public function indexAdm(){
    try{
      $linhas = Linha::where('empresa_id', Auth::user()->empresa_id)->get();

      return view('admin.linhas.index', compact('linhas'));
    }catch(\Exception $error){
      return response()->json($error, 500);
    }
  }

  public function form(){
    return view('admin.linhas.form');
  }

  public function save(Request $request){
    try {

      Linha::create([
        'nome_linha' => $request->nome,
        'grupo' => $request->grupo,
        'empresa_id' => Auth::user()->empresa_id
      ]);

      return redirect()->route('admin.home');

    }catch (\Exception $error){
      return response()->json($error, 500);
    }
  }

  public function edit($id){
    try{
      $linha = Linha::find($id);

      return view('admin.linhas.edit', compact('linha'));
    }catch(\Exception $error){
      return response()->json($error, 500);
    }
  }

  public function update(Request $request){
    try {
      $linha = Linha::find($request->linha_id);

      $linha->update([
        'nome_linha' => $request->nome,
        'grupo' => $request->grupo
      ]);

      return redirect()->route('admin.home');

    } catch (\Exception $error) {
        return response()->json($error, 500);
    }
  }

  public function delete($id){
    try{
      $linha = Linha::find($id);

      $linha->delete();

      return response()->json(['text' => "Linha excluida!", "title" => "Sucesso!", "class"=>'success']);


    }catch(\Exception $error){
      return response()->json($error, 500);
    }
  }

  public function seccionamentos($id){
    try{
      $linha = Linha::find($id);

      $seccionamentos = Seccionamento::where('linha_id', $linha->id)->get();


      return view('admin.linhas.seccionamentos.index', compact('linha', 'seccionamentos'));
    }catch(\Exception $erro){
      dd($erro);
      return response()->json($erro, 500);
    }
  }

  public function form_seccionamentos($id){
    try{
      $linha = Linha::find($id);
      $pontos = Pontos::all();

      return view('admin.linhas.seccionamentos.form', compact('linha', 'pontos'));
    }catch(\Exception $error){
      dd($error);
      return response()->json($error, 500);
    }
  }

  public function saveseccionamento(Request $request){
    try{

      Seccionamento::create([
        'valor_passagem' => $request->valor_passagem,
        'linha_id' => $request->linha_id,
        'id_ponto_saida' => $request->ponto_saida,
        'id_ponto_chegada' => $request->ponto_chegada,
        'sentido' => $request->sentido
      ]);

      return redirect()->route('admin.home');
    }
    catch(\Exception $error){
      dd($error);
    }
  }

  public function editseccionamento($id){
    try{
      $seccionamento = Seccionamento::find($id);
      $pontos = Pontos::all();

      return view('admin.linhas.seccionamentos.edit', compact('seccionamento', 'pontos'));
    }catch(\Exception $error){

    }
  }

  public function updateseccionamento(Request $request){
    try{

      $seccionamento = Seccionamento::find($request->seccionamento_id);

      $seccionamento->update([ 
        'valor_passagem' => $request->valor_passagem,
        'id_ponto_saida' => $request->ponto_saida,
        'id_ponto_chegada' => $request->ponto_chegada,
        'sentido' => $request->sentido
      ]);

      return redirect()->route('admin.home');
    }catch(\Exception $erro){
      dd($erro);
      return response()->json($erro, 500);
    }
  }

  public function deleteseccionamento($id){
    try{
      $seccionamento = Seccionamento::find($id);

      $seccionamento->delete();

      return response()->json(['text' => "Seccionamento excluido!", "title" => "Sucesso!", "class"=>'success']);

    }catch(\Exception $erro){
      return response()->json($erro, 500);
    }
  }

}
