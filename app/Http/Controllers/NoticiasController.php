<?php

namespace App\Http\Controllers;

use App\Models\Noticias;
use Illuminate\Http\Request;

class NoticiasController extends Controller
{
  public function index()
  {
    $noticias = Noticias::all();

    return response()->json($noticias);
  }

  public function store(Request $request)
  {
    try {
      $noticia = new Noticias();
      $noticia->titulo_noticia = $request->titulo_noticia;
      $noticia->corpo_noticia = $request->corpo_noticia;
      $noticia->publicado = $request->publicado;
      $noticia->save();
      return response()->json(['success' => true], 201);
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function show($id)
  {
    $noticia = Noticias::find($id);
    return $noticia;
  }

  public function destroy($id)
  {
    try {
      $noticia = Noticias::find($id);
      $noticia->delete();
      return response()->json(['destroy noticia success' => true], 200);
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }
}
