<?php

namespace App\Http\Controllers;

use App\Models\Cooperados;
use App\Models\Linha;
use App\Models\PagamentosDiretos;
use App\Models\Seccionamento;
use App\Models\User;
use Illuminate\Http\Request;

class PagamentosDiretosController extends Controller
{
    //

    public function index($id, Request $request){
        try{
            $user = User::find($id);
            $cooperado = Cooperados::find($request->cooperado_id);
            $linha = Linha::where('id', $cooperado->linha_id)->first();
            return view('pagamentosDiretos.index', compact('user', 'cooperado', 'linha'));
        }catch(\Exception $error){
            dd($error);
        }
        
    }

    public function checkout(Request $request){
        try{
            $user = User::find($request->user_id);
            $cooperado = Cooperados::find($request->cooperado_id);
            $seccionamento = Seccionamento::find($request->seccionamento_id);
            
            return view('pagamentosDiretos.next', compact('user', 'cooperado', 'seccionamento'));
        }catch(\Exception $error){
            dd($error);
        }
        
    }

    public function save(Request $request){
        try{
            $user = User::find($request->user_id);
            $cooperado = Cooperados::find($request->cooperado_id);
            $saldo_final_user = $user->saldo - $request->valor;
            $saldo_final_cooperado = $cooperado->saldo + ($request->valor * 0.9);

            PagamentosDiretos::create([
                'valor' => $request->valor,
                'user_id' => $user->id,
                'cooperado_id' => $cooperado->id,
                'data' => $request->data
            ]);

            $user->update([
                'saldo' => $saldo_final_user
            ]);

            $cooperado->update([
                'saldo' => $saldo_final_cooperado
            ]);

            return view('pagamentosDiretos.status');
        }catch(\Exception $error){
            dd($error);
        }
        
    }
}
