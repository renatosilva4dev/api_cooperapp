<?php

namespace App\Http\Controllers;

use App\Models\Agencias;
use App\Models\Cooperados;
use App\Models\Horario;
use App\Models\Linha;
use App\Models\Passagens;
use App\Models\Pontos;
use App\Models\Seccionamento;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class PassagensController extends Controller
{
  public function payment(Request $request){
    try {

      $seccionamento = Seccionamento::find($request->seccionamento_id);
      $pessoa = ["nome" => $request->nome_passageiro, "documento" => $request->doc_passageiro];
      $ponto_saida = Pontos::find($seccionamento->id_ponto_saida);
      $ponto_chegada = Pontos::find($seccionamento->id_ponto_chegada);
      $nome_passagem = $ponto_saida->nome . ' - '. $ponto_chegada->nome;
      $emissao = date('Y-m-d');
      $user = User::find($request->user_id);
      $data_viagem = date('Y-m-d', strtotime(str_replace("/", "-", $request->data_viagem)));
      $data_original = $request->data_viagem;
      $horario = Horario::find($request->horario_id);

      if($request->meia == 1){
        $meia_passagem=1;
        $meia=true;
      }else{
        $meia_passagem=0;
        $meia=false;
      }

      $passagem = Passagens::create([
        'data_viagem' => $data_viagem,
        'emissao' => $emissao,
        'nome_passageiro' => strtoupper($request->nome_passageiro),
        'doc_passageiro' => $request->doc_passageiro,
        'nome_passagem' => $nome_passagem,
        'valor' => $request->valor,
        'horario_id' => $request->horario_id,
        'status' => 0,
        'meia_passagem' => $meia_passagem,
        'linha_id' => $request->linha_id,
        'empresa_id' => $user->empresa_id,
        'seccionamento_id' => $request->seccionamento_id,
        'user_id' => $user->id,
        'cashback' => 0

      ]);

      return view('passagens.formcheckout', compact('seccionamento', 'pessoa', 'user', 'data_original', 'horario', 'passagem', 'meia' ));

    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function index($id){
    try {
      $passagens = Passagens::where('user_id', $id)->get();
      foreach($passagens as $passagem){
        $passagem->data_viagem = date('d/m/Y', strtotime($passagem->data_viagem));
        $passagem->emissao = date('d/m/Y', strtotime($passagem->emissao));

      }

      return response()->json($passagens, 200);
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }
  

  public function show($id){
    try {
      $passagem = Passagens::find($id);
      $passagem->data_viagem = date('d/m/Y', strtotime($passagem->data_viagem));
      $passagem->emissao = date('d/m/Y', strtotime($passagem->emissao));
      return response()->json($passagem);
    } catch (\Exception $error) {
	dd($error);
      return response()->json($error, 500);
    }
  }

  public function form($user_id){
    try {
      $linhas = Linha::all();
      $user = User::find($user_id);
      return view('passagens.forminit', compact('user', 'linhas'));
    }
    catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function formstep(Request $request){
    try {
      $seccionamento = Seccionamento::find($request->seccionamento_id);
      $pessoa = ["nome" => $request->nome_completo, "documento" => $request->doc_passageiro];
      $data_viagem = date('m/d/Y', strtotime(str_replace("/", "-", $request->data_viagem)));
      $dia = date('D', strtotime($data_viagem));
      $meia = false;
      
      if($dia == "Sun"){
        $tipo = 1;
      }
      else{
        $tipo = 0;
      }

      $data_original = $request->data_viagem;
      $user = User::find($request->user_id);
      $horarios = Horario::where('seccionamento_id', $seccionamento->id)->
                           where('tipo', $tipo)->
                           where('sentido', $seccionamento->sentido)->
                           get();

                           
      if($request->meia == "on"){
        $seccionamento->valor_passagem = $seccionamento->valor_passagem/2;
        $meia = true;
      }

      $data_where = date('Y-m-d', strtotime(str_replace("/", "-", $request->data_viagem)));
                          
      foreach($horarios as $horario){
        $total = Passagens::where('seccionamento_id', $seccionamento->id)
                          ->where('data_viagem', $data_where)
                          ->where('horario_id', $horario->id)
                          ->count();

        $horario->total_passagens = $total; 
      }

      return view('passagens.formnext', compact('seccionamento', 'pessoa', 'horarios', 'user', 'data_original', 'meia'));
    }
    catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function status(Request $request){
    try{
      $status = $request->status;
      $pagamento = $request->payment_id;
      $passagem = Passagens::find($request->passagem_id);

      $user = User::find($passagem->user_id);
      $cashback = $passagem->valor * 0.1;
      $user_cashback = $user->saldo_cashback + $cashback;

      if($status == 'approved'){
        $status = 1;
        $user->update([
          'saldo_cashback' => $user_cashback
        ]);
      }
      else{
        $status = 0;
      }

      $passagem->update([
        'status' => $status,
        'pagamento' => $pagamento
      ]);

      return view('passagens.status', compact('status'));
    }
    catch(\Exception $error){
      return response()->json($error, 400);
    }
  }

  public function pay_after($id){
    $passagem = Passagens::find($id); 
    $user = $passagem->user; 
    $user = User::find($passagem->user_id);
    $passagem->data_viagem = date('d/m/Y', strtotime(str_replace("-", "/", $passagem->data_viagem)));
    return view("passagens.payafter", compact('passagem', 'user'));
  }

  public function use(Request $request){
    try{

      $passagem = Passagens::find($request->passagem_id);
      $cooperado = Cooperados::find($request->cooperado_id);

      if($cooperado->linha->grupo == $passagem->linha->grupo){
        $saldo_passagem = $passagem->valor * 0.9;
        $saldo_final = $cooperado->saldo + $saldo_passagem;

        $passagem->update([
          'status' => 2,
          'utilizacao' => date('Y-m-d'),
          'cooperado_id' => $cooperado->id
        ]);

        $cooperado->update([
          'saldo' => $saldo_final
        ]);

        return view('passagens.use');
      }else{
        return view('passagens.fail');
      }
     
    }
    catch(\Exception $error){
      return response()->json($error, 400);
    }
  }

  public function find_user($id){
    try{
      $user = User::find($id);
      $agente = Agencias::find($user->agencia_id);
      return view('passagens.agente.buscar', compact('user', 'agente'));
    }
    catch(\Exception $erro){

    }
  }

  public function form_agente(Request $request){
    try {
      $linhas = Linha::all();
      $user = User::where('email', $request->email)->first();
      $agente = Agencias::find($request->agente_id);

      return view('passagens.agente.forminit', compact('user', 'linhas', 'agente'));
    }
    catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function formstep_agente(Request $request){
    try {
      $seccionamento = Seccionamento::find($request->seccionamento_id);
      $pessoa = ["nome" => $request->nome_completo, "documento" => $request->doc_passageiro];
      $data_viagem = date('m/d/Y', strtotime(str_replace("/", "-", $request->data_viagem)));
      $dia = date('D', strtotime($data_viagem));
      $agente = Agencias::find($request->agente_id);
      $meia = false;
      
      if($dia == "Sun"){
        $tipo = 1;
      }
      else{
        $tipo = 0;
      }

      if($request->meia == "on"){
        $seccionamento->valor_passagem = $seccionamento->valor_passagem/2;
        $meia = true;
      }

      $data_original = $request->data_viagem;
      $user = User::find($request->user_id);

      return view('passagens.agente.formnext', compact('seccionamento', 'pessoa', 'user', 'data_original', 'agente', 'meia'));
    }
    catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function payment_money(Request $request){
    try {

      $seccionamento = Seccionamento::find($request->seccionamento_id);
      $pessoa = ["nome" => $request->nome_passageiro, "documento" => $request->doc_passageiro];
      $ponto_saida = Pontos::find($seccionamento->id_ponto_saida);
      $ponto_chegada = Pontos::find($seccionamento->id_ponto_chegada);
      $nome_passagem = $ponto_saida->nome . ' - '. $ponto_chegada->nome;
      $emissao = date('Y-m-d');
      $user = User::find($request->user_id);
      $data_viagem = date('Y-m-d', strtotime(str_replace("/", "-", $request->data_viagem)));
      $data_original = $request->data_viagem;
      $horario = Horario::find($request->horario_id);
      $agente = Agencias::find($request->agente_id);

      if($request->meia == 1){
        $meia_passagem=1;
        $meia=true;
      }else{
        $meia_passagem=0;
        $meia=false;
      }

      $passagem = Passagens::create([
        'data_viagem' => $data_viagem,
        'emissao' => $emissao,
        'nome_passageiro' => strtoupper($request->nome_passageiro),
        'doc_passageiro' => $request->doc_passageiro,
        'nome_passagem' => $nome_passagem,
        'valor' => $request->valor,
        'status' => 0,
        'linha_id' => $request->linha_id,
        'meia_passagem' => $meia_passagem,
        'empresa_id' => $user->empresa_id,
        'seccionamento_id' => $request->seccionamento_id,
        'user_id' => $user->id,
        'agente_id' => $agente->id,
        'cashback' => 0
      ]);

      return view('passagens.agente.formcheckout', compact('seccionamento', 'pessoa', 'user', 'data_original', 'horario', 'passagem', 'agente' ));

    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function status_agente(Request $request){
    try{

      $pagamento ='AG';
      $passagem = Passagens::find($request->passagem_id);

      $agente = Agencias::find($passagem->agente_id);
      $saldo_ag = $passagem->valor * 0.05;
      $saldo_final_ag = $agente->saldo + $saldo_ag;


      $user = User::find($passagem->user_id);
      $cashback = $passagem->valor * 0.1;
      $user_cashback = $user->saldo_cashback + $cashback;

      $user->update([
        'saldo_cashback' => $user_cashback
      ]);

      $agente->update([
        'saldo' => $saldo_final_ag
      ]);

      $passagem->update([
        'status' => 1,
        'pagamento' => $pagamento
      ]);

      return view('passagens.agente.status');
    }
    catch(\Exception $error){
      return response()->json($error, 400);
    }
  }

  public function form_saldo($user_id){
    try {
      $linhas = Linha::all();
      $user = User::find($user_id);
      return view('passagens.saldo.forminit', compact('user', 'linhas'));
    }
    catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function formstep_saldo(Request $request){
    try {
      $seccionamento = Seccionamento::find($request->seccionamento_id);
      $pessoa = ["nome" => $request->nome_completo, "documento" => $request->doc_passageiro];
      $data_viagem = date('m/d/Y', strtotime(str_replace("/", "-", $request->data_viagem)));
      $dia = date('D', strtotime($data_viagem));
      $meia = false;

      if($dia == "Sun"){
        $tipo = 1;
      }
      else{
        $tipo = 0;
      }

      if($request->meia == "on"){
        $seccionamento->valor_passagem = $seccionamento->valor_passagem/2;
        $meia = true;
      }

      $data_original = $request->data_viagem;
      $user = User::find($request->user_id);
      $horarios = Horario::where('seccionamento_id', $seccionamento->id)->
                           where('tipo', $tipo)->
                           where('sentido', $seccionamento->sentido)->
                           get();

      $data_where = date('Y-m-d', strtotime(str_replace("/", "-", $request->data_viagem)));
      
      foreach($horarios as $horario){
        $total = Passagens::where('seccionamento_id', $seccionamento->id)
                          ->where('data_viagem', $data_where)
                          ->where('horario_id', $horario->id)
                          ->count();
                                      
        $horario->total_passagens = $total; 
      }

      return view('passagens.saldo.formnext', compact('seccionamento', 'pessoa', 'horarios', 'user', 'data_original', 'meia'));
    }
    catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function payment_saldo(Request $request){
    try {

      $seccionamento = Seccionamento::find($request->seccionamento_id);
      $pessoa = ["nome" => $request->nome_passageiro, "documento" => $request->doc_passageiro];
      $ponto_saida = Pontos::find($seccionamento->id_ponto_saida);
      $ponto_chegada = Pontos::find($seccionamento->id_ponto_chegada);
      $nome_passagem = $ponto_saida->nome . ' - '. $ponto_chegada->nome;
      $emissao = date('Y-m-d');
      $user = User::find($request->user_id);
      $data_viagem = date('Y-m-d', strtotime(str_replace("/", "-", $request->data_viagem)));
      $data_original = $request->data_viagem;
      $horario = Horario::find($request->horario_id);

      if($request->meia == 1){
        $meia_passagem=1;
        $meia=true;
      }else{
        $meia_passagem=0;
        $meia=false;
      }

      $passagem = Passagens::create([
        'data_viagem' => $data_viagem,
        'emissao' => $emissao,
        'nome_passageiro' => strtoupper($request->nome_passageiro),
        'doc_passageiro' => $request->doc_passageiro,
        'nome_passagem' => $nome_passagem,
        'valor' => $request->valor,
        'horario_id' => $request->horario_id,
        'status' => 0,
        'linha_id' => $request->linha_id,
        'meia_passagem' => $meia_passagem,
        'empresa_id' => $user->empresa_id,
        'seccionamento_id' => $request->seccionamento_id,
        'user_id' => $user->id,
        'cashback' => 0
      ]);

      return view('passagens.saldo.formcheckout', compact('seccionamento', 'pessoa', 'user', 'data_original', 'horario', 'passagem', 'meia' ));

    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function status_saldo(Request $request){
    try{

      $pagamento ='SD';
      $passagem = Passagens::find($request->passagem_id);

      $agente = Agencias::find($passagem->agente_id);


      $user = User::find($passagem->user_id);

      $saldo_user = $user->saldo - $passagem->valor;
      $user->update([
        'saldo' => $saldo_user
      ]);

      $passagem->update([
        'status' => 1,
        'pagamento' => $pagamento
      ]);

      return view('passagens.saldo.status');
    }
    catch(\Exception $error){
      return response()->json($error, 400);
    }
  }

  public function form_cashback($user_id){
    try {
      $linhas = Linha::all();
      $user = User::find($user_id);
      return view('passagens.cashback.forminit', compact('user', 'linhas'));
    }
    catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function formstep_cashback(Request $request){
    try {
      $seccionamento = Seccionamento::find($request->seccionamento_id);
      $pessoa = ["nome" => $request->nome_completo, "documento" => $request->doc_passageiro];
      $data_viagem = date('m/d/Y', strtotime(str_replace("/", "-", $request->data_viagem)));
      $dia = date('D', strtotime($data_viagem));
      
      if($dia == "Sun"){
        $tipo = 1;
      }
      else{
        $tipo = 0;
      }
      $data_original = $request->data_viagem;
      $user = User::find($request->user_id);
      $horarios = Horario::where('seccionamento_id', $seccionamento->id)->
                           where('tipo', $tipo)->
                           where('sentido', $seccionamento->sentido)->
                           get();
                           
      $data_where = date('Y-m-d', strtotime(str_replace("/", "-", $request->data_viagem)));
      
      foreach($horarios as $horario){
        $total = Passagens::where('seccionamento_id', $seccionamento->id)
                          ->where('data_viagem', $data_where)
                          ->where('horario_id', $horario->id)
                          ->count();
                                      
        $horario->total_passagens = $total; 
      }

      return view('passagens.cashback.formnext', compact('seccionamento', 'pessoa', 'horarios', 'user', 'data_original'));
    }
    catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function payment_cashback(Request $request){
    try {

      $seccionamento = Seccionamento::find($request->seccionamento_id);
      $pessoa = ["nome" => $request->nome_passageiro, "documento" => $request->doc_passageiro];
      $ponto_saida = Pontos::find($seccionamento->id_ponto_saida);
      $ponto_chegada = Pontos::find($seccionamento->id_ponto_chegada);
      $nome_passagem = $ponto_saida->nome . ' - '. $ponto_chegada->nome;
      $emissao = date('Y-m-d');
      $user = User::find($request->user_id);
      $data_viagem = date('Y-m-d', strtotime(str_replace("/", "-", $request->data_viagem)));
      $data_original = $request->data_viagem;
      $horario = Horario::find($request->horario_id);

      $passagem = Passagens::create([
        'data_viagem' => $data_viagem,
        'emissao' => $emissao,
        'nome_passageiro' => strtoupper($request->nome_passageiro),
        'doc_passageiro' => $request->doc_passageiro,
        'nome_passagem' => $nome_passagem,
        'valor' => $request->valor,
        'horario_id' => $request->horario_id,
        'status' => 0,
        'linha_id' => $request->linha_id,
        'empresa_id' => $user->empresa_id,
        'seccionamento_id' => $request->seccionamento_id,
        'user_id' => $user->id,
        'cashback' => 1
      ]);

      return view('passagens.cashback.formcheckout', compact('seccionamento', 'pessoa', 'user', 'data_original', 'horario', 'passagem' ));

    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function status_cashback(Request $request){
    try{

      $pagamento ='CB';
      $passagem = Passagens::find($request->passagem_id);

      $agente = Agencias::find($passagem->agente_id);


      $user = User::find($passagem->user_id);

      $saldo_user = $user->saldo_cashback - $passagem->valor;
      $user->update([
        'saldo_cashback' => $saldo_user
      ]);

      $passagem->update([
        'status' => 1,
        'pagamento' => $pagamento
      ]);

      return view('passagens.cashback.status');
    }
    catch(\Exception $error){
      return response()->json($error, 400);
    }
  }

  public function delete($id){
    try{
      $passagem = Passagens::find($id);

      $passagem->delete();

      return response()->json("Passagem apagada");
    }
    catch(\Exception $error){
      return response()->json($error, 400);
    }
  }

  public function verify_payment($payment_id){
    try{
      $ch = curl_init('https://api.mercadopago.com/v1/payments/'. $payment_id);

      curl_setopt_array($ch, [
      
          // Equivalente ao -X:
          CURLOPT_CUSTOMREQUEST => 'GET',
      
          // Equivalente ao -H:
          CURLOPT_HTTPHEADER => [
              'Authorization: Bearer APP_USR-6696593648823568-022318-97164e8025eb819aebb2241409f8dbe5-719432533'
          ],
      
          // Permite obter o resultado
          CURLOPT_RETURNTRANSFER => 1,
      ]);
      
      $resposta = json_decode(curl_exec($ch), true);
      curl_close($ch);

      if($resposta['status'] == "approved"){
        $passagem = Passagens::where('pagamento', $payment_id)->first();
        
        $passagem->update([
          'status' => 1,
        ]);

        $user = $passagem->user;

        $cashback = $passagem->valor * 0.1;
        
        $user->update([
           'saldo_cashback' => $user->saldo_cashback + $cashback
        ]);

        return response()->json("aprovado");
      }
      else{
        return response()->json("erro");
      }


    }catch(\Exception $error){
      dd($error);
    }
  }

  public function RelatorioCooperadoIndex(){
    try{
      $cooperados = Cooperados::where('empresa_id', Auth::user()->empresa_id)->get();

      return view('admin.relatorios.cooperados.passagens.index', compact('cooperados'));

    }catch(\Exception $error){
      return response()->json(['errors' => ['exception'=> 'Houve um erro inesperado, entre em contato com o suporte']], 422);
    }
  }

  public function RelatorioCooperadoConsulta(Request $request){
    try{

      $request->data_in = date('Y-m-d', strtotime(str_replace("/", "-",$request->data_in)));
      $request->data_fi = date('Y-m-d', strtotime(str_replace("/", "-",$request->data_fi)));

      if($request->cooperado_id == "0"){
        $cooperado = "todos";
        if($request->data_ref == "1"){
          $passagens = Passagens::where('empresa_id', Auth::user()->empresa_id)
                                ->whereBetween("data_viagem", [$request->data_in, $request->data_fi])
                                ->where('status', 2)
                                ->get();
          $tipoData = "viagem";
        }elseif($request->data_ref == "2"){
          $passagens = Passagens::where('empresa_id', Auth::user()->empresa_id)
                                ->whereBetween("emissao", [$request->data_in, $request->data_fi])
                                ->where('status', 2)
                                ->get();
          $tipoData = "emissao";
        }
      }else{
        $cooperado = Cooperados::find($request->cooperado_id);

        if($request->data_ref == "1"){
          $passagens = Passagens::where('empresa_id', Auth::user()->empresa_id)
                                ->where('cooperado_id', $request->cooperado_id)
                                ->whereBetween("data_viagem", [$request->data_in, $request->data_fi])
                                ->where('status', 2)
                                ->get();
          $tipoData = "viagem";
        }elseif($request->data_ref == "2"){
          $passagens = Passagens::where('empresa_id', Auth::user()->empresa_id)
                                ->where('cooperado_id', $request->cooperado_id)
                                ->whereBetween("emissao", [$request->data_in, $request->data_fi])
                                ->where('status', 2)
                                ->get();
          $tipoData = "emissao";              
        }
      }

      $data_in = date('d/m/Y', strtotime($request->data_in));
      $data_fi = date('d/m/Y', strtotime($request->data_fi));

      $tipo = "Relatório de passagens por cooperado";

      return view('admin.relatorios.cooperados.passagens.relatorio', compact('cooperado', 'data_in', 'data_fi', 'tipo', 'passagens', 'tipoData'));
    }catch(\Exception $error){
      return response()->json(['errors' => ['exception'=> 'Houve um erro inesperado, entre em contato com o suporte']], 422);
    }
  }

  public function RelatorioLinhasIndex(){
    try{
      $linhas = Linha::where('empresa_id', Auth::user()->empresa_id)->get();

      return view('admin.relatorios.linhas.passagens.index', compact('linhas'));

    }catch(\Exception $error){
      return response()->json(['errors' => ['exception'=> 'Houve um erro inesperado, entre em contato com o suporte']], 422);
    }
  }

  public function RelatorioLinhasConsulta(Request $request){
    try{

      $request->data_in = date('Y-m-d', strtotime(str_replace("/", "-",$request->data_in)));
      $request->data_fi = date('Y-m-d', strtotime(str_replace("/", "-",$request->data_fi)));

      if($request->linha_id == "0"){
        $linha = "todos";
        if($request->data_ref == "1"){
          $passagens = Passagens::where('empresa_id', Auth::user()->empresa_id)
                                ->whereBetween("data_viagem", [$request->data_in, $request->data_fi])
                                ->where('status', 2)
                                ->get();
          $tipoData = "viagem";
        }elseif($request->data_ref == "2"){
          $passagens = Passagens::where('empresa_id', Auth::user()->empresa_id)
                                ->whereBetween("emissao", [$request->data_in, $request->data_fi])
                                ->where('status', 2)
                                ->get();
          $tipoData = "emissao";
        }
      }else{
        $linha = Linha::find($request->linha_id);

        if($request->data_ref == "1"){
          $passagens = Passagens::where('empresa_id', Auth::user()->empresa_id)
                                ->where('linha_id', $request->linha_id)
                                ->whereBetween("data_viagem", [$request->data_in, $request->data_fi])
                                ->where('status', 2)
                                ->get();
          $tipoData = "viagem";
        }elseif($request->data_ref == "2"){
          $passagens = Passagens::where('empresa_id', Auth::user()->empresa_id)
                                ->where('linha_id', $request->linha_id)
                                ->whereBetween("emissao", [$request->data_in, $request->data_fi])
                                ->where('status', 2)
                                ->get();
          $tipoData = "emissao";              
        }
      }

      $data_in = date('d/m/Y', strtotime($request->data_in));
      $data_fi = date('d/m/Y', strtotime($request->data_fi));

      $tipo = "Relatório de passagens por linha";

      return view('admin.relatorios.linhas.passagens.relatorio', compact('linha', 'data_in', 'data_fi', 'tipo', 'passagens', 'tipoData'));
    }catch(\Exception $error){
      return response()->json(['errors' => ['exception'=> 'Houve um erro inesperado, entre em contato com o suporte']], 422);
    }
  }

  public function RelatorioAgentesIndex(){
    try{
      $agentes = Agencias::where('empresa_id', Auth::user()->empresa_id)->get();

      return view('admin.relatorios.agentes.passagens.index', compact('agentes'));

    }catch(\Exception $error){
      return response()->json(['errors' => ['exception'=> 'Houve um erro inesperado, entre em contato com o suporte']], 422);
    }
  }

  public function RelatorioAgentesConsulta(Request $request){
    try{

      $request->data_in = date('Y-m-d', strtotime(str_replace("/", "-",$request->data_in)));
      $request->data_fi = date('Y-m-d', strtotime(str_replace("/", "-",$request->data_fi)));

      if($request->agente_id == "0"){
        $agente = "todos";
        if($request->data_ref == "1"){
          $passagens = Passagens::where('empresa_id', Auth::user()->empresa_id)
                                ->whereBetween("data_viagem", [$request->data_in, $request->data_fi])
                                ->whereNotNull('agente_id')
                                ->get();
          $tipoData = "viagem";
        }elseif($request->data_ref == "2"){
          $passagens = Passagens::where('empresa_id', Auth::user()->empresa_id)
                                ->whereBetween("emissao", [$request->data_in, $request->data_fi])
                                ->whereNotNull('agente_id')
                                ->get();
          $tipoData = "emissao";
        }
      }else{
        $agente = Agencias::find($request->agente_id);

        if($request->data_ref == "1"){
          $passagens = Passagens::where('empresa_id', Auth::user()->empresa_id)
                                ->where('agente_id', $request->agente_id)
                                ->whereBetween("data_viagem", [$request->data_in, $request->data_fi])
                                ->get();
          $tipoData = "viagem";
        }elseif($request->data_ref == "2"){
          $passagens = Passagens::where('empresa_id', Auth::user()->empresa_id)
                                ->where('agente_id', $request->agente_id)
                                ->whereBetween("emissao", [$request->data_in, $request->data_fi])
                                ->get();
          $tipoData = "emissao";              
        }
      }

      $data_in = date('d/m/Y', strtotime($request->data_in));
      $data_fi = date('d/m/Y', strtotime($request->data_fi));

      $tipo = "Relatório de passagens por agentes";

      return view('admin.relatorios.agentes.passagens.relatorio', compact('agente', 'data_in', 'data_fi', 'tipo', 'passagens', 'tipoData'));
    }catch(\Exception $error){
      return response()->json(['errors' => ['exception'=> 'Houve um erro inesperado, entre em contato com o suporte']], 422);
    }
  }
}
