<?php

namespace App\Http\Controllers;

use App\Models\Pontos;
use Illuminate\Http\Request;

class PontosController extends Controller
{
  public function index()
  {
    try {
      $pontos = Pontos::all();

      return response()->json($pontos);
    } catch (\Exception $error) {
      return response()->json($error, 500);
    }
  }

  public function store(Request $request)
  {
    try {
      $data = new Pontos();

      Pontos::create([
        'nome' => $request->nome,
        'latitude' => $request->latitude,
        'longitude' => $request->longitude,
        'linha_id' => $request->linha_id,
      ]);

      return response()->json($data, 201);
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function indexAdm(){
    try {
      $pontos = Pontos::all();

      return view('admin.pontos.index', compact('pontos'));
    } catch (\Throwable $th) {
      throw $th;
    }
  }

  public function form(){
      return view('admin.pontos.form');
  }

  public function save(Request $request){
    try {

      Pontos::create([
        'nome' => $request->nome,
        'latitude' => '0',
        'longitude' => '0'
      ]);

      return redirect()->route('admin.home');
    } catch (\Exception $error) {
      dd($error);
    }
  }

  public function edit($id){
    try{
      $ponto = Pontos::find($id);

      return view('admin.pontos.edit', compact('ponto'));
    }catch(\Exception $error){
      return response()->json($error, 500);
    }
  }

  public function update(Request $request){
    try {
      $ponto = Pontos::find($request->ponto_id);

      $ponto->update([
        'nome' => $request->nome
      ]);

      return redirect()->route('admin.home');

    } catch (\Exception $error) {
        return response()->json($error, 500);
    }
  }

  public function delete($id){
    try{
      $linha = Pontos::find($id);

      $linha->delete();

      return response()->json(['text' => "Ponto excluido!", "title" => "Sucesso!", "class"=>'success']);


    }catch(\Exception $error){
      return response()->json($error, 500);
    }
  }

  
}
