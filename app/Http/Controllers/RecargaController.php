<?php

namespace App\Http\Controllers;

use App\Models\Agencias;
use App\Models\Recargas;
use App\Models\User;
use Illuminate\Http\Request;

class RecargaController extends Controller
{
    //

    public function index_user($id){
        try {
          $recargas = Recargas::where('user_id', $id)->get();
    
          return response()->json($recargas, 200);
        } catch (\Exception $error) {
          return response()->json($error, 400);
        }
    }

    public function index($id){
        try{
            $user = User::find($id);
            return view('recarga.user.index', compact('user'));
        }
        catch(\Exception $error){
            return response()->json($error, 400);
        }
    }

    public function indexagencia($id, Request $request){
        try{
            $user = User::find($id);
            $agente = Agencias::find($request->agente_id);
            return view('recarga.agente.index', compact('user', 'agente'));
        }
        catch(\Exception $error){
            return response()->json($error, 400);
        }
    }

    public function pay(Request $request){
        try {
            $user = User::find($request->user_id);
            $valor = $request->valor;
            $recarga  = Recargas::create([
                'user_id' => $user->id,
                'valor' => $valor ,
                'status' => 0,
            ]);

            return view('recarga.user.pay', compact('user', 'valor', 'recarga'));
        }
        catch (\Exception $error) {
            return response()->json($error, 400);
        }
    }
    
    public function receiveagencia(Request $request){
        try {
            $user = User::find($request->user_id);
            $agente = Agencias::find($request->agente_id);
            $valor = $request->valor;

            $recarga  = Recargas::create([
                'user_id' => $user->id,
                'valor' => $valor ,
                'agente_id' => $agente->id,
                'status' => 0,
            ]);

            return view('recarga.agente.pay', compact('user', 'valor', 'recarga'));
        }
        catch (\Exception $error) {
            return response()->json($error, 400);
        }
    }

    public function status(Request $request){
        try{
            $status = $request->status;
            $pagamento = $request->payment_id;
            $recarga = Recargas::find($request->recarga_id);
            $user = User::find($recarga->user_id);

            $valor_agente = $recarga->valor * 0.05;
            $valor_cashback = $recarga->valor * 0.1;
            $valor_final_cashback = $user->saldo_cashback + $valor_cashback;
            $saldo_final_user = $user->saldo + $recarga->valor;
            

            if($status == 'approved'){
                $status = 1;
                $user->update([
                    'saldo' => $saldo_final_user,
                    'saldo_cashback' => $valor_final_cashback
                ]);
            }
            else{
                $status = 0;
            }
        
            $recarga->update([
                'status' => $status,
                'pagamento' => $pagamento
            ]);

            return view('recarga.user.status', compact('status'));
        }
        catch(\Exception $error){
          return response()->json($error, 400);
        }
    }

    public function statusagencia(Request $request){
        try{
            $pagamento = 'AG';
            $recarga = Recargas::find($request->recarga_id);
            $user = User::find($recarga->user_id);
            $agente = Agencias::find($recarga->agente_id);

            $valor_agente = $recarga->valor * 0.05;
            $valor_cashback = $recarga->valor * 0.1;
            $valor_final_cashback = $user->saldo_cashback + $valor_cashback;
            $saldo_final_user = $user->saldo + $recarga->valor;
            $saldo_final_agencia = $agente->saldo + $valor_agente;
            
            $agente->update([
                'saldo' => $saldo_final_agencia
            ]);

            $user->update([
                'saldo' => $saldo_final_user,
                'saldo_cashback' => $valor_final_cashback
            ]);
                

            $recarga->update([
                'status' => 1,
                'pagamento' => $pagamento
            ]);

            return view('recarga.agente.status');
        }
        catch(\Exception $error){
            dd($error);
          return response()->json($error, 400);
        }
    }

    public function delete($id){
        try{
          $recarga = Recargas::find($id);
    
          $recarga->delete();
    
          return response()->json("Recarga apagada");
        }
        catch(\Exception $error){
          return response()->json($error, 400);
        }
    }

    public function verify_payment($payment_id){
        try{
          $ch = curl_init('https://api.mercadopago.com/v1/payments/'. $payment_id);
    
          curl_setopt_array($ch, [
          
              // Equivalente ao -X:
              CURLOPT_CUSTOMREQUEST => 'GET',
          
              // Equivalente ao -H:
              CURLOPT_HTTPHEADER => [
                  'Authorization: Bearer APP_USR-6696593648823568-022318-97164e8025eb819aebb2241409f8dbe5-719432533'
              ],
          
              // Permite obter o resultado
              CURLOPT_RETURNTRANSFER => 1,
          ]);
          
          $resposta = json_decode(curl_exec($ch), true);
          curl_close($ch);
    
          if($resposta['status'] == "approved"){
            $recarga = Recargas::where('pagamento', $payment_id)->first();
            
            $recarga->update([
              'status' => 1,
            ]);
            
            $user = $recarga->user;

            $valor_cashback = $recarga->valor * 0.1;
            $valor_final_cashback = $user->saldo_cashback + $valor_cashback;
            $saldo = $user->saldo + $recarga->valor;

            $user->update([
                'saldo' => $saldo,
                'saldo_cashback' => $valor_final_cashback
            ]);
    
            return response()->json("aprovado");
          }
          else{
            return response()->json("erro");
          }
    
    
        }catch(\Exception $error){
          dd($error);
        }
    }
}
