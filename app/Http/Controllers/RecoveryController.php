<?php

namespace App\Http\Controllers;

use App\Mail\MailRecovery;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class RecoveryController extends Controller
{
  public function recover($token)
  {
    try {
      $reset = DB::table('senha_resets')->where('token', $token)->first();

      if (!empty($reset)) {
        return redirect('/');
      } else {
        return response()->json('Token de validação inválido, envie outro e-mail de recuperação.', 400);
      }
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function send(Request $request)
  {
    try {
      $user = User::where('email', $request->email)->first();
      if (empty($user)) {
        return response()->json('O email é necessário!', 422);
      }

      Mail::send(new MailRecovery($user));

      return response()->json('Recuperação de senha foi enviada com sucesso, verifique sua caixa de entrada.', 200);
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }
}
