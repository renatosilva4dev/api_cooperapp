<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResetController extends Controller
{
  public function reset(Request $request)
  {
    $request->validate([
      "token" => 'required',
      "email" => 'required|email',
      'senha' => 'required|confirmed|min:6',
      'senha_confirmation' => 'required',
    ], [
      'token.required' => 'Algo deu errado, tente novamente.',
      'email.required' => 'Informe o e-mail.',
      'email.email' => 'E-mail inválido.',
      'senha.required' => 'Informe uma senha.',
      'senha.confirmed' => 'As senhas não são iguais.',
      'senha.min' => 'A senha deve conter pelo menos 6 caracteres.',
      'senha_confirmation.required' => 'informe uma senha.',
    ]);

    try {
      $reset = DB::table('senha_resets')->where('token', $request->token)->where('email', $request->email)->first();
      if (empty($reset)) {
        return response()->json(['email' => 'E-mail não encontrado!'], 422);
      }

      $user = User::where('email', $reset->email)->first();

      $user->update(["senha" => bcrypt($request->senha)]);

      return response()->json(['success' => 'Senha alterada com sucesso.'], 200);
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }
}
