<?php

namespace App\Http\Controllers;

use App\Models\Seccionamento;
use Illuminate\Http\Request;

class SeccionamentoController extends Controller
{
  public function index()
  {
    try {
      $seccionamento = Seccionamento::all();

      return response()->json($seccionamento, 200);
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }

  public function store(Request $request)
  {
    try {

      Seccionamento::create([
        'id_ponto_chegada' => $request->id_ponto_chegada,
        'id_ponto_saida' => $request->id_ponto_saida,
        'valor_passagem' => $request->valor_passagem,
        'linha_id' => $request->linha_id,
      ]);

      return response()->json(["success" => true], 201);
    } catch (\Exception $error) {
      return response()->json($error, 400);
    }
  }
}
