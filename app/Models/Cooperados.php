<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cooperados extends Model
{
  use HasFactory;

  protected $fillable = [
    'id',
    'nome',
    'telefone',
    'matricula',
    'saldo',
    'empresa_id',
    'linha_id'
  ];

  public function empresa()
  {
    return $this->belongsTo(Empresa::class, 'empresa_id');
  }

  public function linha()
  {
    return $this->belongsTo(Linha::class, 'linha_id');
  }


  public function Passagens()
  {
    return $this->hasMany(Passagens::class, 'horario_id');
  }

  public function usuarios(){
    return $this->hasMany(User::class, 'cooperado_id');
  }
}
