<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
  use HasFactory;

  protected $fillable = [
    'id',
    'nome',
    'cnpj',
    'telefone',
    'endereco'
  ];

  public function user()
  {
    return $this->hasMany(User::class, 'empresa_id');
  }
}
