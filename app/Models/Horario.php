<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
  use HasFactory;
  protected $fillable = [
    'id',
    'horario_saida',
    'horario_chegada',
    'tipo',
    'linha_id',
    'sentido'
  ];

  public function horario()
  {
    return $this->belongsTo(Linha::class, 'linha_id');
  }

  public function Passagens()
  {
    return $this->hasMany(Passagens::class, 'horario_id');
  }
}
