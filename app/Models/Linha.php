<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Linha extends Model
{
  use HasFactory;
  protected $fillable = [
    'id',
    'nome_linha',
    'grupo',
    'empresa_id'
  ];
  
  public function horarios()
  {
    return $this->hasMany(Horario::class, 'linha_id');
  }

  public function seccionamentos()
  {
    return $this->hasMany(Seccionamento::class, 'linha_id');
  }

  public function cooperados()
  {
    return $this->hasMany(Cooperados::class, 'linha_id');
  }

  public function empresa()
  {
    return $this->belongsTo(Empresa::class, 'empresa_id');
  }


}
