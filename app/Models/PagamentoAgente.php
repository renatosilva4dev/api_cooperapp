<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PagamentoAgente extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'agente_id',
        'valor',
        'data'
    ];

    public function agente()
    {
        return $this->belongsTo(Agencias::class, 'agente_id');
    }

}
