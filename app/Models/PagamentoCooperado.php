<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PagamentoCooperado extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'id',
        'cooperado_id',
        'valor',
        'data'
    ];

    public function cooperado()
    {
      return $this->belongsTo(Cooperados::class, 'cooperado_id');
    }
}
