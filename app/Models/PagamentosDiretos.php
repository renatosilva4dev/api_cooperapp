<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PagamentosDiretos extends Model
{
    use HasFactory;

    protected $fillable = [
        'valor',
        'user_id',
        'cooperado_id',
        'data'
    ];
}
