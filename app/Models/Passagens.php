<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Passagens extends Model
{
  use HasFactory;

  protected $fillable = [
    'id',
    'nome_passageiro',
    'nome_passagem',
    'data_viagem',
    'emissao',
    'utilizacao',
    'valor',  
    'status',
    'meia_passagem',
    'linha_id',
    'empresa_id',
    'agente_id',
    'cooperado_id',
    'seccionamento_id',
    'user_id',
    'doc_passageiro',
    'pagamento',
    'cashback'
  ];

  public function empresas()
  {
    $this->belongsTo(Empresa::class, 'empresa_id');
  }

  public function cooperado()
  {
    return $this->belongsTo(Cooperados::class, 'cooperado_id');
  }

  public function agente()
  {
    return $this->belongsTo(Agencias::class, 'agente_id');
  }

  public function seccionamento()
  {
    $this->belongsTo(Seccionamento::class, 'seccionamento_id');
  }

  public function linha()
  {
    return $this->belongsTo(Linha::class, 'linha_id');
  }

  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function horario()
  {
    return $this->belongsTo(Horario::class, 'horario_id');
  }
}
