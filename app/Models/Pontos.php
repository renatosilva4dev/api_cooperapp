<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pontos extends Model
{
  use HasFactory;

  protected $fillable = [
    'id',
    'nome',
    'latitude',
    'longitude'
  ];
  
  public function seccionamento_chegada()
  {
    $this->hasMany(Pontos::class, 'id_ponto_chegada');
  }
  public function seccionamento_saida()
  {
    $this->hasMany(Pontos::class, 'id_ponto_saida');
  }
}
