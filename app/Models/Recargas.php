<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recargas extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'valor',
        'user_id',
        'agente_id',
        'status',
        'pagamento'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
