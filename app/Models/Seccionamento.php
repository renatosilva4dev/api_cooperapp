<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seccionamento extends Model
{
  use HasFactory;

  protected $fillable = [
    'id',
    'id_ponto_saida',
    'id_ponto_chegada',
    'linha_id',
    'valor_passagem',
    'sentido'
  ];

  public function linha()
  {
    return $this->belongsTo(Linha::class, 'linha_id');
  }

  public function ponto_chegada()
  {
    return $this->belongsTo(Pontos::class, 'id_ponto_chegada');
  }

  public function ponto_saida()
  {
    return $this->belongsTo(Pontos::class, 'id_ponto_saida');
  }
}
