<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
  use HasFactory, Notifiable;

  protected $fillable = [
    'nome_completo',
    'email',
    'CPF',
    'data_nascimento',
    'telefone',
    'endereco',
    'bairro',
    'cidade',
    'cep',
    'data_nasc',
    'senha',
    'confirmacao_senha',
    'empresa_id',
    'aceite_termos',
    'tipo',
    'saldo',
    'cooperado_id',
    'agencia_id',
    'saldo_cashback',
    'api_token'
  ];

  protected $hidden = [
    'senha',
    'confirmacao_senha',
  ];

  public function empresa()
  {
    return $this->belongsTo(Empresa::class, 'empresa_id');
  }

  public function cooperado(){
    return $this->belongsTo(Cooperados::class, 'cooperado_id');
  }
}
