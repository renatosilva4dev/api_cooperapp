<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class MailRecovery extends Mailable
{

  private $user;

  public function __construct(User $user)
  {
    $this->user = $user;
  }

  public function build()
  {
    $this->subject('Cooperita - Recuperação de conta');
    $this->to($this->user->email);

    $hash = $this->user->email . uniqid(date('HisYmd')) . rand(1000, 9999) . $this->user->id;
    $token = str_replace("/", '', Hash::make($hash));

    $data = [
      "url" => route('recovery.recover', $token),
    ];

    DB::table('password_resets')->insert(['email' => $this->user->email, 'token' => $token, 'created_at' => date('Y-m-d H:i:s')]);

    return $this->markdown('mail.recovery', ['user' => $this->user, 'data' => $data]);
  }
}
