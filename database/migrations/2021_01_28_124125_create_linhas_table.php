<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinhasTable extends Migration
{

  public function up()
  {
    Schema::create('linhas', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->integer('grupo');
      $table->string('nome_linha');
      $table->foreignId('empresa_id')->constrained('empresas')->onDelete('cascade')->onUpdate('cascade');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('linhas');
  }
}
