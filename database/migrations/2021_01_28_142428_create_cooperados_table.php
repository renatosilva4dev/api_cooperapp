<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCooperadosTable extends Migration
{
  public function up()
  {
    Schema::create('cooperados', function (Blueprint $table) {
      $table->id();
      $table->string('nome');
      $table->string('matricula');
      $table->string('telefone');
      $table->double('saldo');
      $table->foreignId('empresa_id')->constrained('empresas')->onDelete('cascade')->onUpdate('cascade');
      $table->foreignId('linha_id')->constrained('linhas')->onDelete('cascade')->onUpdate('cascade');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('cooperados');
  }
}
