<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('users', function (Blueprint $table) {
      $table->id();
      $table->string('nome_completo');
      $table->string('email')->unique();
      $table->string('CPF')->unique();
      $table->date('data_nascimento');
      $table->string('telefone');
      $table->string('endereco')->nullable();
      $table->string('bairro')->nullable();
      $table->string('cidade')->nullable();
      $table->string('cep')->nullable();
      $table->string('senha');
      $table->integer('tipo');
      $table->foreignId('cooperado_id')->nullable()->constrained('cooperados')->onDelete('cascade')->onUpdate('cascade');
      $table->foreignId('agencia_id')->nullable()->constrained('agencias')->onDelete('cascade')->onUpdate('cascade');
      $table->double('saldo');
      $table->double('saldo_cashback');
      $table->rememberToken();
      $table->timestamp('email_verified_at')->nullable();
      $table->string('api_token', 80)->unique()->nullable()->default(null);
      $table->foreignId('empresa_id')->constrained('empresas')->onDelete('cascade')->onUpdate('cascade');
      $table->integer('aceite_termos');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('users');
  }
}
