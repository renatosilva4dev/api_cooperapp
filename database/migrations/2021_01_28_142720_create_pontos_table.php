<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePontosTable extends Migration
{

  public function up()
  {
    Schema::create('pontos', function (Blueprint $table) {
      $table->id();
      $table->string('nome');
      $table->string('latitude');
      $table->string('longitude');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('pontos');
  }
}
