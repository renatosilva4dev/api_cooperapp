<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSenhaResetsTable extends Migration
{
  public function up()
  {
    Schema::create('senha_resets', function (Blueprint $table) {
      $table->string('email')->index();
      $table->string('token');
    });
  }

  public function down()
  {
    Schema::dropIfExists('senha_resets');
  }
}
