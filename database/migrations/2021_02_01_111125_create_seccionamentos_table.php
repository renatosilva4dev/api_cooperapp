<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeccionamentosTable extends Migration
{
  public function up()
  {
    Schema::create('seccionamentos', function (Blueprint $table) {
      $table->id();
      $table->string('valor_passagem');
      $table->foreignId('linha_id')->constrained('linhas')->onDelete('cascade')->onUpdate('cascade');
      $table->foreignId('id_ponto_saida')->constrained('pontos')->onDelete('cascade')->onUpdate('cascade');
      $table->foreignId('id_ponto_chegada')->constrained('pontos')->onDelete('cascade')->onUpdate('cascade');
      $table->string('sentido');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('seccionamentos');
  }
}
