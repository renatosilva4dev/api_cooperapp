<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHorariosTable extends Migration
{

  public function up()
  {
    Schema::create('horarios', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('horario_saida');
      $table->string('horario_chegada');
      $table->integer('tipo');
      $table->string('sentido');
      $table->foreignId('seccionamento_id')->constrained('seccionamentos')->onDelete('cascade')->onUpdate('cascade');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('horarios');
  }
}
