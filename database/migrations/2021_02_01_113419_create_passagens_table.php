<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePassagensTable extends Migration
{
  public function up()
  {
    Schema::create('passagens', function (Blueprint $table) {
      $table->id();
      $table->date('data_viagem');
      $table->date('emissao');
      $table->date('utilizacao')->nullable();
      $table->string('nome_passageiro');
      $table->string('doc_passageiro');
      $table->string('nome_passagem');
      $table->string('valor');
      $table->integer('status');
      $table->integer('meia_passagem');
      $table->integer('cashback');
      $table->string('pagamento')->nullable();
      $table->foreignId('seccionamento_id')->constrained('seccionamentos')->onDelete('cascade')->onUpdate('cascade');
      $table->foreignId('linha_id')->constrained('linhas')->onDelete('cascade')->onUpdate('cascade');
      $table->foreignId('user_id')->constrained('users')->onDelete('cascade')->onUpdate('cascade');
      $table->foreignId('cooperado_id')->nullable()->constrained('cooperados')->onDelete('cascade')->onUpdate('cascade');
      $table->foreignId('agente_id')->nullable()->constrained('agencias')->onDelete('cascade')->onUpdate('cascade');
      $table->foreignId('empresa_id')->constrained('empresas')->onDelete('cascade')->onUpdate('cascade');
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('passagens');
  }
}
