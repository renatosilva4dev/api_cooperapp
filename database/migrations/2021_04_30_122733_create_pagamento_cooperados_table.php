<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagamentoCooperadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagamento_cooperados', function (Blueprint $table) {
            $table->id();
            $table->foreignId('cooperado_id')->nullable()->constrained('cooperados')->onDelete('cascade')->onUpdate('cascade');
            $table->double('valor');
            $table->date('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagamento_cooperados');
    }
}
