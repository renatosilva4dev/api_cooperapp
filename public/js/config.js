$(document).ready(function(){

    setInterval(function(){
        $.ajaxSetup({
            headers: {
                'X-XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'GET',
            url: '/',
            error: function (err) {
                if(err.status == 401){
                    window.location.href = '/';
                }           
            },
        });
    }, 3660000)

    $("#btn-menu").click(function(){
        $('.menu').toggleClass('menu-close');
        $('.menu nav').fadeToggle(200);
        if($('.menu nav'). is(":visible")){
            $('.overlay-close-menu').fadeToggle(0);
        }
        $(this).find('.fa-bars').fadeToggle(0);
        $(this).find('.fa-times').fadeToggle(0);
    });

    $('.overlay-close-menu').click(function(){
        $('.menu').removeClass('menu-close');
        $('.menu nav').fadeOut(200);
        $('.overlay-close-menu').fadeOut(0);
        $("#btn-menu").find('.fa-bars').fadeToggle(0);
        $("#btn-menu").find('.fa-times').fadeToggle(0);
    });

    $('.mobile-menu-btn-2').click(function(){
        $('.menu').removeClass('menu-close');
        $('.menu nav').fadeOut(200);
        $('.overlay-close-menu').fadeOut(0);        
        $("#btn-menu").find('.fa-bars').fadeToggle(0);
        $("#btn-menu").find('.fa-times').fadeToggle(0);
    });
    
    var route = "sem rota";// global, armazena rota da pagina carregada
    $(".drop-link").click(function(e){// ------ dropdown no menu
        $(".drop-link").removeClass("linkfocus");
        $(this).addClass("linkfocus");
        $('.drop-link').next('ul').slideUp();
        $('.drop-link').find('span i').removeClass( "fa-rotate-270" );
        if($(this).next('ul').is(':visible')){
            $(this).next('ul').slideUp();
            $(this).find('span i').removeClass( "fa-rotate-270" );
        }else{
            $(this).next('ul').slideDown();
            $(this).find('span i').addClass( "fa-rotate-270" );
        }
        
        e.preventDefault();
    });

// ----------------------------- navegação ---------------------------------------
    $(".link-append").click(function(e){
        e.preventDefault();
        $(".drop-link").removeClass("linkfocus");
        $(this).parent().parent().prev().addClass("linkfocus");
        $(".link-append").removeClass("sublinkfocus");
        $(this).addClass("sublinkfocus");
        route = $(this).attr("href");
        $.ajax({
            url: route,
            beforeSend: function () {
                $("#modal_ajax_load").modal('show');
            }, 
            success: function(result){
                $("#main").html(result).fadeIn(500); 
            },
            error : function(erro){
                var erros = [];
                if(erro.status == 401){
                    $("#modal_success").find("h1").removeClass("text-danger");
                    $("#modal_success").find("h1").removeClass("text-success"); 
                    $("#modal_success").find("h1").text("Bloqueado!").addClass('text-danger');
                    $("#modal_success").find("p").text("Acesso não autorizado!");
                    $("#modal_success").modal("show"); 
                    $('#modal_success').on('hidden.bs.modal', function(e){
                        window.location.href = "/";
                    })
                }else if(erro.status == 500){
                    $("#modal_success").find("h1").removeClass("text-danger");
                    $("#modal_success").find("h1").removeClass("text-success"); 
                    $("#modal_success").find("h1").text("Erro!").addClass('text-danger');
                    $("#modal_success").find("p").text("Houve um erro inesperado, entre em contato com o suporte");
                    $("#modal_success").modal("show"); 
                }else{
                    erros = erro.responseJSON.errors;
                }
            },
            complete: function(e){
                $(".addPicker").selectpicker();
                $("#modal_ajax_load").modal("hide");   
            }
        });
    });


// -------------------------- cadastro empresa e usuario ----------------------------
    $(document).on("DOMNodeInserted", "#cadastro_empresa", function(e){
        if(e.target == this){         
            $("#form_cad_empresa input[type=text]").maiusculo(); 
            $("#form_cad_empresa #cnpj").maskInput("99.999.999.9999/99");
            $("#form_cad_empresa #cep").maskInput("99999-999");
            $("#form_cad_empresa #telefone").maskInput("(99) 9999-9999");
            $("#form_cad_empresa #matricula").somenteNumero();
            $("#form_cad_empresa #numero").somenteNumero();
        }
    });

    $(document).on("DOMNodeInserted", "#cadastro_presidente", function(e){
        if(e.target == this){          
            $("#form_cad_presidente input[type=text]").maiusculo(); 
            $("#form_cad_presidente #cpf").maskInput("999.999.999-99");
            $("#form_cad_presidente #cep").maskInput("99999-999");
            $("#form_cad_presidente #telefone").maskInput("(99) 9 9999-9999");
            $("#form_cad_presidente #rg").somenteNumero({max: 12});
            $("#form_cad_presidente #numero").somenteNumero();
            $("#form_cad_presidente #nascimento_cal").calendar({
                minDate: "-100Y",
                maxDate: "0Y",
                yearRange: "0:+10",
            });

            $("#form_cad_presidente #in_mandato_cal").calendar({
                minDate: "-10Y",
                maxDate: "6Y",
                yearRange: "0:+10",
            });

            $("#form_cad_presidente #fi_mandato_cal").calendar({
                minDate: "-10Y",
                maxDate: "6Y",
                yearRange: "0:+10",
            });
        }
    });
//------------------------------- documentos empresa ---------------------------------
    $(document).on("DOMNodeInserted", "#documentos_empresa", function(e){
        if(e.target == this){          
            $("#validade").calendar({
                minDate: "0Y",
                maxDate: "5Y",
                yearRange: "0:+10",
            });
        }
    });

    $(document).on('click','.delete-conf', function(e){
        $('.delete-conf').next(".toolip-delete").fadeOut(200);
        $(this).next(".toolip-delete").fadeIn(200);
    });

    $(document).on('click','.delete-cancel', function(e){
        $(this).parent().parent().fadeOut(200);
    });

    $(document).on('click', function(e){
        var classs = e.target.className.split(" ");
        if(classs[0] != "delete"){
            $(".toolip-delete").fadeOut(200);
        }
    });

    $(document).on("change", ".arquivo", function(e){
        $("#nome").val(e.target. files[0].name.split("."+e.target. files[0].name.split('.').pop()).shift());//pega o nome do arquivo
    });

    //------------------------------------ filial -------------------------------------------
    $(document).on("DOMNodeInserted", "#relatorio_filial", function(){
        if(e.target == this){       
            $("#data_fi").calendar({
                minDate: "-10Y",
                maxDate: "+1Y",
                yearRange: "0:+10",
            });

            $("#data_in").calendar({
                minDate: "-5Y",
                maxDate: "1Y",
                yearRange: "0:+10",
            });
        }
    });

    $(document).on("click", ".btn-status", function(){
        $('#form_auth_filial #empresa_id').val($(this).attr('data-id'));
        $('#modal_filial').modal('show');
    });

//---------------------------------- cooperado -----------------------------------------------
    $(document).on("DOMNodeInserted", "#novo_cooper", function(e){// ativar o script da pagina carregada
        if(e.target == this){
            $("#form_cad_cooperado input[type=text]").maiusculo();
            $("#form_cad_cooperado #cpf").maskInput("999.999.999-99");
            $("#form_cad_cooperado #cep").maskInput("99999-999");
            $("#form_cad_cooperado #nome").somenteLetras();
            $("#form_cad_cooperado #expedidor").somenteLetras();
            $("#form_cad_cooperado #matricula_cooperativa").somenteNumero({max:4});
            $("#form_cad_cooperado #matricula_detran").somenteNumero({max:6});
            $("#form_cad_cooperado #rg").somenteNumero({max:15});
            $("#form_cad_cooperado #data_nasc").calendar({
                minDate: "-100Y",
                maxDate: "-18Y",
                yearRange: "0:+0",
            });
            $("#form_cad_cooperado #validade_cnh").calendar({
                minDate: "0Y",
                maxDate: "10Y",
                yearRange: "0:+10",
            });
            $("#form_cad_cooperado #cnh").somenteNumero({max:13});
            $("#form_cad_cooperado #telefone").maskInput("(99) 9999-9999");
            $("#form_cad_cooperado #celular").maskInput("(99) 9 9999-9999");
            $("#form_cad_cooperado #numero").somenteNumero({max:5});            
            $("#form_cad_cooperado #marca").maiusculo();
            $("#form_cad_cooperado #modelo").maiusculo();
            $("#form_cad_cooperado #placa").maskInput("AAA-9*99");
            $("#form_cad_cooperado #cor").maiusculo();
            $("#form_cad_cooperado #placa").maiusculo();
            $("#form_cad_cooperado #renavam").somenteNumero({max:15});
            $("#form_cad_cooperado #chassi").maiusculo().semSibolos();
            $("#form_cad_cooperado #data_licen").calendar({
                minDate: "-1Y",
                maxDate: "+10Y",
                yearRange: "0:+10",
            });
            $("#form_cad_cooperado #vistoria").calendar({
                minDate: "-1Y",
                maxDate: "+10Y",
                yearRange: "0:+10",
            });
            $("#form_cad_cooperado .dValidade").calendar({
                minDate: "0Y",
                maxDate: "10Y",
                yearRange: "0:+10",
            });
        }
    }); 
    $(document).on("DOMNodeInserted", "#documentos_cooperado", function(e){
        if(e.target == this){          
            $("#validade").calendar({
                minDate: "0Y",
                maxDate: "5Y",
                yearRange: "0:+10",
            });
        }
    });

    $(document).on("DOMNodeInserted", "#relatorio_cooperado", function(e){
        if(e.target == this){       
            $("#data_fi_cal").calendar({
                minDate: "-10Y",
                maxDate: "1Y",
                yearRange: "0:+10",
            });

            $("#data_in_cal").calendar({
                minDate: "-5Y",
                maxDate: "1Y",
                yearRange: "0:+10",
            });
        }
    });

     
    $(document).on("DOMNodeInserted", "#oficio_cooper", function(e){
        if(e.target == this){          
            $("#c_data_cnh").calendar({
                minDate: "-100Y",
                maxDate: "0Y",
                yearRange: "0:+10",
            });
            $("#c_data_demissao").calendar({
                minDate: "-100Y",
                maxDate: "0Y",
                yearRange: "0:+10",
            });
            $('#pontos_cnh').somenteNumero();
        }
    });
//----------------------------------- caixa ----------------------------------------------
    $(document).on("DOMNodeInserted", "#caixas", function(e){
        if(e.target == this){          
            $("#valor").somenteValor();
            $("#recebimento_cal").calendar({
                minDate: "-1Y",
                maxDate: "0Y",
                yearRange: "0:+5",
            });
        }
    });    
    $(document).on("DOMNodeInserted", "#novo_caixa", function(e){
        if(e.target == this){          
            $("#saldo").somenteValor();
            $("#dia").calendar({
                minDate: "-5Y",
                maxDate: "0Y",
                yearRange: "0:+10",
            });
        }
    });

    $(document).on("click", ".btn-entrada-direta", function(){
        $('#caixa_id').val($(this).attr('data-id'));
    });

    $(document).on("click", ".conciliar", function(){
        $(this).actionId({rota: $(this).attr('data-reload')});
    });
    
    $(document).on("click", ".abrir", function(){
        $(this).actionId({rota: $(this).attr('data-reload')});
    });

    $(document).on("DOMNodeInserted", "#relatorio_caixa", function(){
        $("#data_fi_cal").calendar({
            minDate: "-1Y",
            maxDate: "1Y",
            yearRange: "0:+10",
        });

        $("#data_in_cal").calendar({
            minDate: "-5Y",
            maxDate: "1Y",
            yearRange: "0:+10",
        });
    });

    $(document).on("change", "#form_relatorio_caixa #tipo", function(){
        var tipo = $(this).val();
        if(tipo == 2){
            $('#div-recebimento').slideDown();
            $('#div-pagamento').hide();
            $('#type-date').slideDown();
            $('.type-date').slideDown();
            $('#div-detalhar').slideDown();
        }else if(tipo == 3){
            $('#div-recebimento').hide();
            $('#div-pagamento').slideDown();
            $('#div-vencimento').slideDown();
            $('#type-date').slideDown();
            $('.type-date').slideDown();
            $('#div-detalhar').slideDown();
        }else if(tipo == 4){      
            $('#div-recebimento').hide();
            $('#div-vencimento').hide();
            $('#div-pagamento').slideDown();
            $('#type-date').hide();
            $('#div-detalhar').hide();
        }else if(tipo == 6){
            $('#div-recebimento').hide();
            $('#div-vencimento').hide();
            $('#div-pagamento').hide();
            $('.type-date').hide();
            $('#div-detalhar').slideDown();
        }else{            
            $('#div-recebimento').hide();
            $('#div-pagamento').hide();
            $('#type-date').slideDown();
            $('#div-detalhar').slideDown();
            $('.type-date').slideDown();
        }
    });

//-------------------------------- categorias de pagamentos ------------------------------------
    $(document).on('click', '.btn_editar_categoria', function(){
        var data = $(this).val().split('|');
        $('#categoria_id').val(data[0]);
        $('#nomeEdit').val(data[1]);
        $('#tipo_id').val(data[0]);

        $('#modal_edit_categoria').modal('show');
    });

//----------------------------- contas de pagamento --------------------------------------------
    $(document).on("DOMNodeInserted", "#nova_conta_pag", function(e){
        if(e.target == this){          
            $("#valor").somenteValor();
        }
    });

//------------------------------------ pagamento  --------------------------------------------
    $(document).on("DOMNodeInserted", "#pagamento", function(e){
        if(e.target == this){          
            $("#valor").somenteValor();
            $("input[name='data_pag']").calendar({
                minDate: "-5Y",
                maxDate: "0Y",
                yearRange: "0:+10",
            });
            $("input[name='vencimento']").calendar({
                minDate: "-1Y",
                maxDate: "1Y",
                yearRange: "0:+5",
            });
        }
    });
    $(document).on("DOMNodeInserted", "#fatura", function(e){
        if(e.target == this){          
            $("#valor").somenteValor();
            $("input[name='vencimento']").calendar({
                minDate: "-1Y",
                maxDate: "1Y",
                yearRange: "0:+5",
            });
        }
    });
//----------------------------- contas de recebimento --------------------------------------------
    $(document).on("DOMNodeInserted", "#nova_conta_rec", function(e){
        if(e.target == this){          
            $("#valor").somenteValor();
        }
    });

//----------------------------- taxas empresa --------------------------------------------
    $(document).on("DOMNodeInserted", "#gerar_taxas", function(e){
        if(e.target == this){          
            $("#valor").somenteValor();
            $("#ven").calendar({
                minDate: "0Y",
                maxDate: "1Y",
                yearRange: "0:+1",
            });
        }
    });

    $(document).on("DOMNodeInserted", "#recebimento", function(e){
        if(e.target == this){          
            $("#valor").somenteValor();
            $("#valor_desconto").somenteValor();
            $("#data_venc_call").calendar({
                minDate: "-5Y",
                maxDate: "1Y",
                yearRange: "0:+1",
            });
            $("#data_receb_call").calendar({
                minDate: "-1Y",
                maxDate: "0Y",
                yearRange: "0:+1",
            });
            $(".cal_periodo").calendar({
                minDate: "-5Y",
                maxDate: "1Y",
                yearRange: "0:+5",
            });
        }
    });

    $(document).on("click", "#drop-desconto", function(){
        $("#desconto").slideToggle();
    });

    $(document).on("input", "#valor_desconto", function(){
        var valor = $("#valor").val().replace(".", "").replace(",", ".") - $(this).val().replace(".", "").replace(",", ".");
        $("#valor_com_desconto").val(Number(valor).toLocaleString('pt-BR', { style: 'decimal', minimumFractionDigits:2}));
    });
//----------------------------------- emprestimo ----------------------------------------
    $(document).on("DOMNodeInserted", "#emprestimo", function(e){
        if(e.target == this){
            $("#valor").somenteValor();
            flag = true;
        }  
    });
    $(document).on("DOMNodeInserted", "#recebimento_parcela", function(e){
        if(e.target == this){          
            $("#data_receb_call").calendar({
                minDate: "-1Y",
                maxDate: "0Y",
                yearRange: "0:+1",
            });
        }
    });
    $(document).on('change', "#form_emprestimo_cooper #qtd_parcelas", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        $.ajax({
            type:'GET',
            url: $(this).attr("data-route")+"/"+$("#form_emprestimo_cooper #valor").val()+"/"+$(this).val(),
            beforeSend: function () {
                $("#form_emprestimo_cooper #valor_parcela").addClass(" loading-input");
                $("#form_emprestimo_cooper #total").addClass(" loading-input");
            }, 
            success:function(result){   
                $("#form_emprestimo_cooper #valor_parcela").val(result['valor_parcela'].toLocaleString('pt-BR', { maximumFractionDigits: 2 }));            
                $("#form_emprestimo_cooper #total").val(result['total'].toLocaleString('pt-BR', { maximumFractionDigits: 2 }));  
            },
            complete: function(){
                $("#form_emprestimo_cooper #valor_parcela").removeClass(" loading-input");
                $("#form_emprestimo_cooper #total").removeClass(" loading-input");
            },
        });
        
    });
// ----------------------------------- taxas cooperado ----------------------------------------
    $(document).on("DOMNodeInserted", "#gerar_taxas_cooperado", function(e){
        if(e.target == this){          
            $("#valor").somenteValor();
            $("#ven").calendar({
                minDate: "-1Y",
                maxDate: "1Y",
                yearRange: "0:+1",
            });
        }
    });

    $(document).on("change", "#form_gerar_taxas_cooperado #lancamento_unico", function(){
        $("#div_vencimento").slideToggle();
        if($(this).is(':checked')){
            $("#form_gerar_taxas_cooperado #mes").attr('disabled', 'disabled');
        }else{
            $("#form_gerar_taxas_cooperado #mes").removeAttr('disabled');
        }
    })

// ----------------------------------- linhas ----------------------------------------
    $(document).on("DOMNodeInserted", "#nova_linha", function(e){// ativar o script da pagina carregada
        if(e.target == this){
            $("#form_cad_linha input[type=text]").maiusculo();
            $("#form_cad_linha #numero").somenteNumero();
            $("#form_cad_linha #tipo").somenteNumero();
            $("#form_cad_linha #especie").somenteNumero();
            $("#form_cad_linha #distancia").somenteValor();
        }
    }); 

    $(document).on("DOMNodeInserted", "#novo_seccionamento", function(e){// ativar o script da pagina carregada
        if(e.target == this){
            $("#form_seccionamento #tarifa").somenteValor();
        }
    }); 

// ----------------------------------- geral helpers ------------------------------------------
    $(document).on('submit', ".form-data", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        $(this).postFormData({rota: route});
    });

    $(document).on('submit', ".form-ajax", function(e){// usado para enviar e validar dados de qualquer formulario submetido   
        $(this).postForm({rota: route});
    });

    $(document).on('submit', ".form-redirect", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        $(this).postFormRedirect();
    });
    
    $(document).on("submit", ".form-rellot", function(){
        $(this).postFormRellot();
    });

    $(document).on('click', '.loadPage', function(){// usado para carregar um link
        route = $(this).attr("data-route");
        $(this).loadPage();        
        $(".addPicker").selectpicker();
    });

    $(document).on('click', '.loadValidate', function(){// usado para carregar um link
        route = $(this).attr("data-route");
        $(this).loadValidate();        
        $(".addPicker").selectpicker();
    });

    $(document).on('click', '.actionId', function(){// usado para ações com id, delete, alterar status ...
        $(this).actionId({rota:route});
    });

    $(document).on("keyup", "#search", function(e){
        var value = $(this).val().toLowerCase();
        $("#lista tr").filter(function(e) {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $(document).on("keyup", "#search_doc", function(e){
        var value = $(this).val().toLowerCase();
        $(".lista_doc").filter(function(e) {
            $(this).toggle($(this).attr("data-search").toLowerCase().indexOf(value) > -1)
        });
    });

    $(document).on('change', '.findVal', function(){// usado para carregar um link
        $(this).findVal();        
    });

    $(document).on('change', '.findDebito', function(){// usado para carregar um link
        $(this).findDebito();        
    });

    $(document).on('change', '.findTaxa', function(){// usado para carregar um link
        $(this).findTaxa();        
    });

    $(document).on("DOMNodeInserted", ".main-area", function(e){
        if(e.target == this){         
            $(".addPicker").selectpicker('refresh');
        }
    });

    $(document).on("click", ".actionSubmit", function(){
        $(this).actionSubmit();
    });

    $(document).on("click", ".print", function(){
        $(".rellot").printThis();
    });

    $(document).on("click", ".actionNoReload", function(){
        $(this).actionNoReload();
    });

    $('#modal_success').on('hidden.bs.modal', function(e){
        $('.modal-backdrop').remove();
    });

    $('#modal_cooperado').on('hidden.bs.modal', function(e){
        $('.modal-backdrop').remove();
    });
});