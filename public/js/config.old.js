$(document).ready(function(e){
    var myinterval = null;
    function checkSession(){
        myinterval = setInterval(function(){       
                $.getJSON("/users/check", function(result){
                    if(!result['check']){
                        $("#modal_success").find("h1").text("Atenção").addClass("text-danger");
                        $("#modal_success").find("p").text("Sua sessão foi encerrada, faça login novamente!");
                        $("#close-modal").text("LOGIN");
                        $("#modal_success").find("button").attr("onclick","document.location.reload(true);");
                        $("#modal_success").attr({
                            "data-backdrop":"static",
                            "data-keyboard":"false",
                        });
                        $("#modal_success").modal('show');
                        clear();
                    }
                    var data = new Date();                            
                    // console.log("Checkado as "+data.getHours()+":"+data.getMinutes());
                });   
        }, 60000*60);
    }

    function clear(){
        clearInterval(myinterval);
    } 
    
    checkSession();
//------------------------------------ load iframe --------------------------------------------------
    function loadIframe(){
        $(".my-iframe").on('load', function(){
            $(".loading").css("display", "none").animate({opacity: '0'});
        }).show();
    }
//------------------------------------ fim load iframe --------------------------------------------------
//--------------------------------- notify ---------------------------------------------
    $(".btn-notify").click(function(e){
        $('.notify-area').slideToggle(300);
    });

    $('.notify-area div a').click(function(e){
        if(e.target != this){
            e.preventDefault();
        }
    });

    $('.close-notify').click(function(){
        $(this).parent().animate({opacity: 0}, 500).slideUp(500);
        var cont = Number($(".btn-notify span").text())-1;
        $(".btn-notify span").text(cont);
        
        if(cont == 0){
            $(".btn-notify span").remove();
        }

        $.getJSON($(this).attr("data-route"), function(result){});  
    });

    $(document).click(function(e){
        clear();
        checkSession();

        var not = $(".notify-area");
        var div = $(".btn-notify");
        if ((!div.is(e.target) && div.has(e.target).length === 0) && (!not.is(e.target) && not.has(e.target).length === 0)){
            if($('.notify-area'). is(":visible")){               
                $('.notify-area').slideToggle(300);
            }
        }

    });

    $(".overlay-close-menu").click(function(){
        $(".btn-menu").attr('data-click-state', 0);
        $("#menu").css("margin-left", "-"+$("#menu").width()+"px");
        $("#menu").removeClass("shadow-menu");
        $(this).hide();
        $(".mobile-menu-btn-2").hide();               
    });
// ------------------------------- menu ---------------------------------------
    function toggleMenu(link=false){
        if(link){
            if($(window).width()<=576){               
                $(".btn-menu").attr('data-click-state', 0);
                $("#menu").css("margin-left", "-"+$("#menu").width()+"px");
                $("#menu").removeClass("shadow-menu");
                $(".mobile-menu-btn-2").hide();                
                $(".overlay-close-menu").hide();               
            }           
        }else{
            if($(".btn-menu").attr('data-click-state') == 1) {
                $(".btn-menu").attr('data-click-state', 0);
                $("#menu").css("margin-left", "-"+$("#menu").width()+"px");
                $("#menu").removeClass("shadow-menu");
                $(".mobile-menu-btn-2").hide();                
                $(".overlay-close-menu").hide();
            }else{
                $(".btn-menu").attr('data-click-state', 1);  
                $("#menu").css("margin-left", "0px");
                $("#menu").addClass("shadow-menu");
                if($(window).width()<=576){                
                    $(".overlay-close-menu").show();
                    $(".mobile-menu-btn-2").fadeIn('slow');                
                }
            }
        }
    }
    
    $(".drop-link").click(function(e){// ------ dropdown no menu
        $(".drop-link").removeClass("linkfocus");
        $(this).addClass("linkfocus");
        $('.drop-link').next('ul').slideUp();
        $('.drop-link').find('span i').removeClass( "fa-rotate-270" );
        if($(this).next('ul').is(':visible')){
            $(this).next('ul').slideUp();
            $(this).find('span i').removeClass( "fa-rotate-270" );
        }else{
            $(this).next('ul').slideDown();
            $(this).find('span i').addClass( "fa-rotate-270" );
        }
        
        e.preventDefault();
    });

    if($(window).width()<=576){// -------- mudar o estado do menu, aberto ou fechado
        $(".btn-menu").attr('data-click-state', 0);
        
    }else{
        $(".btn-menu").attr('data-click-state', 1);
    }
    
    $(".btn-menu").click(function(e){// -------- abrir e fechar menu
        toggleMenu();
    });

    $(window).resize(function(e){// ---------- ajustar largura e margin do menu em diferentes telas, em caso de redimencionamento 
        setTimeout(() => {
            if($(".btn-menu").attr('data-click-state') == 0) {
                $("#menu").css("margin-left", "-"+$("#menu").width()+"px");                
                $(".overlay-close-menu").hide();
                $(".mobile-menu-btn-2").hide(); 
            }else{
                $("#menu").css("margin-left", "0px");                    
                if($(window).width()<=576){// -------- mudar o estado do menu, aberto ou fechado
                    $(".overlay-close-menu").show();
                    $(".mobile-menu-btn-2").fadeIn('slow');                 
                }else{             
                    $(".overlay-close-menu").hide();
                    $(".mobile-menu-btn-2").hide(); 
                } 
            }                    
        }, 500);
    });

    $("#btn-user").focus(function(e){
        e.preventDefault();
        $(".dropdown-menu-user").fadeToggle();
    });
    $("#btn-user").blur(function(){
        $(".dropdown-menu-user").fadeOut();
    });
//---------------------------------- end menu ---------------------------------------

//-------------------------------- navegação do sistema -----------------------------
    var flag = false;// varialvel global de verificação de ativação do script dos forms carregados
    var route = "sem rota";// global, armazena rota da pagina carregada

    $(".link-append").click(function(e){// ------ dropdown no menu
        $(".drop-link").removeClass("linkfocus");
        $(this).parent().parent().prev().addClass("linkfocus");
        $(".link-append").removeClass("sublinkfocus");
        $(this).addClass("sublinkfocus");
        route = $(this).attr("data-route");
        $.ajax({
            url: route,
            beforeSend: function () {
                $("#modal_ajax_load").modal('show');
            }, 
            success: function(result){
                $("#main").hide().html(result).fadeIn(500);                
                $("#modal_ajax_load").modal("hide"); 
                toggleMenu(true);                
                $(".addPicker").selectpicker();
            },
            error : function(erro){
                var erros = [];
                if(erro.status == 401){
                    erros["title"] = "Atenção";
                    erros["text"] = "Tempo de sessão encerrado, faça login novamente.";
                }else{
                    erros = erro.responseJSON.errors;
                }
                
                $("#modal_success").find("h1").removeClass("text-success");
                $("#modal_success").find("h1").text(erros["title"]).addClass("text-danger");
                $("#modal_success").find("p").text(erros["text"]);    
                $("#modal_success").find("button").attr("onclick","document.location.reload(true);");
                $("#modal_success").attr({
                    "data-backdrop":"static",
                    "data-keyboard":"false",
                });            

                $("#modal_success").modal('show');
            },
            complete: function(e){
                $("#modal_ajax_load").modal("hide");   
            }
        });

        flag = false;
        e.preventDefault();
    });

    //---------------------------------- Abrir pagina ------------------------------------
    
    $(document).on('click', '.btn_open_page', function(e){
        e.preventDefault(); 
        var route = $(this).attr("data-route");
        $.ajax({
            url: route,
            beforeSend: function () {
                $("#modal_ajax_load").modal('show');
            }, 
            success: function(result){
                $("#main").hide().html(result).fadeIn(500);                
                $("#modal_ajax_load").modal("hide"); 
            },
            complete: function(e){
                $("#modal_ajax_load").modal("hide");   
            }
        });
    });
    //---------------------------------- fim navegação ------------------------------------

    $(document).on("click", '.border-danger', function(e){
        $(this).removeClass("border-danger");
        $(this).next("span").text("");
        // $(this).parent().parent().find("span").text("");
    });


    // ------------------------------ fim notify ------------------------------------------
    // -------------------------- cadastro empresa e usuario ----------------------------
    $(document).on("DOMNodeInserted", "#cadastro_empresa", function(e){
        if(e.target == this){
            flag = true;            
            $("#form_cad_empresa #cnpj").maskInput("99.999.999.9999/99");
            $("#form_cad_empresa #cep").maskInput("99999-999");
            $("#form_cad_empresa #telefone").maskInput("(99) 9999-9999");
            $("#form_cad_empresa #numero").maskInput("9999");
        }
    });

    $(document).on("DOMNodeInserted", "#edit_empresa", function(e){
        if(e.target == this){
            flag = true;            
            $("#edit_empresa #cnpj").maskInput("99.999.999.9999/99");
            $("#edit_empresa #cep").maskInput("99999-999");
            $("#edit_empresa #telefone").maskInput("(99) 9999-9999");
            $("#edit_empresa #numero").maskInput("9999");
        }
    });

    $(document).on('submit', "#form_cad_empresa", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false; // variavel de ativação do script dos forms carregados
        $(this).postFormData({rota: route});
    });

    $(document).on("DOMNodeInserted", "#cadastro_usuario", function(e){
        if(e.target == this){
            flag = true;
        }  
    });

    $(document).on('submit', "#form_cad_usuario", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false;// variavel de ativação do script dos forms carregados
        $(this).ativarAjax({rota: route});// ativa o ajax no form, parametro de rota para reload do form
    });

    $(document).on('submit', "#form_edit_empresa", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false;// variavel de ativação do script dos forms carregados
        $(this).postFormData({rota: route});// ativa o ajax no form, parametro de rota para reload do form
    });

    $(document).on('submit', "#form_edit_usuario", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false;// variavel de ativação do script dos forms carregados
        $(this).ativarAjax({rota: route});// ativa o ajax no form, parametro de rota para reload do form
    });
    
    $(document).on("keyup", "#search_empresa", function(e){
        var value = $(this).val().toLowerCase();
        $(".item_empresa").filter(function(e) {
            $(this).toggle($(this).attr("data-search").toLowerCase().indexOf(value) > -1)
        });
    });
//---------------------------------- fim cadastro empresa ------------------------------------

//-------------------------------- funções menu cooperado ----------------------------------
    //---------------------------------pagina novo cooperado
    $(document).on("DOMNodeInserted", "#novo_cooper", function(e){// ativar o script da pagina carregada
        if(e.target == this){
            $("#form_cad_cooperado input[type=text]").maiusculo();
            $("#form_cad_cooperado #cpf").maskInput("999.999.999-99");
            $("#form_cad_cooperado #nome").somenteLetras();
            $("#form_cad_cooperado #expedidor").somenteLetras();
            $("#form_cad_cooperado #matricula_cooperativa").somenteNumero({max:4});
            $("#form_cad_cooperado #matricula_detran").somenteNumero({max:6});
            $("#form_cad_cooperado #rg").somenteNumero({max:15});
            $("#form_cad_cooperado #data_nasc").calendar({
                minDate: "-100Y",
                maxDate: "-18Y",
                yearRange: "0:+0",
            });
            $("#form_cad_cooperado #validade_cnh").calendar({
                minDate: "0Y",
                maxDate: "10Y",
                yearRange: "0:+10",
            });
            $("#form_cad_cooperado #cnh").somenteNumero({max:13});
            $("#form_cad_cooperado #telefone").maskInput("(99) 9999-9999");
            $("#form_cad_cooperado #celular").maskInput("(99) 9 9999-9999");
            $("#form_cad_cooperado #numero").somenteNumero({max:5});            
            $("#form_cad_cooperado #marca").maiusculo();
            $("#form_cad_cooperado #modelo").maiusculo();
            $("#form_cad_cooperado #placa").maskInput("AAA-9*99");
            $("#form_cad_cooperado #cor").maiusculo();
            $("#form_cad_cooperado #placa").maiusculo();
            $("#form_cad_cooperado #renavam").somenteNumero({max:15});
            $("#form_cad_cooperado #chassi").maiusculo().semSibolos();
            $("#form_cad_cooperado #data_licen").calendar({
                minDate: "-1Y",
                maxDate: "+10Y",
                yearRange: "0:+10",
            });
            $("#form_cad_cooperado #vistoria").calendar({
                minDate: "-1Y",
                maxDate: "+10Y",
                yearRange: "0:+10",
            });
            $("#form_cad_cooperado .dValidade").calendar({
                minDate: "0Y",
                maxDate: "10Y",
                yearRange: "0:+10",
            });
            flag = true;
        }
    }); 

    $(document).on('click', "#form_cad_cooperado .btn-plus-file", function(e){
        var uniqueName = Math.random().toString(36).substr(2, 6);
        var divs = '<div class="col-12 form-row p-0 slid_'+uniqueName+'" style="display: none;">\
            <div class="form-group col-md-3">\
             <label for="nomeIdentificacao_'+uniqueName+'">Nome para identificação</label>\
             <input type="text" id="nomeIdentificacao_'+uniqueName+'" name="nomeIdentificacao_'+uniqueName+'" class="form-control" placeholder="Ex: Copia RG..." autocomplete="off">\
             </div>\
             <div class="form-group col-md-3">\
            <label for="dValidade'+uniqueName+'">Validade</label>\
            <div class="input-group">\
            <input type="text" id="dValidade'+uniqueName+'" name="dValidade'+uniqueName+'" class="form-control validade" autocomplete="off">\
                <div class="input-group-append">\
                    <label class="input-group-text pointer" for="dValidade'+uniqueName+'"><i class="far fa-calendar-alt" aria-hidden="true"></i></label>\
                </div>\
            </div>\
            </div> \
            <div class="form-group col-md-6">\
            <label for="arquivoId_'+uniqueName+'">Arquivo</label>\
            <div class="input-group">\
            <input type="file" id="arquivoId_'+uniqueName+'" name="arquivo_'+uniqueName+'" data-id="nomeIdentificacao_'+uniqueName+'"style="padding-top: .2em; padding-bottom: .22em;" class="form-control inpt-doc" autocomplete="off">\
                    <div class="input-group-append">\
                        <label class="input-group-text pointer btn-plus-file" title="Click para adicionar outro arquivo">\
                            <i class="fa fa-plus" aria-hidden="true"></i>\
                        </label>\
                    </div>\
                </div>\
                <span id="arquivo_'+uniqueName+'"></span>\
            </div></div>';
            $(this).parent().parent().parent().parent().after(divs); 
            $(this).after('<label class="input-group-text pointer bg-danger text-white btn-remove-file" title="Click para remover este arquivo"><i class="far fa-trash-alt" aria-hidden="true"></i></label>');
            $('.slid_'+uniqueName).slideDown(300);
            $(this).remove(); 
            
            $("#form_cad_cooperado #dValidade"+uniqueName).calendar({
                minDate: "0Y",
                maxDate: "10Y",
                yearRange: "0:+10",
            });

    });

    $(document).on('click', "#form_cad_cooperado .btn-remove-file", function(e){
        $(this).parent().parent().parent().parent().slideUp(300, function(e){
            $(this).remove();
        });
    });

    $(document).on('submit', "#form_cad_cooperado", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false; 
        e.preventDefault();

        if($("#ano_fab").val() > $("#ano_modelo").val()){
            $("#ano_fab").focus().next("span").remove();
            $("#ano_fab").addClass("border-danger").after('<span class="text-danger">Ano de fabricação maior que ano modelo!</span>');
        }else{
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: $(this).attr("action"),
                method:'POST',
                data: new FormData(this),
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $("#modal_ajax_load").modal("show");
                }, 
                success:function(result){  
                    $("#modal_success").find("h1").removeClass("text-danger");
                    $("#modal_success").find("h1").addClass("text-success");
                    $("#modal_success").find("h1").text(result["title"]);
                    $("#modal_success").find("p").text(result["text"]);
                    $("#main").load(route);
                    $("#modal_success").modal('show');  
                },
                error: function (err) {
                    var erros = err.responseJSON.errors;
                    if(typeof erros["title"]!='undefined'){
                        $("#modal_success").find("h1").removeClass("text-success");
                        $("#modal_success").find("h1").text(erros["title"]).addClass("text-danger");
                        $("#modal_success").find("p").text(erros["text"]);                
                        $("#modal_success").modal('show');
                    }else{
                        for(key in erros){
                            $("#"+key).next("span").remove();
                            $("#"+key).after('<span class="text-danger">'+erros[key]+'</span>');
                            $("#"+key).addClass("border-danger");
                        }  
                    }              
                },
                complete: function(e){
                    $("#modal_ajax_load").modal("hide");  
                }
            });
        }
    });

    //---------------------------------pagina listar cooperado ----------------------------
    $(document).on("click", ".btn_doc_cooper", function(e){
        var editRoute = $(this).attr("data-route");
         $.ajax({
             url: editRoute,
             beforeSend: function () {
                 $("#modal_ajax_load").modal('show');
             }, 
             success: function(result){
                 $("#main").hide().html(result).fadeIn(500);                
                 $("#modal_ajax_load").modal("hide"); 
             },
             complete: function(e){
                 $("#modal_ajax_load").modal("hide");   
             }
         });
    });

    $(document).on("DOMNodeInserted", "#lista_cooper", function(e){
        $("#novo_veiculo #marca").maiusculo();
        $("#novo_veiculo #modelo").maiusculo();
        $("#novo_veiculo #placa").maskInput("AAA-9*99");
        $("#novo_veiculo #cor").maiusculo();
        $("#novo_veiculo #placa").maiusculo();
        $("#novo_veiculo #renavam").somenteNumero({max:15});
        $("#novo_veiculo #condicao").change(function(){
            if($(this).val()!=""){                
                $("#novo_veiculo #cod").maiusculo();
                $("#novo_veiculo #cod").removeAttr("disabled");
            }else{
                $("#novo_veiculo #cod").attr("disabled", "disabled");
            }
        });
    });

    $(document).on("click", ".btn_substituicao", function(e){
        var editRoute = $(this).attr("data-route");        
        $(".debito-ativo").hide();                    
        $("#novo_veiculo").hide();
        $.ajax({
            url: editRoute,
            beforeSend: function () {
                $("#modal_ajax_load").modal('show');
            }, 
            success: function(result){             
                $("#modal_ajax_load").modal("hide"); 
                if(result['debito'] == 1){
                    $(".debito-ativo").show();                    
                    $("#novo_veiculo").hide();
                    $("#route-debito").attr("href", result['route']);
                }else{
                    $("#novo_veiculo #id_cooperado").val(result['id']);
                    $(".debito-ativo").hide();                    
                    $("#novo_veiculo").show();
                }    
                $("#modal_veiculo").modal("show");
            },
            complete: function(e){
                $("#modal_ajax_load").modal("hide");   
            }
        });
    });

    $(document).on('click', "#modal_veiculo a", function(e){ 
        $("#modal_veiculo").modal("hide");
    });

    $(document).on('submit', "#novo_veiculo", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        $("#modal_veiculo").modal("hide");
    });

    $(document).on("DOMNodeInserted", "#form_editar_cooper", function(e){
        if(e.target == this){
            $("#form_editar_cooper input[type=text]").maiusculo();
            $("#form_editar_cooper #cpf").maskInput("999.999.999-99");
            $("#form_editar_cooper #nome").somenteLetras();
            $("#form_editar_cooper #matricula_cooperativa").somenteNumero({max:4});
            $("#form_editar_cooper #matricula_detran").somenteNumero({max:6});
            $("#form_editar_cooper #rg").somenteNumero({max:15});
            $("#form_editar_cooper #data_nasc").calendar({
                minDate: "-100Y",
                maxDate: "-18Y",
                yearRange: "0:+0",
            });            
            $("#form_editar_cooper #validade_cnh").calendar({
                minDate: "0Y",
                maxDate: "10Y",
                yearRange: "0:+10",
            });
            $("#form_editar_cooper #cnh").somenteNumero({max:13});
            $("#form_editar_cooper #telefone").maskInput("(99) 9999-9999");
            $("#form_editar_cooper #celular").maskInput("(99) 9 9999-9999");
            $("#form_editar_cooper #numero").somenteNumero({max:5});            
            $("#form_editar_cooper #marca").maiusculo();
            $("#form_editar_cooper #modelo").maiusculo();
            $("#form_editar_cooper #placa").maskInput("AAA-9*99");
            $("#form_editar_cooper #cor").maiusculo();
            $("#form_editar_cooper #placa").maiusculo();
            $("#form_editar_cooper #renavam").somenteNumero({max:15});
            $("#form_editar_cooper #chassi").maiusculo().semSibolos();
            $("#form_editar_cooper #data_licen").calendar({
                minDate: "-1Y",
                maxDate: "+10Y",
                yearRange: "0:+10",
            });
            $("#form_editar_cooper #vistoria").calendar({
                minDate: "-1Y",
                maxDate: "+10Y",
                yearRange: "0:+10",
            });
            $("#form_editar_cooper .dValidade").calendar({
                minDate: "0Y",
                maxDate: "10Y",
                yearRange: "0:+10",
            });

            flag = true;
        }
    });   

    $(document).on("keyup", "#search", function(e){
        var value = $(this).val().toLowerCase();
        $(".lista tr").filter(function(e) {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
        // editar cooperado
    $(document).on('submit', "#form_editar_cooper", function(e){ //usado para enviar e validar dados de qualquer formulario submetido   
        flag = false;// variavel de ativação do script dos forms carregados
        e.preventDefault();
        
        if($("#ano_fab").val() > $("#ano_licen").val()){
            $("#ano_fab").focus().next("span").remove();
            $("#ano_fab").addClass("border-danger").after('<span class="text-danger">Ano de fabricação maior que de licenciamento!</span>');
        }else{
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: $(this).attr("action"),
                method:'POST',
                data: new FormData(this),
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $("#modal_ajax_load").modal("show");
                }, 
                success:function(result){  
                    $("#modal_success").find("h1").removeClass("text-danger");
                    $("#modal_success").find("h1").addClass("text-success");
                    $("#modal_success").find("h1").text(result["title"]);
                    $("#modal_success").find("p").text(result["text"]);
                    $("#main").load(route);
                    $("#modal_success").modal('show');  
                },
                error: function (err) {
                    var erros = err.responseJSON.errors;
                    if(typeof erros["title"]!='undefined'){
                        $("#modal_success").find("h1").removeClass("text-success");
                        $("#modal_success").find("h1").text(erros["title"]).addClass("text-danger");
                        $("#modal_success").find("p").text(erros["text"]);                
                        $("#modal_success").modal('show');
                    }else{
                        for(key in erros){
                            $("#"+key).next("span").remove();
                            $("#"+key).after('<span class="text-danger">'+erros[key]+'</span>');
                            $("#"+key).addClass("border-danger");
                        }  
                    }              
                },
                complete: function(e){
                    $("#modal_ajax_load").modal("hide");  
                }
            });
        }
    });

    $(document).on('click', "#form_editar_cooper input, #form_editar_cooper select", function(e){
        $(this).next("span").remove();
        $(this).removeClass("border-danger");
    });

    $(document).on("click", ".btn_editar_cooper", function(e){
        e.preventDefault();
        var editRoute = $(this).attr("data-route");
        $.ajax({
            url: editRoute,
            beforeSend: function () {
                $("#modal_ajax_load").modal('show');
            }, 
            success: function(result){
                $("#main").hide().html(result).fadeIn(500);                
                $("#modal_ajax_load").modal("hide"); 
            },
            complete: function(e){
                $("#modal_ajax_load").modal("hide");   
            }
        });
    });

    $(document).on("click", "#btn-edit-cancel", function(e){
        $.ajax({
            url: route,
            beforeSend: function () {
                $("#modal_ajax_load").modal('show');
            }, 
            success: function(result){
                $("#main").hide().html(result).fadeIn(500);                
                $("#modal_ajax_load").modal("hide"); 
            },
            complete: function(e){
                $("#modal_ajax_load").modal("hide");   
            }
        });  
    });
    
    $(document).on("click", "#btn-edit-veiculo", function(e){
        if($("#edit-veiculo").find("input#modelo").attr('disabled')){
            $("#edit-veiculo").slideDown().find("input, select").removeAttr('disabled');
            $(this).find("i").remove();
            $(this).append("<span>&times;</span>").removeClass("btn-light").addClass("btn-danger");
        }else{
            $("#edit-veiculo").slideUp().find("input, select").attr('disabled', "disabled");            
            $(this).find("span").remove();
            $(this).append("<i class='far fa-edit' aria-hidden='true'></i>").removeClass("btn-danger").addClass("btn-light");
        }
    });
        // ativar/desativar cooperado
    $(document).on("click", ".btn_status_cooper", function(e){
        $(this).toggleClass("bg-light");        
        $(this).actionId({rota: route});
    });

        // emprestimo cooperado
    $(document).on("DOMNodeInserted", "#emprestimo", function(e){
        if(e.target == this){
            $("#valor").somenteValor();
            $(".cooperado").selectpicker();
            flag = true;
        }  
    });

    $(document).on('change', "#form_emprestimo_cooper #parcelas", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        $.ajax({
            type:'GET',
            url: $(this).attr("data-route")+"/"+$("#form_emprestimo_cooper #valor").val()+"/"+$(this).val(),
            beforeSend: function () {
                $("#form_emprestimo_cooper #valor_parcela").addClass(" loading-input");
                $("#form_emprestimo_cooper #total").addClass(" loading-input");
            }, 
            success:function(result){   
                $("#form_emprestimo_cooper #valor_parcela").val(result['valor_parcela'].toLocaleString('pt-BR', { maximumFractionDigits: 2 }));            
                $("#form_emprestimo_cooper #total").val(result['total'].toLocaleString('pt-BR', { maximumFractionDigits: 2 }));  
            },
            complete: function(){
                $("#form_emprestimo_cooper #valor_parcela").removeClass(" loading-input");
                $("#form_emprestimo_cooper #total").removeClass(" loading-input");
            },
        });
        
    });

    $(document).on('submit', "#form_emprestimo_cooper", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false;// variavel de ativação do script dos forms carregados
        $(this).submitRedirect({rota: route});
    });

    // gerar taxas para cooperados
    $(document).on("DOMNodeInserted", "#gerar_taxas", function(e){
        if(e.target == this){
            $("#form_gerar_taxas #cooperado").selectpicker();
            $("#form_gerar_taxas #valor").somenteValor();            
            $("#form_gerar_taxas #data_lanc").calendar({
                minDate: "-2Y",
                maxDate: "5Y",
                yearRange: "0:+5",
            });
        }  
    });

    $(document).on("click", "#form_gerar_taxas #allCooper", function(e){
        if($(this).attr("data-click") == 1){
            $("#form_gerar_taxas .dropdown-toggle").attr("disabled", "disabled");
            $("#form_gerar_taxas .cooperado").attr("disabled", "disabled");
            $(this).attr("data-click", 0);
        }else{
            $("#form_gerar_taxas .dropdown-toggle").removeAttr("disabled"); 
            $("#form_gerar_taxas .cooperado").removeAttr("disabled"); 
            $("#form_gerar_taxas .cooperado").selectpicker('refresh');
            $(this).attr("data-click", 1);
        }
    });

    $(document).on("click", "#form_gerar_taxas #unico", function(e){
        if($(this).attr("data-click") == 1){
            $("#form_gerar_taxas #div_data").slideDown(); 
            $("#form_gerar_taxas #mes").attr("disabled", "disabled");
            $(this).attr("data-click", 0);
        }else{
            $("#form_gerar_taxas #div_data").slideUp(); 
            $("#form_gerar_taxas #mes").removeAttr("disabled"); 
            $(this).attr("data-click", 1);
        }
    });

    $(document).on("change", "#form_gerar_taxas #id_conta_receb", function(e){
        if($(this).val() != ""){
            $.getJSON($(this).attr("data-route")+"/"+$(this).val(), function(result){
                var conta = result['conta'];
                $("#valor").val(conta.valor);
            });
        }else{
            $("#valor").val("");
        }
    });

    $(document).on('submit', "#form_gerar_taxas", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false;// variavel de ativação do script dos forms carregados
        $(this).ativarAjax({rota: route});// ativa o ajax no form, parametro de rota para reload do form
    });
    //------------------- pagina documentos cooperado -----------------------------
    
    $(document).on("keyup", "#search_doc", function(e){
        var value = $(this).val().toLowerCase();
        $(".lista_doc").filter(function(e) {
            $(this).toggle($(this).attr("data-search").toLowerCase().indexOf(value) > -1)
        });
    });

    $(document).on('click', "#form_cad_documento .btn-plus-file", function(e){
        var uniqueName = Math.random().toString(36).substr(2, 6);
        var divs = '<div class="col-12 row slid_'+uniqueName+'" style="display: none;">\
            <div class="form-group col-md-3">\
             <label for="nomeIdentificacao_'+uniqueName+'">Nome</label>\
             <input type="text" id="nomeIdentificacao_'+uniqueName+'" name="nomeIdentificacao_'+uniqueName+'" class="form-control" placeholder="Ex: Copia RG..." autocomplete="off">\
             </div>\
             <div class="form-group col-md-3">\
            <label for="dValidade'+uniqueName+'">Validade</label>\
            <div class="input-group">\
            <input type="text" id="dValidade'+uniqueName+'" name="dValidade'+uniqueName+'" class="form-control validade" autocomplete="off">\
                <div class="input-group-append">\
                    <label class="input-group-text pointer" for="dValidade'+uniqueName+'"><i class="far fa-calendar-alt" aria-hidden="true"></i></label>\
                </div>\
            </div>\
            </div> \
            <div class="form-group col-md-6">\
            <label for="arquivo_'+uniqueName+'">Arquivo</label>\
            <div class="input-group">\
            <input type="file" id="arquivo_'+uniqueName+'" name="arquivo_'+uniqueName+'" data-id="nomeIdentificacao_'+uniqueName+'" class="form-control inpt-doc" style="padding-top: .2em; padding-bottom: .22em;" autocomplete="off">\
                    <div class="input-group-append">\
                        <label class="input-group-text pointer btn-plus-file" title="Click para adicionar outro arquivo">\
                            <i class="fa fa-plus" aria-hidden="true"></i>\
                        </label>\
                    </div>\
                </div>\
            </div></div>';
            $(this).parent().parent().parent().parent().after(divs); 
            $(this).after('<label class="input-group-text pointer bg-danger text-white btn-remove-file" title="Click para remover este arquivo"><i class="far fa-trash-alt" aria-hidden="true"></i></label>');
            $('.slid_'+uniqueName).slideDown(300);
            $(this).remove(); 
            
            $("#form_cad_documento #dValidade"+uniqueName).calendar({
                minDate: "0Y",
                maxDate: "10Y",
                yearRange: "0:+10",
            });

    });

    $(document).on('click', "#form_cad_documento .btn-remove-file", function(e){
        $(this).parent().parent().parent().parent().slideUp(300, function(e){
            $(this).remove();
        });
    });

    $(document).on('click', ".lista_doc .deletar-doc", function(e){            
        e.preventDefault();
        $(this).actionId({rota:$(this).attr("data-reload")});
    });

    $(document).on('hidden.bs.modal', '#modal_documento', function(e){
        document.getElementById("form_cad_documento").reset();       
    });

    $(document).on('DOMNodeInserted', '#documentos_cooper', function(e){
        if(e.target == this){            
            $("#form_cad_documento .dValidade").calendar({
                minDate: "0Y",
                maxDate: "10Y",
                yearRange: "0:+10",
            });
            flag = true;
        }        
    });

    $(document).on("change", ".inpt-doc", function(e){
        $("#"+$(this).attr("data-id")).val(e.target. files[0].name.split("."+e.target. files[0].name.split('.').pop()).shift());//pega o nome do arquivo
    
    });

    $(document).on("submit", "#form_cad_documento", function(e){        
        e.preventDefault();
        route = $(this).attr("data-route");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: $(this).attr("action"),
            method:'POST',
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $("#modal_ajax_load").modal("show");
            }, 
            success:function(result){    
                $("#modal_success").find("h1").removeClass("text-danger");
                $("#modal_success").find("h1").addClass("text-success");
                $("#modal_success").find("h1").text(result["title"]);
                $("#modal_success").find("p").text(result["text"]);
                $("#main").load(route);                  
                $("#modal_success").modal('show');
                
                flag = false; 
            },
            error: function (err) {
                var erros = err.responseJSON.errors;
                for(key in erros){
                    $("#"+key).parent().next("span").remove();
                    $("#"+key).parent().after('<span class="text-danger">'+erros[key]+'</span>');
                    $("#"+key).parent().addClass("border-danger");
                }            
                setTimeout(function(e){                                                                              
                    $("#modal_documento").modal("show");
                }, 200);                              
            },
            complete: function(e){
                $("#modal_ajax_load").modal("hide");  
            }
        });
        $(this).find('input').click(function(e){
            $(this).removeClass("border-danger");
            $(this).parent().next("span").remove();
        });
    });

    $(document).on('click','.delete-conf', function(e){
        $('.delete-conf').next(".toolip-delete").fadeOut(200);
        $(this).next(".toolip-delete").fadeIn(200);
    });

    $(document).on('click','.delete-cancel', function(e){
        $(this).parent().parent().fadeOut(200);
    });

    $(document).on('click', function(e){
        var classs = e.target.className.split(" ");
        if(classs[0] != "delete"){
            $(".toolip-delete").fadeOut(200);
        }
    });
    //------------------- pagina relatorios cooperado -----------------------------
    $(document).on("DOMNodeInserted", "#relatorios_cooper", function(e){
        if(e.target == this){
            $("#relatorios_cooper .periodo").calendar({
                minDate: "-100Y",
                maxDate: "+100Y",
                yearRange: "0:+100",
            });
            $("#relatorios_cooper .cooperado").selectpicker();  
            loadIframe();        
        }  
    });

    $(document).on("click", "#relatorios_cooper #tudo", function(e){
        if($(this).attr("data-click") == 1){
            $("#relatorios_cooper .periodo").attr("disabled", "disabled");
            $(this).attr("data-click", 0);
        }else{
            $("#relatorios_cooper .periodo").removeAttr("disabled");
            $(this).attr("data-click", 1);
        }
    });

    $(document).on("submit", "#relatorios_cooper #confirme-delete", function(e){
        e.preventDefault();
        var dados = $(this).serializeArray();
        $.ajaxSetup({
            headers: {
                'X-XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: $(this).attr("action"),
            data: dados,
            beforeSend: function () {                
                $("#modal_ajax_load").modal("show");
            }, 
            success:function(result){   
                var taxa = result["taxa"]; 
                if(taxa){
                    $("#no-taxa").hide();
                    $("#form-deletar-taxa").show();
                    $("#form-deletar-taxa #id_taxa").val(taxa.id_taxa);
                    $("#form-deletar-taxa #cooperado").text(taxa.cooperado);
                    $("#form-deletar-taxa #conta").text(taxa.conta);
                    $("#form-deletar-taxa #vencimento").text(taxa.vencimento);
                    $("#form-deletar-taxa #valor").text(taxa.valor);
                    $("#form-deletar-taxa #observacao").text(taxa.observacao);
                    if(taxa.observacao != ""){
                        $("#form-deletar-taxa #observacao").text(taxa.observacao);
                    }else{
                        $("#form-deletar-taxa #observacao").text("sem observação");
                    }
                }
                $("#modal_dell_taxa").modal("show");
            },
            error: function (err) {
                var erros = err.responseJSON.errors;
                for(key in erros){
                    $("#"+key).next("span").remove();
                    $("#"+key).after('<span class="text-danger">'+erros[key]+'</span>');
                    $("#"+key).addClass("border-danger");
                } 
                $("#modal_ajax_load").modal("hide");
            },
            complete: function(){                
                $("#modal_ajax_load").modal("hide");
            },
        });
    });

    $(document).on("submit", "#relatorios_cooper #form-deletar-taxa", function(e){
        flag = false;// variavel de ativação do script dos forms carregados 
        $(this).ativarAjax({
            extModal: "#modal_dell_taxa",//fechar o modal do form ou abrir caso erro
        });

        setTimeout(function(){
            $("#form_relatorios_cooper").submit();
        },1000);
    });
//-------------------------- fim funções cooperado ------------------------------------

//--------------------------------- funções contas a receber ------------------------------------
    $(document).on('click', '.btn-edit', function(e){
        $(this).loadPage(route);
    });

    $(document).on('click', '.btn_nova_conta', function(e){
        e.preventDefault(); 
        var route = $(this).attr("data-route");
        $.ajax({
            url: route,
            beforeSend: function () {
                $("#modal_ajax_load").modal('show');
            }, 
            success: function(result){
                $("#main").hide().html(result).fadeIn(500);                
                $("#modal_ajax_load").modal("hide"); 
            },
            complete: function(e){
                $("#modal_ajax_load").modal("hide");   
            }
        });
    });

    $(document).on('click', '.btn_navigate', function(e){
        e.preventDefault(); 
        var r = $(this).attr("data-route");
        $.ajax({
            url: r,
            beforeSend: function () {
                $("#modal_ajax_load").modal('show');
            }, 
            success: function(result){
                $("#modal_ajax_load").modal("hide");
                if(typeof result['showModal'] != 'undefined' && result['showModal']){                    
                    $("#modal_success").find("h1").removeClass("text-danger");
                    $("#modal_success").find("h1").addClass("text-success");
                    $("#modal_success").find("h1").text(result["title"]);
                    $("#modal_success").find("p").text(result["text"]);
                    $("#modal_success").modal("show"); 
                    $("#main").load(route);               

                }else{
                    $("#main").hide().html(result).fadeIn(500);               
                }

            },
            error: function(err){
                var erros = err.responseJSON.errors;
                if(typeof erros["title"]!='undefined'){
                    $("#modal_success").find("h1").removeClass("text-success");
                    $("#modal_success").find("h1").text(erros["title"]).addClass("text-danger");
                    $("#modal_success").find("p").text(erros["text"]);                
                    $("#modal_success").modal("show");
                }
            },
            complete: function(e){
                $("#modal_ajax_load").modal("hide");   
            }
        });
    });
    // pagina recebimento    
    $(document).on("DOMNodeInserted", "#recebimento", function(e){// ativar o script da pagina carregada        
        if(e.target == this){
            $("#form_receber #valor").somenteValor();
            $("#form_receber .cooperado").selectpicker();
            $(".addPicker").selectpicker();
            $("#form_receber #data_receb_call").calendar({
                dateFormat: "dd/mm/yy",
                minDate: "-10Y",
                maxDate: "0Y",
                yearRange: "0:+0",
            });
            
            $("#form_receber #data_venc_call").calendar({
                dateFormat: "dd/mm/yy",
                minDate: "-10Y",
                maxDate: "10Y",
                yearRange: "0:+10",
            });
            flag = true;
        }       
    });

    $(document).on("change", "#form_receber #cooperado", function(e){
        $.ajax({
            type:'GET',
            url: $(this).attr("data-route")+"/"+$(this).val(),
            beforeSend: function () {
                $("#form_receber #id_taxa").addClass(" loading-input");
            }, 
            success:function(result){   
                var dados = result["dados"];
                var after = "<option value=''>Selecione</option>";
                for(key in dados){
                    after += "<option value='"+dados[key].id_taxa+"'>"+dados[key].conta.nome+" - Valor devido: "+dados[key].valor.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })+"</option>";
                }
                $("#form_receber #id_taxa").find("option").remove();
                $("#form_receber #id_taxa").append(after)  
            },
            complete: function(){
                $("#form_receber #id_taxa").removeClass(" loading-input"); 
            },
        });
            $("#form_receber #valor").val("");                
            $("#form_receber #valor").attr("disabled", "disabled");
    });

    $(document).on("change", "#form_receber #filial", function(e){
        $.ajax({
            type:'GET',
            url: $(this).attr("data-route")+"/"+$(this).val(),
            beforeSend: function () {
                $("#form_receber #id_taxa").addClass(" loading-input");
            }, 
            success:function(result){   
                var dados = result["dados"];
                var after = "<option value=''>Selecione</option>";
                for(key in dados){
                    after += "<option value='"+dados[key].id_taxa+"'>"+dados[key].conta.nome+" - Valor devido: "+dados[key].valor.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })+"</option>";
                }
                $("#form_receber #id_taxa").find("option").remove();
                $("#form_receber #id_taxa").append(after)  
            },
            complete: function(){
                $("#form_receber #id_taxa").removeClass(" loading-input"); 
            },
        });
            $("#form_receber #valor").val("");                
            $("#form_receber #valor").attr("disabled", "disabled");
    });

    $(document).on("change", "#form_receber #id_taxa", function(e){
        $.ajax({
            type:'GET',
            url: $("#form_receber #id_taxa").attr("data-route")+"/"+$("#form_receber #id_taxa").val(),
            beforeSend: function () {
                $("#form_receber #valor").addClass(" loading-input");
            }, 
            success:function(result){   
                if(result['taxa'].emprestimo){
                    $("#form_receber #valor").removeAttr("disabled");
                }else{                
                    $("#form_receber #valor").attr("disabled", "disabled");
                }
                $("#form_receber #valor").val(result['taxa'].valor);    
            },
            complete: function(){
                $("#form_receber #valor").removeClass(" loading-input"); 
            },
        });
    });

    $(document).on('submit', "#form_receber", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false;// variavel de ativação do script dos forms carregados 
        $("#form_receber #valor").removeAttr("disabled");
        $(this).ativarAjax({rota: route});// ativa o ajax no form, parametro de rota para reload do form
    });

    // pagina nova conta recebimento
    $(document).on("DOMNodeInserted", "#nova_conta_rec", function(e){// ativar o script da pagina carregada        
        if(e.target == this){
            $("#form_cad_conta_rec #valor").somenteValor();
            flag = true;
        }       
    });

    $(document).on('submit', "#form_cad_conta_rec", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false;// variavel de ativação do script dos forms carregados
        $(this).ativarAjax({rota: route});// ativa o ajax no form, parametro de rota para reload do form
    });

    $(document).on('DOMNodeInserted', "#form_edit_conta_rec", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        if(e.target == this){
            $("#form_edit_conta_rec #nome").somenteLetras();
            $("#form_edit_conta_rec #valor").somenteValor();
            flag = true;
        } 
    });
    
    $(document).on('submit', "#form_edit_conta_rec", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false;// variavel de ativação do script dos forms carregados
        $(this).ativarAjax({rota: route});// ativa o ajax no form, parametro de rota para reload do form
    });
    // pagina pagamento
    $(document).on("DOMNodeInserted", "#pagamento", function(e){// ativar o script da pagina carregada        
        if(e.target == this){
            $("#for_pagamento #valor").somenteValor();            
            $("#for_pagamento #id_conta_pag").selectpicker();
            $("#for_pagamento #data_pag").calendar();
            flag = true;
        }       
    });

    $(document).on("change", "#id_conta_pag", function(e){
        $.getJSON($(this).attr("data-route")+"/"+ $(this).val(), function(result){
            $("#for_pagamento #valor").val(result['conta'].valor);
        });
    });

    $(document).on('submit', "#for_pagamento", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false;// variavel de ativação do script dos forms carregados
        $(this).ativarAjax({rota: route});// ativa o ajax no form, parametro de rota para reload do form
    });

    // pagina nova conta pagamento
    $(document).on("DOMNodeInserted", "#nova_conta_pag", function(e){// ativar o script da pagina carregada        
        if(e.target == this){
            $("#form_nova_pag #nome").somenteLetras();
            $("#form_nova_pag #valor").somenteValor();
            flag = true;
        }       
    });

    $(document).on('submit', "#form_nova_pag", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false;// variavel de ativação do script dos forms carregados
        $(this).ativarAjax({rota: route});// ativa o ajax no form, parametro de rota para reload do form
    });

    $(document).on('DOMNodeInserted', "#form_edit_pag", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        if(e.target == this){
            $("#form_edit_pag #nome").somenteLetras();
            $("#form_edit_pag #valor").somenteValor();
            flag = true;
        } 
    });
    
    $(document).on('submit', "#form_edit_pag", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false;// variavel de ativação do script dos forms carregados
        $(this).ativarAjax({rota: route});// ativa o ajax no form, parametro de rota para reload do form
    });
    
//------------------------------ fim funções contas a receber ------------------------------------

//--------------------------------- funções controle caixa ---------------------------------------
    $(document).on("DOMNodeInserted", "#caixas", function(e){// ativar o script da pagina carregada        
        if(e.target == this){
            $("#form_entrada_direta #valor").somenteValor();
            $("#form_entrada_direta #data_entrada").calendar({                    
                minDate: "-1Y",
                maxDate: "0Y",
                yearRange: "0:+0",
            });
            flag = true;
        }       
    });

    $(document).on("click", ".btn-entrada-direta", function(e){
        $("#form_entrada_direta #id_caixa").val($(this).attr("data-id"));
    });

    $(document).on("submit", "#form_entrada_direta", function(e){
        flag = false;// variavel de ativação do script dos forms carregados
        $(this).ativarAjax({
            rota: route,
            extModal : '#modal_entrada'
        });// ativa o ajax no form, parametro de rota para reload do form
        $("#modal_entrada").modal("hide");
    });

    $(document).on("click", "#btn-fechar", function(e){ 
        $("#modal_ajax_load").modal("show");
        $(".main-area").fadeOut();
        setTimeout(function(e){
            $("#modal_ajax_load").modal("hide");
        }, 600);      
        $("#iframe-table").attr("src", $(this).attr("data-route"));
        $("#controles-caixa").fadeIn();
        $("#id_caixa").val($(this).attr("data-id"));
        $("#btn-conciliar").attr("data-route", $(this).attr('data-route-conciliar'));
    });

    $(document).on("submit", "#form-busca-lancamento", function(e){ 
        e.preventDefault();
        if($(this).find("#id_lancamento").val() != "" && $(this).find("#tipo").val() != ""){
            var dados = $(this).serializeArray();
            $.ajaxSetup({
                headers: {
                    'X-XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                dataType: 'json',
                type:'POST',
                url: $(this).attr("action"),
                data: dados,
                beforeSend: function () {
                    $("#modal_ajax_load").modal("show");
                }, 
                success: function(result){                    
                    $("#div_cooper").fadeOut();
                    
                    if(result['tipo']==1){
                        $("#div_cooper").fadeIn();
                        $("#Tcooperado").text(result['cooperado']);   
                        $("#id_taxa").val(result['taxa'].id);
                        $("#Tvalor").text(result['taxa'].valor);
                        $("#Tvencimento").text(result['taxa'].mes);
                        if(result['taxa'].observacao != ""){
                            $("#Tobservacao").text(result['taxa'].observacao);
                        }else{
                            $("#Tobservacao").text("sem observação");
                        }
                    }else if(result['tipo']==5){
                        $("#Tvalor").text(result['valor']);
                        $("#Tvencimento").text(result['data']);
                        if(result['observacao'] != ""){
                            $("#Tobservacao").text(result['observacao']);
                        }else{
                            $("#Tobservacao").text("sem observação");
                        }
                        $("#Tconta").text(result['conta']); 
                        $("#id_registro").val(result['id_registro']);
                        $("#tipoR").val(result['tipo']);
                    }else{
                        $("#div_cooper").fadeOut();
                        $("#Tvalor").text(result['saida'].valor);
                        $("#Tvencimento").text(result['saida'].data_pag);
                        if(result['saida'].observacao != ""){
                            $("#Tobservacao").text(result['saida'].observacao);
                        }else{
                            $("#Tobservacao").text("sem observação");
                        } 
                    }
                    $("#Tconta").text(result['conta']); 
                    $("#id_registro").val(result['id_registro']);
                    $("#tipoR").val(result['tipo']);

                    $("#modal_corrigir").modal("show");
                },
                error : function(erro){
                    var erros = erro.responseJSON.errors;
                    for(key in erros){
                        $("#"+key).next("span").remove();
                        $("#"+key).after('<span class="text-danger">'+erros[key]+'</span>');
                        $("#"+key).addClass("border-danger");
                    }                      
                    $("#modal_corrigir").modal("hide"); 
                },
                complete: function(e){
                    $("#modal_ajax_load").modal("hide");  
                }
            }, "json");
        }
    });

    $(document).on("submit", "#excluir-reg", function(e){
        e.preventDefault();
        var dados = $(this).serializeArray();
            $.ajaxSetup({
                headers: {
                    'X-XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                dataType: 'json',
                type:'POST',
                url: $(this).attr("action"),
                data: dados,
                beforeSend: function () {
                    $("#modal_ajax_load").modal("show");
                }, 
                success: function(result){
                    $("#modal_success").find("h1").removeClass("text-danger").text(result["title"]).addClass("text-success");
                    $("#modal_success").find("p").text(result["text"]);    
                    $("#modal_success").modal('show');
                    $("#iframe-table").reloadFrame(); 
                },
                complete: function(e){
                    $("#modal_ajax_load").modal("hide");  
                    $("#modal_corrigir").modal("hide");
                }
        },"json");
    });

    $(document).on("click", "#btn-conciliar", function(e){
        $.ajax({
            url: $(this).attr("data-route"),
            beforeSend: function () {
                $("#modal_ajax_load").modal("show");
            }, 
            success: function(result){
                $("#modal_success").find("h1").removeClass("text-danger").text(result["title"]).addClass("text-success");
                $("#modal_success").find("p").text(result["text"]);    
                $("#modal_success").modal('show');

                $("#main").load(route);
            },
            error : function(erro){
                var erros = erro.responseJSON.errors;
            },
            complete: function(e){
                $("#modal_ajax_load").modal("hide");  
            }
        });
    });

    $(document).on("click", "#btn-abrir", function(e){
        $.ajax({
            url: $(this).attr("data-route"),
            beforeSend: function () {
                $("#modal_ajax_load").modal("show");
            }, 
            success: function(result){
                $("#modal_success").find("h1").removeClass("text-danger").addClass("text-success").text(result["title"]);
                $("#modal_success").find("p").text(result["text"]);
                $("#modal_success").modal('show');
                $("#main").load(route);
            },
            error : function(erro){
                var erros = erro.responseJSON.errors;
            },
            complete: function(e){
                $("#modal_ajax_load").modal("hide");  
            }
        });
    });

    $(document).on("click", ".btn-movimento", function(e){
        e.preventDefault();
       $(this).loadPage();
    });

    // pagina novo caixa
    $(document).on("DOMNodeInserted", "#novo_caixa", function(e){// ativar o script da pagina carregada        
        if(e.target == this){
            $("#form_novo_caixa #valor").somenteValor();
            flag = true;
        }       
    });

    $(document).on('submit', "#form_novo_caixa", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false;// variavel de ativação do script dos forms carregados
        $(this).ativarAjax({rota: route});// ativa o ajax no form, parametro de rota para reload do form
    });

    // pagina movimento caixa
    $(document).on("DOMNodeInserted", "#movimento_caixa", function(e){// ativar o script da pagina carregada        
        if(e.target == this){            
            $("#data_in").calendar({
                maxDate:"1Y"
            });
            $("#data_fi").calendar({
                maxDate:"1Y"
            });
            flag = true;
            loadIframe();
        }       
    });

    $(document).on("click", "#todo_periodo", function(e){
        if($(this).attr("data-click") == 1){
            $("#data_in").attr("disabled", "disabled");
            $("#data_fi").attr("disabled", "disabled");
            $("#data_fi").attr("disabled", "disabled");
            $("#div-data input").attr("disabled", "disabled");
            $(this).attr("data-click", 0);
        }else{
            $("#data_in").removeAttr("disabled");
            $("#data_fi").removeAttr("disabled");
            $("#div-data input").removeAttr("disabled");
            $(this).attr("data-click", 1);
        }
    });

    $(document).on("change", "#form_movimento_caixa #conta_emprestimo", function(e){
        if($(this).val() != ""){
            $("#form_movimento_caixa #conta_pag").attr("disabled", "disabled");
        }else{
            $("#form_movimento_caixa #conta_pag").removeAttr("disabled");
        }
    });

    $(document).on("change", "#form_movimento_caixa #conta_pag", function(e){
        if($(this).val() != ""){
            $("#form_movimento_caixa #conta_emprestimo").attr("disabled", "disabled");
            $("#form_movimento_caixa #emprestimo").attr("disabled", "disabled");
        }else{
            $("#form_movimento_caixa #conta_emprestimo").removeAttr("disabled");
            $("#form_movimento_caixa #emprestimo").removeAttr("disabled");
        }
    });

    $(document).on("change", "#form_movimento_caixa #tipo", function(e){
        if($(this).val() == ""){
            $("#form_movimento_caixa #div_conta_receb").fadeOut();
            $("#form_movimento_caixa #div_conta_pag").fadeOut();
            $("#form_movimento_caixa #div_detalhado").fadeOut();
            $("#form_movimento_caixa #div-data").fadeOut();
        }else if($(this).val() == 1){
            $("#form_movimento_caixa #div_conta_receb").fadeOut();
            $("#form_movimento_caixa #div_conta_pag").fadeOut();
            $("#form_movimento_caixa #div_detalhado").fadeOut();
            $("#form_movimento_caixa #div-data").fadeIn();
        }else if($(this).val() == 2){            
            $("#form_movimento_caixa #div_conta_pag").fadeOut();
            $("#form_movimento_caixa #div_detalhado").fadeOut();
            $("#form_movimento_caixa #div-data").fadeOut();
            setTimeout(function(){
                $("#form_movimento_caixa #div_conta_receb").fadeIn();
            }, 300);
        }else if($(this).val() == 3){            
            $("#form_movimento_caixa #div_conta_receb").fadeOut();
            $("#form_movimento_caixa #div-data").fadeOut();
            setTimeout(function(){
                $("#form_movimento_caixa #div_detalhado").fadeIn();
                $("#form_movimento_caixa #div_conta_pag").fadeIn();
                $("#form_movimento_caixa #conta_emprestimo").parent().fadeIn();
                $("#form_movimento_caixa #emprestimo").parent().fadeIn();
            }, 300);
        }else if($(this).val() == 4){             
            $("#form_movimento_caixa #div_detalhado").fadeOut();
            $("#form_movimento_caixa #conta_emprestimo").parent().fadeOut();
            $("#form_movimento_caixa #emprestimo").parent().fadeOut();
            $("#form_movimento_caixa #emprestimo").prop("checked", false);
            $("#form_movimento_caixa #div_conta_receb").fadeOut();
            $("#form_movimento_caixa #div-data").fadeOut();
            setTimeout(function(){                
                $("#form_movimento_caixa #div_conta_pag").fadeIn();
            }, 300);
        }else if($(this).val() == 5){            
            $("#form_movimento_caixa #div_conta_receb").fadeOut();
            $("#form_movimento_caixa #div_detalhado").fadeOut();
            $("#form_movimento_caixa #div-data").fadeOut();
            setTimeout(function(){
                $("#form_movimento_caixa #div_conta_pag").fadeIn();
                $("#form_movimento_caixa #conta_emprestimo").parent().fadeIn();
                $("#form_movimento_caixa #emprestimo").parent().fadeIn();
                $("#form_movimento_caixa #emprestimo").prop("checked", true);
            }, 300);
        }
    });

    $(document).on("change", "#form_movimento_caixa #caixa", function(e){
        if($("#form_movimento_caixa #caixa").val() != ""){
            $.getJSON($(this).attr("data-route")+"/"+ $(this).val(), function(result){
                var dados = result["dados"];
                var after = "<option value=''>Todas</option>";
                var contas_receb = "";
                for(key in dados["contas_receb"]){
                    contas_receb += "<option value='"+dados["contas_receb"][key].id+"'>"+dados["contas_receb"][key].nome+"</option>";
                }

                var contas_pag = "";
                for(key in dados["contas_pag"]){
                    contas_pag += "<option value='"+dados["contas_pag"][key].id+"'>"+dados["contas_pag"][key].nome+"</option>";
                }

                $("#form_movimento_caixa #conta_receb").find("option").remove();
                $("#form_movimento_caixa #conta_receb").append(after+contas_receb);            
                $("#form_movimento_caixa #conta_pag").find("option").remove();
                $("#form_movimento_caixa #conta_pag").append(after+contas_pag); 
                $("#form_movimento_caixa #conta_emprestimo").find("option").remove();  

                if(dados["conta_emprestimo"] != false){                
                    var conta_emprestimo = "<option value='"+dados["conta_emprestimo"].id+"'>"+dados["conta_emprestimo"].nome+"</option>";
                    $("#form_movimento_caixa #conta_emprestimo").append("<option value=''>Selecione</option>"+conta_emprestimo);
                }else{
                    $("#form_movimento_caixa #conta_emprestimo").append("<option value=''>Selecione</option>"); 
                }

                $("#movimento_caixa #conta_pag").selectpicker('refresh');
            });
        }
    });

    $(document).on('submit', ".form_target", function(e){ 
        $(".loading").css("display", "flex").animate({opacity: '1'});
    });

    //pagina funções banco
    $(document).on("DOMNodeInserted", "#banco_index", function(e){// ativar o script da pagina carregada        
        if(e.target == this){
            $("#form_banco_depositar #valor").somenteValor();
            flag = true;
        }       
    });

    $(document).on('submit', "#form_novo_banco", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false;// variavel de ativação do script dos forms carregados
        $(this).ativarAjax({rota: route});// ativa o ajax no form, parametro de rota para reload do form
        $("#modal_novo_banco").modal("hide");
    });

    $(document).on('submit', "#form_banco_conta", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false;// variavel de ativação do script dos forms carregados
        $("#modal_banco_conta").modal("hide");
        $(this).ativarAjax({
            rota: route,
            extModal : "#modal_banco_conta"
        });
    });

    $(document).on('click', ".btn_deposito", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        $.getJSON($(this).attr("data-route"), function(result){
            var dados = result['dados'];
            $("#form_banco_depositar #id_conta_banc").children().remove();
            $("#form_banco_depositar #id_conta_banc").append('<option value="">Selecione</option>');
            for(key in dados){
                $("#form_banco_depositar #id_conta_banc").append('<option value="'+dados[key].id+'">'+dados[key].nome+'</option>');
            }  
        });
    });

    $(document).on('submit', "#form_banco_depositar", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        flag = false;// variavel de ativação do script dos forms carregados
        $("#modal_deposito").modal("hide");
        $(this).ativarAjax({
            rota: route,
            extModal : "#modal_deposito"
        });// ativa o ajax no form, parametro de rota para reload do form
    });

    $(document).on('click', ".btn-detalhes", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        $(this).find('i').toggleClass( "fa-rotate-180" );
        $(this).parent().next('.detalhes').slideToggle();
    });
//--------------------------------- fim funções controle caixa ---------------------------------------

//-------------------------------------- funções empresa ---------------------------------------------
    // pagina categorias de contas
    $(document).on("DOMNodeInserted", "#categoria_contas", function(e){// ativar o script da pagina carregada        
        if(e.target == this){
            flag = true;
        }       
    });

    $(document).on('shown.bs.modal', '#modal_categoria', function () {
        $('#cad_categoria_conta #nome').focus();
    });

    $(document).on('submit', "#cad_categoria_conta", function(e){ // usado para enviar e validar dados de qualquer formulario submetido   
        $("#modal_categoria").modal('hide');
        flag = false;// variavel de ativação do script dos forms carregados
        $(this).ativarAjax({rota: route});// ativa o ajax no form, parametro de rota para reload do form
    });
    

    $(document).on("click", ".btn_deletar_categoria", function(e){// ativar o script da pagina carregada        
        $(this).actionId({rota: route});
   });

   $(document).on("click", ".toggle-disabled", function(e){// ativar o script da pagina carregada        
        if($(this).prev('input').is(':disabled')){
            $(this).prev('input').removeAttr("disabled");
        }else{
            $(this).prev('input').attr("disabled", "disabled");
        }
    });
    
    $(document).on('submit', "#form-assinatura", function(e){ 
        flag = false;// variavel de ativação do script dos forms carregados
        $(this).postFormData({
            rota: route,
        });
    });

    $(document).on("click", ".btn_status", function(e){
        $(this).actionId({rota: route});
    })
//------------------------------------ fim funções empresa -------------------------------------------

//------------------------------------ funções usuários -------------------------------------------
    $(document).on("click", ".btn_novo", function(e){// ativar o script da pagina carregada   
        e.preventDefault();       
        $(this).loadPage();
    });
//------------------------------------ fim funções usuários -------------------------------------------

//-------------------------------------- Funções Oficios -----------------------------------------------
// $(document).on("DOMNodeInserted", "#cadastro_oficio", function(e){// ativar o script da pagina carregada        
//     if(e.target == this){         
//         var editor =  new nicEditor(); 
//         editor.panelInstance('conteudo');
//         $(".nicEdit-main").css("max-height", "29.7cm");
//         flag = true;
//     }
// });
//-------------------------------------- Fim funções Oficios -----------------------------------------------

});//-----------end document.ready