$(document).ready(function(){
    $(document).on("change","#foto", function(){    
        $("#append-foto").append('<img id="target" style="width: 100%">');
        $(".div-alterar").fadeIn();
        $(".div-cortar").fadeIn();
        var foto = $(this)[0].files[0];
        var foto64 = new FileReader();
        foto64.onloadend = function(){
            var base64 = foto64.result;
            $("#target").attr("src", base64);
            $("#foto-cooper").attr("src", base64);
            $("#preview").attr("src", base64);
            $("#target").css("display", "none");
            $("#carregando").css("display", "none");
            $("#carregando").fadeIn(100);            
        };		
        foto64.readAsDataURL(foto);  
        carregarArq();
    });

    var jcrop_api; 
    function carregarArq(){
        $(".foto-file").fadeOut(0).removeClass("d-flex");
        setTimeout(function(){ 
            var img_wd = $("#target").width();
            var div_wd = $(".arquivo").width();
            var wd = (div_wd - img_wd) / 2;
            $("#divs").css("margin-left", wd +"px");
            $("#carregando").fadeOut(0);	         		       
            $('#target').Jcrop({
                aspectRatio: 130/173,
                bgFade:     true,
                bgOpacity: .2,
                bgColor: '#FFF',
                setSelect: [80, 80, 189, 252],
                allowSelect: false,                
                onSelect: updateCoords,
                onChange: updateCoords
                },function(){
                    jcrop_api = this;
            });
            
            function updateCoords(c){
                $('#x').val(c.x);
                $('#y').val(c.y);
                $('#w').val(c.w);
                $('#h').val(c.h);

                var width = $("#target").width();
                var height = $("#target").height();
                var rx = 130 / c.w;
                var ry = 173 / c.h;        
                $("#preview").css({ 
                    width: Math.round(rx * width) + 'px',
                    height: Math.round(ry * height) + 'px',
                    marginLeft: '-' + Math.round(rx * c.x) + 'px',
                    marginTop: '-' + Math.round(ry * c.y) + 'px'
                });

                $("#foto-cooper").css({ 
                    width: Math.round(rx * width) + 'px',
                    height: Math.round(ry * height) + 'px',
                    marginLeft: '-' + Math.round(rx * c.x) + 'px',
                    marginTop: '-' + Math.round(ry * c.y) + 'px'
                });
            };

            $('#altura').val($("#target").height());
        }, 1000);
    };

    $(document).on("click","#alterar",function(e){
        jcrop_api.destroy();
        $(".foto-file").fadeIn();
        $(".div-alterar").fadeOut(0);
        $(".div-cortar").fadeOut(0);
        $("#target").remove();
        $("#foto").val("");     
        $(".div-preview").css("display", "none");
        $("#salvar").css("display", "none");
        $("#lbl-foto-cooper").fadeIn(0);
        $("#div-foto-cooper").fadeOut(0);
    });

    $(document).on("click","#cortar",function(e){
        jcrop_api.destroy();
        $(".div-cortar").fadeOut(0);
        $("#target").remove();        
        $("#salvar").css("display", "block");
        $(".div-preview").css("display", "flex");
    });

    $(document).on("click","#salvar",function(e){        
        $("#lbl-foto-cooper").fadeOut(0);
        $("#div-foto-cooper").css("display", "flex");
    });
});