$(document).on("DOMNodeInserted", "#novo_oficio", function(e){
    if(e.target == this){  
        if(tinymce.editors.length > 0){
            tinymce.remove("#editor");
        }      
        
        tinymce.init({//editor de soluções
            language: 'pt_BR',
            selector: "#editor",
            height: '1122px',
            relative_urls: false,
            remove_script_host: false,
            document_base_url: $('#editor').attr('data-base-url'),
            plugins:[
                'link lists fullscreen hr save table lineheight'
            ],
            content_style: "body { margin: 0 3em; }",
            menubar:false,
            toolbar_mode: 'wrap',
            toolbar: [
                {name: 'Ferramentas', items: ['undo', 'redo', 'fullscreen']},
                {name: 'Formatação', items: ['fontselect', 'fontsizeselect', 'styleselect']},
                {name: 'Atributos', items: ['mybutton']},
                {name: 'Inserir', items: ['table', 'link', 'bullist', 'numlist', 'hr', 'hoje', ]},
                {name: 'Identação', items: ['bold', 'italic', 'alignleft', 'aligncenter', 'alignright', 'justify', 'outdent', 'indent', 'lineheight']},
            ],
            table_sizing_mode: 'responsive',
            setup: function (editor) {
                /* Menu items are recreated when the menu is closed and opened, so we need
                   a variable to store the toggle menu item state. */
                var toggleState = false;
            
                /* example, adding a toolbar menu button */
                editor.ui.registry.addMenuButton('mybutton', {
                  text: 'Atributos',
                  fetch: function (callback) {
                    var items = [
                      {
                        type: 'nestedmenuitem',
                        text: 'Empresa',
                        getSubmenuItems: function () {
                          return [
                            {
                              type: 'menuitem',
                              text: 'Nome',
                              onAction: function () {
                                editor.insertContent('<strong>_E-NOME_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Razão social',
                              onAction: function () {
                                editor.insertContent('<strong>_E-RAZAO_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Matrícula',
                              onAction: function () {
                                editor.insertContent('<strong>_E-MATRICULA_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'CNPJ',
                              onAction: function () {
                                editor.insertContent('<strong>_E-CNPJ_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Quantida de cooperados',
                              onAction: function () {
                                editor.insertContent('<strong>_E-Q-COOPERADOS_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Lote',
                              onAction: function () {
                                editor.insertContent('<strong>_E-LOTE_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Regional',
                              onAction: function () {
                                editor.insertContent('<strong>_E-REGIONAL_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Polo',
                              onAction: function () {
                                editor.insertContent('<strong>_E-POLO_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'E-mail',
                              onAction: function () {
                                editor.insertContent('<strong>_E-EMAIL_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Telefone',
                              onAction: function () {
                                editor.insertContent('<strong>_E-TELEFONE_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Estado (UF)',
                              onAction: function () {
                                editor.insertContent('<strong>_E-ESTADO_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Cidade',
                              onAction: function () {
                                editor.insertContent('<strong>_E-CIDADE_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Bairro',
                              onAction: function () {
                                editor.insertContent('<strong>_E-BAIRRO_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'CEP',
                              onAction: function () {
                                editor.insertContent('<strong>_E-CEP_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Endereço',
                              onAction: function () {
                                editor.insertContent('<strong>_E-RUA_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Número',
                              onAction: function () {
                                editor.insertContent('<strong>_E-NUMERO_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Logo',
                              onAction: function () {
                                editor.insertContent('<img src="'+$('#editor').attr('data-logo')+'"/>');
                              }
                            },
                            
                          ];
                        }
                      },
                      {
                        type: 'nestedmenuitem',
                        text: 'Cooperado',
                        getSubmenuItems: function () {
                          return [
                            {
                              type: 'menuitem',
                              text: 'Nome',
                              onAction: function () {
                                editor.insertContent('<strong>_C-NOME_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Matrícula cooperativa',
                              onAction: function () {
                                editor.insertContent('<strong>_C-M-COOPERATIVA_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Matrícula ARCE',
                              onAction: function () {
                                editor.insertContent('<strong>_C-M-ARCE_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Data de nascimento',
                              onAction: function () {
                                editor.insertContent('<strong>_C-NASCIMENTO_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'CPF',
                              onAction: function () {
                                editor.insertContent('<strong>_C-CPF_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'RG',
                              onAction: function () {
                                editor.insertContent('<strong>_C-RG_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Orgão expedidor RG',
                              onAction: function () {
                                editor.insertContent('<strong>_C-EXPEDIDOR_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'CNH',
                              onAction: function () {
                                editor.insertContent('<strong>_C-CNH_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Categoria CNH',
                              onAction: function () {
                                editor.insertContent('<strong>_C-V-CNH_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Validade CNH',
                              onAction: function () {
                                editor.insertContent('<strong>_C-CAT-CNH_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Telefone',
                              onAction: function () {
                                editor.insertContent('<strong>_C-TELEFONE_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Celular',
                              onAction: function () {
                                editor.insertContent('<strong>_C-CELULAR_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'E-mail',
                              onAction: function () {
                                editor.insertContent('<strong>_C-EMAIL_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'CEP',
                              onAction: function () {
                                editor.insertContent('<strong>_C-CEP_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Estado (UF)',
                              onAction: function () {
                                editor.insertContent('<strong>_C-ESTADO_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Cidade',
                              onAction: function () {
                                editor.insertContent('<strong>_C-CIDADE_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Bairro',
                              onAction: function () {
                                editor.insertContent('<strong>_C-BAIRRO_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Endereço',
                              onAction: function () {
                                editor.insertContent('<strong>_C-ENDERECO_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Número',
                              onAction: function () {
                                editor.insertContent('<strong>_C-NUMERO_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Complemtento endereço',
                              onAction: function () {
                                editor.insertContent('<strong>_C-COMPLEMENTO_</strong>');
                              }
                            }
                          ];
                        }
                      },
                      {
                        type: 'nestedmenuitem',
                        text: 'Presidente',
                        getSubmenuItems: function () {
                          return [
                            {
                              type: 'menuitem',
                              text: 'Nome',
                              onAction: function () {
                                editor.insertContent('<strong>_P-NOME_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'CPF',
                              onAction: function () {
                                editor.insertContent('<strong>_P-CPF_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Data de nascimento',
                              onAction: function () {
                                editor.insertContent('<strong>_P-NASCIMENTO_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Telefone',
                              onAction: function () {
                                editor.insertContent('<strong>_P-TELEFONE_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'E-email',
                              onAction: function () {
                                editor.insertContent('<strong>_P-EMAIL_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Estado (UF)',
                              onAction: function () {
                                editor.insertContent('<strong>_P-ESTADO_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Cidade',
                              onAction: function () {
                                editor.insertContent('<strong>_P-CIDADE_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Bairro',
                              onAction: function () {
                                editor.insertContent('<strong>_P-BAIRRO_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Endereçço',
                              onAction: function () {
                                editor.insertContent('<strong>_P-LOGRADOURO_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Número',
                              onAction: function () {
                                editor.insertContent('<strong>_P-NUMERO_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Início do mandato',
                              onAction: function () {
                                editor.insertContent('<strong>_P-I-MANDATO_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Final do mandato',
                              onAction: function () {
                                editor.insertContent('<strong>_P-F-MANDADO_</strong>');
                              }
                            },
                            {
                              type: 'menuitem',
                              text: 'Assinatura',
                              onAction: function () {
                                editor.insertContent('<img src="'+$('#editor').attr('data-assinatura')+'"/>');
                              }
                            },
                          ];
                        }
                      },
                      {
                        type: 'nestedmenuitem',
                        text: 'Veículo',
                        getSubmenuItems: function () {
                          return [
                            {
                              type: 'menuitem',
                              text: 'Marca',
                              onAction: function () {
                                editor.insertContent('<strong>_V-MARCA_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Modelo',
                              onAction: function () {
                                editor.insertContent('<strong>_V-MODELO_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Cor',
                              onAction: function () {
                                editor.insertContent('<strong>_V-COR_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Placa',
                              onAction: function () {
                                editor.insertContent('<strong>_V-PLACA_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Renavam',
                              onAction: function () {
                                editor.insertContent('<strong>_V-RENAVAM_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Chassi',
                              onAction: function () {
                                editor.insertContent('<strong>_V-CHASSI_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Ano fabricação',
                              onAction: function () {
                                editor.insertContent('<strong>_V-A-FABRICACAO_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Ano modelo',
                              onAction: function () {
                                editor.insertContent('<strong>_V-A-MODELO_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Data licenciamento',
                              onAction: function () {
                                editor.insertContent('<strong>_V-D-LICENCIAMENTO_</strong>');
                              }
                            },{
                              type: 'menuitem',
                              text: 'Data vistoria',
                              onAction: function () {
                                editor.insertContent('<strong>_V-D-VISTORIA_</strong>');
                              }
                            },
                          ];
                        }
                      }
                    ];
                    callback(items);
                  }
                });
                editor.ui.registry.addButton('hoje', {
                  icon: 'insert-time',
                  tooltip: 'Inserir data atual',
                  onAction: function (_) {
                    editor.insertContent('<strong>_DATA-ATUAL_</strong>');
                  }
                });
              }
        });
    }
});