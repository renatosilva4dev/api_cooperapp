(function($){ 
//---------------- somente Letras ---------------------
    $.fn.somenteLetras = function(){
        this.on("input", function(){        
            var er = /[^A-Za-zÀ-ú ]/;
            er.lastIndex = 0;
            if (er.test(this.value)) {
                this.value = this.value.slice(0, -1);
            }
        });
        return this;
    };

    $.fn.maiusculo = function(){
        this.on("input", function(){        
            this.value = this.value.toUpperCase();
        });
        return this;
    };

    $.fn.semSibolos = function(){
        this.on("input", function(){        
            var er = /[^A-Za 0-9]/;
            er.lastIndex = 0;
            if (er.test(this.value)) {
                this.value = this.value.slice(0, -1);
            }
        });
        return this;
    };

    $.fn.somenteNumero = function(options){
        var settings = $.extend({
            max: 10,
            simbol:'-'
        }, options);

        this.on("input", function(){        
            var err = '/[^0-9.,'+settings.simbol+']/';
            var er = eval(err);
            er.lastIndex = 0;
            if (er.test(this.value)) {
                this.value = this.value.slice(0, -1);
            }

            if(this.value.length > settings.max){
                this.value = this.value.slice(0, -1);
            }
        });
        return this;
    };
//----------------- somente numeros ----------------
    $.fn.somenteValor = function(){
        this.attr("autocomplete","off" );
        this.on("input", function(){        
            var er = /[^0-9.,]/;
            er.lastIndex = 0;
            if (er.test(this.value)) {
                this.value = this.value.slice(0, -1);
            }else{                    
                var int = 0;
                var decimal = 0;
                var num = this.value.replace(/\D+/g, '');
                if(num.length > 3){ 
                    int = num.slice(0, -2);
                    decimal = num.substr(-2);
                    int = Number(int); 
                    this.value = int.toLocaleString('pt-BR') +","+decimal;
                }
            }
        });
        return this;
    };

// ------------------ calendario ------------------------
    $.fn.calendar = function(options){
        var settings = $.extend({
            minDate: "-5Y",
            maxDate: "0Y",
            yearRange: "0:+0",
            dateFormat: 'dd/mm/yy'
        }, options);

        this.datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true,
            minDate: settings.minDate,
            maxDate: settings.maxDate,
            yearRange: settings.yearRange,
            monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
            dateFormat: settings.dateFormat
        });

        this.attr("autocomplete", "off");
        this.maskInput("99/99/9999");

        return this;
    };

//----------------- ativar ajax nos forms --------------
    $.fn.postForm = function(options){  
        event.preventDefault();
        var settings = $.extend({
            'rota' : "null", 
        }, options);
        var dados = this.serializeArray();
        var modal = this.attr('data-modal');
        var reload = this.attr('data-reload');

        if(reload){
            settings.rota = reload;
        }

        $.ajaxSetup({
            headers: {
                'X-XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: this.attr("action"),
            data: dados,
            beforeSend: function () {
                $("#modal_ajax_load").modal("show");
            }, 
            success:function(result){  
                if(modal){//se este post for feito de um modal, só é fechado se for sucesso
                    $(modal).modal("hide");
                }
                $("#modal_success").find("h1").removeClass("text-danger");
                $("#modal_success").find("h1").removeClass("text-success");  

                $("#modal_success").find("h1").text(result["title"]).addClass("text-"+result["class"]);
                $("#modal_success").find("p").text(result["text"]);

                $("#main").load(settings.rota);

                $("#modal_success").modal("show");                 
            },
            error: function (err) {
                console.log(err);
                var erros = err.responseJSON.errors;
                if(err.status == 500){
                    $("#exception").next("span").remove();
                    $("#exception").after('<span class="text-danger">Houve um erro inesperado, entre em contato com o suporte.</span>');
                }else if(err.status == 401){
                    $("#modal_success").find("h1").removeClass("text-danger");
                    $("#modal_success").find("h1").removeClass("text-success"); 
                    $("#modal_success").find("h1").text("Atenção!").addClass('text-danger');
                    $("#modal_success").find("p").text(erros['text'] );
                    $("#modal_success").modal("show"); 
                }else if(typeof erros["title"] != 'undefined'){
                    $("#modal_success").find("h1").removeClass("text-danger");
                    $("#modal_success").find("h1").removeClass("text-success");  

                    $("#modal_success").find("h1").text(erros["title"]).addClass("text-"+result["class"]);
                    $("#modal_success").find("p").text(erros["text"]);      
                              
                    $("#modal_success").modal("show");
                }else{
                    for(key in erros){
                        $("#"+key).next("span").remove();
                        $("#"+key).after('<span class="text-danger">'+erros[key]+'</span>');
                        $("#"+key).addClass("border-danger");
                    }  
                }              
            },
            complete: function(){
                $("#modal_ajax_load").modal("hide");  
            }
        }, "json");

        return this;
    };

    $.fn.postFormData = function(options){  
        event.preventDefault();
        $("span.text-danger").remove();
        $("input.border-danger").removeClass('border-danger');
        var settings = $.extend({
            'rota' : "null",
            'extModal' : false  // para reload se sucesso
        }, options);
        var dados = new FormData(this[0]);
        var reload = this.attr('data-reload');

        if(reload){
            settings.rota = reload;
        }
        
        $.ajaxSetup({
            headers: {
                'X-XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            dataType: 'json',
            url: this.attr("action"),
            enctype: 'multipart/form-data',
            data: dados,            
            method:'POST',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $("#modal_ajax_load").modal("show");
            }, 
            success:function(result){  
                $("#modal_success").find("h1").removeClass("text-danger");
                $("#modal_success").find("h1").removeClass("text-success");

                $("#modal_success").find("h1").text(result["title"]).addClass("text-"+result["class"]);
                $("#modal_success").find("p").text(result["text"]);

                $("#main").load(settings.rota);

                $("#modal_success").modal("show"); 

                if(settings.extModal != false){
                    $(settings.extModal).modal("hide");
                }
            },
            error: function (err) {
                var erros = err.responseJSON.errors;
                if(err.status == 500){
                    $("#exception").next("span").remove();
                    $("#exception").after('<span class="text-danger">Houve um erro inesperado, entre em contato com o suporte.</span>');
                }else if(typeof erros["title"] != 'undefined'){
                    $("#modal_success").find("h1").removeClass("text-success");
                    $("#modal_success").find("h1").removeClass("text-danger");

                    $("#modal_success").find("h1").text(erros["title"]).addClass("text-danger");
                    $("#modal_success").find("p").text(erros["text"]);       

                    $("#modal_success").modal("show");
                }else{
                    for(key in erros){
                        $("#"+key).next("span").remove();
                        $("#"+key).after('<span class="text-danger">'+erros[key]+'</span>');
                        $("#"+key).addClass("border-danger");
                    }  
                    if(settings.extModal != false){
                        $(settings.extModal).modal("show");
                    }
                }              
            },
            complete: function(){
                $("#modal_ajax_load").modal("hide");  
            }
        }, "json");

        return this;
    };

    $.fn.postFormRellot = function(options){  
        event.preventDefault();
        var dados = this.serializeArray();
        $.ajaxSetup({
            headers: {
                'X-XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'POST',
            url: this.attr("action"),
            data: dados,
            beforeSend: function () {
                $(".loading").fadeIn(200);  
            }, 
            success:function(result){  
                $(".load-rellot").html(result); 
            },
            error:function(err){
                var erros = err.responseJSON.errors;
                if(err.status == 500){
                    $("#exception").next("span").remove();
                    $("#exception").after('<span class="text-danger">Houve um erro inesperado, entre em contato com o suporte.</span>');
                }else{
                    for(key in erros){
                        $("#"+key).next("span").remove();
                        $("#"+key).after('<span class="text-danger">'+erros[key]+'</span>');
                        $("#"+key).addClass("border-danger");
                    }  
                }              
            },
            complete: function(){
                $(".loading").fadeOut(200);  
            }
        });

        return this;
    };

    $.fn.actionId = function(options){        
        event.preventDefault();

        var settings = $.extend({
            'rota' : false,
        }, options);
        var reload = this.attr('data-reload');

        if(reload){
            settings.rota = reload;
        }
        $.ajax({
            type:'GET',
            url: this.attr("data-route"),
            beforeSend: function () {
                $("#modal_ajax_load").modal("show");
            }, 
            success:function(result){       
                $("#modal_success").find("h1").removeClass("text-danger");
                $("#modal_success").find("h1").removeClass("text-success");  

                $("#modal_success").find("h1").text(result["title"]).addClass("text-"+result["class"]);
                $("#modal_success").find("p").text(result["text"]);

                $("#main").load(settings.rota);

                $("#modal_success").modal("show"); 

                if(settings.extModal != false){
                    $(settings.extModal).modal("hide");
                }
            },
            complete: function(){
                $("#modal_ajax_load").modal("hide"); 
            },
            error: function (err) {
                var erros = err.responseJSON.errors;
                if(err.status == 422){
                    $("#modal_success").find("h1").removeClass("text-danger");
                    $("#modal_success").find("h1").removeClass("text-success");
    
                    $("#modal_success").find("h1").text(erros["title"]).addClass("text-"+erros["class"]);
                    $("#modal_success").find("p").text(erros["text"]);

                    $("#modal_success").modal("show"); 
                }else if(err.status == 500){
                    $("#modal_success").find("h1").removeClass("text-danger");
                    $("#modal_success").find("h1").removeClass("text-success"); 
                    $("#modal_success").find("h1").text("Erro!").addClass('text-danger');
                    $("#modal_success").find("p").text("Houve um erro inesperado, entre em contato com o suporte");
                    $("#modal_success").modal("show"); 
                }             
            },
        });
        
        return this;
    };

    $.fn.actionSubmit = function(options){        
        event.preventDefault();
        
        var button = this.attr('data-button');

        $.ajax({
            type:'GET',
            url: this.attr("data-route"),
            beforeSend: function () {
                $("#modal_ajax_load").modal("show");
            }, 
            success:function(result){       
                $(button).trigger( "click" );
            },
            complete: function(){
                $("#modal_ajax_load").modal("hide"); 
            },
            error: function (err) {
                var erros = err.responseJSON.errors;
                if(err.status == 422){
                    $("#modal_success").find("h1").removeClass("text-danger");
                    $("#modal_success").find("h1").removeClass("text-success");
    
                    $("#modal_success").find("h1").text(erros["title"]).addClass("text-"+erros["class"]);
                    $("#modal_success").find("p").text(erros["text"]);

                    $("#modal_success").modal("show"); 
                }else if(err.status == 500){
                    $("#modal_success").find("h1").removeClass("text-danger");
                    $("#modal_success").find("h1").removeClass("text-success"); 
                    $("#modal_success").find("h1").text("Erro!").addClass('text-danger');
                    $("#modal_success").find("p").text("Houve um erro inesperado, entre em contato com o suporte");
                    $("#modal_success").modal("show"); 
                }             
            },
        });
        
        return this;
    };
    $.fn.loadPage = function(options){  
        event.preventDefault();
        var route = null;    
        var settings = $.extend({
            'rota' : null,
        }, options);

        if(settings.rota == null){        
            route = this.attr("data-route");
        }else{
            route = settings.rota;
        }

        $.ajax({
            url: route,
            beforeSend: function () {
                $("#modal_ajax_load").modal('show');
            }, 
            success: function(result){
                $("#main").hide().html(result).fadeIn(500);                
                $("#modal_ajax_load").modal("hide"); 
            },
            error: function(err){
                var erros = err.responseJSON.errors;
                if(err.status == 422){
                    $("#modal_success").find("h1").removeClass("text-danger");
                    $("#modal_success").find("h1").removeClass("text-success");
    
                    $("#modal_success").find("h1").text(erros["title"]).addClass("text-"+erros["class"]);
                    $("#modal_success").find("p").text(erros["text"]);

                    $("#modal_success").modal("show"); 
                }else if(err.status == 500){
                    $("#modal_success").find("h1").removeClass("text-danger");
                    $("#modal_success").find("h1").removeClass("text-success"); 
                    $("#modal_success").find("h1").text("Erro!").addClass('text-danger');
                    $("#modal_success").find("p").text("Houve um erro inesperado, entre em contato com o suporte");
                    $("#modal_success").modal("show"); 
                }
            },
            complete: function(){
                $("#modal_ajax_load").modal("hide");   
            }
        });

        return this;
    };

    $.fn.findVal = function(options){ 
        var rota = this.attr("data-route")+"/"+this.val();    
        var id = this.attr("data-id");
        $.ajax({
            type:'GET',
            url: rota,
            beforeSend: function () {
                $(id).addClass('loading-input');
            }, 
            success:function(result){   
                $(id).val(result['valor']);
            },
            complete: function(){
                $(id).removeClass('loading-input');
            },
        });
        
        return this;
    };

    $.fn.findDebito = function(options){ 
        var rota = this.attr("data-route")+"/"+this.val();    
        var id = this.attr("data-id");

        if(this.val() == ''){
            $(id).find('option').remove(); 
            $(id).append('<option value="">Selecione..</option>');
            return;
        }

        $.ajax({
            type:'GET',
            url: rota,
            beforeSend: function () {
                $(id).find('option').remove(); 
                $(id).addClass('loading-input');
            }, 
            success:function(result){  
                $(id).append('<option value="">Selecione..</option>'+result['options']);
            },
            complete: function(){
                $(id).removeClass('loading-input');
            },
        });
        
        return this;
    };

    $.fn.findTaxa = function(options){ 
        var rota = this.attr("data-route");    
        var valor = "#valor";
        var data = "#data_venc_call";
        var id = this.attr("data-id");
        var conta = "#conta_id";

        var dados = [
            {name: '_token', value:  $('input[name="_token"]').val()},
            {name: 'vencimento', value: $(data).val()},
            {name: 'conta_id', value: $(conta).val()},
            {name: id, value: $('#'+id).val()}
        ];
        
        $.ajaxSetup({
            headers: {
                'X-XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            dataType: 'json',
            type:'POST',
            url: rota,
            data: dados,
            beforeSend: function () {
                $(valor).addClass('loading-input');
            }, 
            success:function(result){   
                $(valor).val(result['valor']);
                $("#taxa_id").val(result['taxa_id']);
            },
            error: function(err){
                console.log(err);
            },
            complete: function(){
                $(valor).removeClass('loading-input');
            },
        });
        
        return this;
    };

    $.fn.postFormRedirect = function(){  
        event.preventDefault();    
        var modal = this.attr("data-modal");
        var dados = this.serializeArray();
        $.ajaxSetup({
            headers: {
                'X-XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: this.attr("action"),
            data: dados,
            beforeSend: function () {
                $("#modal_ajax_load").modal('show');
            }, 
            success: function(result){
                $("#main").html(result['html']).fadeIn(500);
                if(modal){
                    $(modal).modal("hide");
                    $('.modal-backdrop').remove();
                }     
            },
            error: function(err){
                var erros = err.responseJSON.errors;
                if(err.status == 422){
                    $("#modal_success").find("h1").removeClass("text-danger");
                    $("#modal_success").find("h1").removeClass("text-success");
    
                    $("#modal_success").find("h1").text(erros["title"]).addClass("text-"+erros["class"]);
                    $("#modal_success").find("p").text(erros["text"]);

                    $("#modal_success").modal("show"); 
                }else if(err.status == 500){
                    $("#modal_success").find("h1").removeClass("text-danger");
                    $("#modal_success").find("h1").removeClass("text-success"); 
                    $("#modal_success").find("h1").text("Erro!").addClass('text-danger');
                    $("#modal_success").find("p").text("Houve um erro inesperado, entre em contato com o suporte");
                    $("#modal_success").modal("show"); 
                }else{
                    for(key in erros){
                        $("#"+key).next("span").remove();
                        $("#"+key).after('<span class="text-danger">'+erros[key]+'</span>');
                        $("#"+key).addClass("border-danger");
                    } 
                }
            },
            complete: function(){
                $("#modal_ajax_load").modal("hide");
                  
            }
        }, "json");

        return this;
    };

    $.fn.actionNoReload = function(options){ 
        event.stopPropagation();
        var rota = this.attr("data-route"); 
        var element = this;
        $.ajax({
            type:'GET',
            url: rota,
            beforeSend: function () {
                element.addClass('loading-input');
            }, 
            success:function(result){  
                element.parent().remove();
                var counter = Number($(".notify-counter").text());
                $(".notify-counter").text(counter-1);
            },
            complete: function(){
                element.removeClass('loading-input');
            },
        });
        
        return this;
    };

    $.fn.loadValidate = function(options){  
        event.preventDefault();
        var route = this.attr("data-route");
        var form = this.attr("data-form");

        $.ajax({
            url: route,
            beforeSend: function () {
                $("#modal_ajax_load").modal('show');
            }, 
            success: function(result){
                $("#main").hide().html(result).fadeIn(500);                
                $("#modal_ajax_load").modal("hide"); 
            },
            error: function(err){
                var erros = err.responseJSON.errors;
                if(err.status == 401){    
                    $("#modal_cooperado").find("h2").text(erros["title"]).addClass("text-"+erros["class"]);
                    $("#modal_cooperado").find("p").text(erros["text"]);
                    $("#modal_cooperado").find("#cooperado_id").attr('href', erros["route"]);
                    $(form).attr('action', route);
                    $("#modal_cooperado").modal("show"); 
                }else if(err.status == 500){
                    $("#modal_success").find("h1").removeClass("text-danger");
                    $("#modal_success").find("h1").removeClass("text-success"); 
                    $("#modal_success").find("h1").text("Erro!").addClass('text-danger');
                    $("#modal_success").find("p").text("Houve um erro inesperado, entre em contato com o suporte");
                    $("#modal_success").modal("show"); 
                }
            },
            complete: function(){
                $("#modal_ajax_load").modal("hide");    
            }
        });

        return this;
    };
})(jQuery);