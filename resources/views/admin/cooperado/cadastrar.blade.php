<div class="main-area" id="nova_linha">
    <h1 class="text-center mb-4 border-blue-b">Cadastrar Cooperado</h1>
    <form action="{{ route('cooperado.save') }}" id="form_cad_linha" method="POST" class="w-100 mx-0 row justify-content-center text-2">
        @csrf
        <div class="col-md-6">
            <div class="form-group">
                <label for="nome">Nome</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <input type="text" id="nome" name="nome" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="matricula">Matricula</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <input type="text" id="matricula" name="matricula" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="telefone">Telefone</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <input type="text" class="form-control" name="telefone" id="telefone" required>
            </div>
            <div class="form-group">
                <label for="linha_id">Linha</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <select name="linha_id" id="linha_id" class="form-control">
                    @foreach ($linhas as $linha)
                        <option value={{ $linha->id }}>{{ $linha->nome_linha }}</option>
                    @endforeach
                </select>
            </div> 
            <div class="form-group mt-2">
                <button type="submit" id="exception" class="btn btn-md btn-primary btn-block">SALVAR</button>
            </div>
        </div>
    </form>
</div>