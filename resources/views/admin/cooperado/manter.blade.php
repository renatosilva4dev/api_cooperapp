<div class="main-area">
    <div  class="w-100 rellot">
        <div class="w-100 d-flex justify-content-between align-items-center">
        </div>
        <div class="w-100 border-bottom mb-4">
            <p class="m-0"><em style="font-size: .9em;">Cooperados em {{ date('d/m/Y') }}</em></p>
        </div>
        <div class="w-100">
            <table class="my-table mb-5">
                <thead>
                    <tr>
                        <th class="text-left">Cooperado</th>
                        <th class="text-center">Linha</th>
                        <th class="text-center text-success">Saldo</th>
                        <th class="text-center">Pagar</th>
                        <th class="text-center">Usuários</th>
                        <th class="text-center">Editar</th>
                        <th class="text-center">Excluir</th>
                    </tr>
                </thead>
                <tbody>
                    @php $total_cooperados = 0; @endphp
                    @foreach($cooperados as $cooperado)
                        <tr class="border-bottom border-gray">
                            <td class="text-left">{{$cooperado->matricula . ' - ' .$cooperado->nome }}</td>
                            <td class="text-center">{{ $cooperado->linha->nome_linha }}</td>
                            <td class="text-center text-success">R$ {{ $cooperado->saldo }}</td>
                            <td class="text-center"><button data-route="{{ route('cooperado.pagar', $cooperado->id) }}" class="btn btn-success btn-sm loadPage"><i class="fas fa-money-bill-wave" aria-hidden="true"></i></button></td>
                            <td class="text-center"><button data-route="{{ route('cooperado.usuarios', $cooperado->id) }}" class="btn btn-primary btn-sm loadPage"><i class="fas fa-user-cog" aria-hidden="true"></i></i></button></td>
                            <td class="text-center"><button data-route="{{ route('cooperado.edit', $cooperado->id) }}" class="btn btn-secondary btn-sm loadPage"><i class="far fa-edit" aria-hidden="true"></i></button></td>
                            <td>                
                                <div class="text-center align-intens-center" style="">
                                    <a class="delete btn border text-danger ml-1 delete-conf" title="Excluir">
                                        <i class="delete far fa-trash-alt" aria-hidden="true"></i>                        
                                    </a>
                                    <div class="delete toolip-delete">
                                        <p class="delete" style="color: #000;">Excluir?</p>
                                        <div class="delete align-intens-center ">
                                            <button type="button" class="delete btn btn-success mx-2 actionId" data-route="{{ route('cooperado.deletar', $cooperado->id) }}">SIM</button>
                                            <button type="button" class="delete btn btn-danger mx-2 delete-cancel">NÃO</button>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @php $total_cooperados++; @endphp
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6" class="text-left"><strong>Quantidade total de cooperados</strong></td>
                        <td class="text-center"><strong>{{ $total_cooperados }}</strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>