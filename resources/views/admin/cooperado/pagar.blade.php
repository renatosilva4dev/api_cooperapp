<div class="main-area" id="nova_linha">
    <h1 class="text-center mb-4 border-blue-b">Pagar Cooperado</h1>
    <form action="{{ route('cooperado.pagar.confirm') }}" id="form_cad_linha" method="POST" class="w-100 mx-0 row justify-content-center text-2">
        @csrf
        <div class="col-md-6">
            <h3>O saldo do cooperado é: <span class="text-success">R$ {{$cooperado->saldo}}</span></h3>
            <div class="form-group">
                <label for="nome">Valor</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <input type="text" id="valor" name="valor" class="form-control" required>
            </div>
            <input type="hidden" name="cooperado_id" value="{{$cooperado->id}}">
            <div class="form-group mt-2">
                @if ($cooperado->saldo <= 0)
                    <button type="submit" id="exception" class="btn btn-md btn-success btn-block" disabled>PAGAR</button>
                @else
                    <button type="submit" id="exception" class="btn btn-md btn-success btn-block">PAGAR</button>
                @endif
            </div>
        </div>
    </form>
</div>