<div class="main-area">
    <div  class="w-100 rellot">
        <div class="w-100 d-flex justify-content-between align-items-center">
        </div>
        <div class="w-100">
            <table class="my-table mb-5">
                <thead>
                    <tr>
                        <th class="text-left">Usuário</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">CPF</th>
                        <th class="text-center">Editar</th>
                        <th class="text-center">Excluir</th>
                    </tr>
                </thead>
                <tbody>
                    @php $total_usuarios = 0; @endphp
                    @foreach($usuarios as $usuario)
                        <tr class="border-bottom border-gray">
                            <td class="text-left">{{$usuario->id . ' - ' .$usuario->nome_completo }}</td>
                            <td class="text-center">{{ $usuario->email }}</td>
                            <td class="text-center"> {{ $usuario->CPF }}</td>
                            <td class="text-center"><button data-route="{{ route('cooperado.usuario.edit', $usuario->id) }}" class="btn btn-secondary btn-sm loadPage"><i class="far fa-edit" aria-hidden="true"></i></button></td>
                            <td>                
                                <div class="text-center align-intens-center" style="">
                                    <a class="delete btn border text-danger ml-1 delete-conf" title="Excluir">
                                        <i class="delete far fa-trash-alt" aria-hidden="true"></i>                        
                                    </a>
                                    <div class="delete toolip-delete">
                                        <p class="delete" style="color: #000;">Excluir?</p>
                                        <div class="delete align-intens-center ">
                                            <button type="button" class="delete btn btn-success mx-2 actionId" data-route="{{ route('cooperado.usuario.deletar', $usuario->id) }}">SIM</button>
                                            <button type="button" class="delete btn btn-danger mx-2 delete-cancel">NÃO</button>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @php $total_usuarios++; @endphp
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4" class="text-left"><strong>Quantidade total de usuários do cooperado {{ $cooperado->id . ' - '. $cooperado->nome }}</strong></td>
                        <td class="text-center"><strong>{{ $total_usuarios }}</strong></td>
                    </tr>
                </tfoot>
            </table>
            <div class="text-right">
                <button data-route="{{ route('cooperado.usuarios.form', $cooperado->id) }}" class="btn btn-primary btn-sm loadPage">NOVO USUÁRIO</button>
            </div>
        </div>
    </div>
</div>