<!DOCTYPE html>
<html lang="pt">
<head>
    @include('layouts.head')
    <style>
        .logoimg{
            max-width: 60%;
        }
    </style>
    <link rel="shortcut icon" href="{{ asset('images/icon.ico') }}">
    <title>ADM Cooperapp</title>
</head>
<body class="d-flex">    
    <div class="overlay-close-menu"></div>
    <div class="menu">     
        <nav class="nav">         
            <ul class="nav-ul">  
                <li class="w-100 p-0 d-flex justify-content-between border-empresa">
                    <a class="link link-empresa text-center text-4 pl-2" href="{{ url('/') }}">
                        <img class="logoimg" src="https://uploaddeimagens.com.br/images/003/188/376/full/logo_cooperita.png?1617972627" alt="">
                    </a>
        
                    <button class="mobile-menu-btn-2">&times;</button>
                </li>      
                <li class="w-100 p-0">
                    <a class="link drop-link px-2" href="#"><i class="fa fa-user" aria-hidden="true"></i> Cooperados
                        <span class="arrow"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
                    </a>
                    <ul class="w-100 drop-menu">
                        <li><a href="{{ route('cooperado.cadastrar') }}" class="link-append"><i class="fa fa-circle text-1" aria-hidden="true"></i> Cadastrar</a></li>
                        <li><a href="{{ route('cooperado.manter') }}" class="link-append"><i class="fa fa-circle text-1" aria-hidden="true"></i> Manter</a></li>
                    </ul>
                </li>  
                <li class="w-100 p-0">
                    <a class="link drop-link px-2" href="#"><i class="fa fa-user" aria-hidden="true"></i> Agentes
                        <span class="arrow"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
                    </a>
                    <ul class="w-100 drop-menu">
                        <li><a href="{{ route('agente.cadastrar') }}" class="link-append"><i class="fa fa-circle text-1" aria-hidden="true"></i> Cadastrar</a></li>
                        <li><a href="{{ route('agente.manter') }}" class="link-append"><i class="fa fa-circle text-1" aria-hidden="true"></i> Manter</a></li>
                    </ul>
                </li>  
                <li class="w-100 p-0">
                    <a class="link drop-link px-2" href="#"><i class="fa fa-route" aria-hidden="true"></i> Linhas
                        <span class="arrow"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
                    </a>
                    <ul class="w-100 drop-menu">
                        <li><a href="{{ route('linhas.form') }}" class="link-append"><i class="fa fa-circle text-1" aria-hidden="true"></i> Cadastrar</a></li>
                        <li><a href="{{ route('linhas.index') }}" class="link-append"><i class="fa fa-circle text-1" aria-hidden="true"></i> Manter</a></li>
                    </ul>
                </li>
                {{-- <li class="w-100 p-0">
                    <a class="link drop-link px-2" href="#"><i class="fas fa-ticket-alt"></i> Passagens
                        <span class="arrow"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
                    </a>
                    <ul class="w-100 drop-menu">
                        <li><a href="/" class="link-append"><i class="fa fa-circle text-1" aria-hidden="true"></i> Dados</a></li>
                    </ul>
                </li> --}}
                <li class="w-100 p-0">
                    <a class="link drop-link px-2" href="#"><i class="far fa-file-alt"></i> Relatórios de Passagens
                        <span class="arrow"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
                    </a>
                    <ul class="w-100 drop-menu">
                        <li><a href="{{ route('relatorio.cooperado.index') }}" class="link-append"><i class="fa fa-circle text-1" aria-hidden="true"></i> Cooperados</a></li>
                        <li><a href="{{ route('relatorio.linha.index') }}" class="link-append"><i class="fa fa-circle text-1" aria-hidden="true"></i> Linhas</a></li>
                        <li><a href="{{ route('relatorio.agente.index') }}" class="link-append"><i class="fa fa-circle text-1" aria-hidden="true"></i> Agentes</a></li>
                    </ul>
                </li>
                <li class="w-100 p-0">
                    <a class="link drop-link px-2" href="#"><i class="far fa-file-alt"></i> Relatórios de Pagamentos
                        <span class="arrow"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
                    </a>
                    <ul class="w-100 drop-menu">
                        <li><a href="{{ route('relatorio.cooperado.pagamento.index') }}" class="link-append"><i class="fa fa-circle text-1" aria-hidden="true"></i> Cooperados</a></li>
                        <li><a href="{{ route('relatorio.agente.pagamento.index') }}" class="link-append"><i class="fa fa-circle text-1" aria-hidden="true"></i> Agentes</a></li>
                    </ul>
                </li>
        
                <li class="w-100 p-0">
                    <a class="link drop-link px-2" href="#"><i class="fa fa-cog" aria-hidden="true"></i> Configurações
                        <span class="arrow"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
                    </a>
                    <ul class="w-100 drop-menu">
                        {{-- <li><a href="/" class="link-append"><i class="fa fa-circle text-1" aria-hidden="true"></i> Empresa</a></li>    --}}
                        <li><a href="{{ route('users.index') }}" class="link-append"><i class="fa fa-circle text-1" aria-hidden="true"></i> Usuários administrativos</a></li>   
                        <li><a href="{{ route('pontos.index') }}" class="link-append"><i class="fa fa-circle text-1" aria-hidden="true"></i> Pontos</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
    <div class="col grid-container p-0">
        <div class="header">  
            <div class="w-100 d-flex justify-content-between">                
                <button class="btn-menu" data-click-state="1" id="btn-menu">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                    <i class="fas fa-times" aria-hidden="true"></i>
                </button> 
                <div class="logo">
                    <a href="{{ url('/') }}">
                        <h3>cooperapp admin</h3>
                    </a>
                </div>
                <div class="d-flex justify-content-between">
                    <div class="dropdwon">
                        <button class="dropdown-toggle btn-header" id="btn-user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user-circle"></i>
                            <span class="user-name">{{ explode(" ", Auth::user()->nome_completo)[0] }}</span>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="btn-user">
                            <a class="dropdown-item loadPage" data-route="/">Dados</a>
                            <a class="dropdown-item" href="{{ route('admin.logout') }}">Sair</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>        

        <div class="main" id="main">
            @yield('content') 
            <div class="w-100 text-center mt-5">
            </div>
        </div>

        <footer class="footer"> 
            <span class="text-center">©{{ date('Y') }} | Cooperapp - Todos os direitos reservados.</span>
            @include('layouts.script')           
            @yield('script')
        </footer>  
    </div>
    <div class="modal" id="modal_ajax_load" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 2147483647 !important;">
        <div class="modal-dialog modal-dialog-centered modal-sm" role="document"> 
            <div class="modal-body align-intens-center d-flex">   
                <div class="lds-ring"><div></div><div></div><div></div><div></div></div> <!-- animação ajax -->  
            </div>   
        </div>
    </div>

    <div class="modal pb-5" id="modal_success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h1 class="text-center w-100"></h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <p></p>
                <button type="button" id="close-modal" class="btn btn-info" data-dismiss="modal">OK</button>
            </div>
            </div>
        </div>
    </div>
</body>
</html>