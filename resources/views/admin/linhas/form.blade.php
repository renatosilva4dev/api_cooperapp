<div class="main-area" id="nova_linha">
    <h1 class="text-center mb-4 border-blue-b">Nova Linha</h1>
    <form action="{{ route('linhas.save') }}" id="form_cad_linha" method="POST" class="w-100 mx-0 row justify-content-center text-2">
        @csrf
        <div class="col-md-6">
            <div class="form-group">
                <label for="nome">Nome</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <input type="text" id="nome" name="nome" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="nome">Grupo</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <input type="text" id="grupo" name="grupo" class="form-control" required>
            </div>
            <div class="form-group mt-2">
                <button type="submit" id="exception" class="btn btn-md btn-primary btn-block">SALVAR</button>
            </div>
        </div>
    </form>
</div>