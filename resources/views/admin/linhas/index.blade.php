<div class="main-area">
    <div  class="w-100 rellot">
        <div class="w-100 d-flex justify-content-between align-items-center">
        </div>
        <div class="w-100 border-bottom mb-4">
            <p class="m-0"><em style="font-size: .9em;">Linhas em {{ date('d/m/Y') }}</em></p>
        </div>
        <div class="w-100">
            <table class="my-table mb-5">
                <thead>
                    <tr>
                        <th class="text-left">ID</th>
                        <th class="text-center">Linha</th>
                        <th class="text-center">Grupo</th>
                        <th class="text-center">Seccionamentos</th>
                        <th class="text-center">Editar</th>
                        <th class="text-center">Excluir</th>
                    </tr>
                </thead>
                <tbody>
                    @php $total_linhas = 0; @endphp
                    @foreach($linhas as $linha)
                        <tr class="border-bottom border-gray">
                            <td class="text-left">{{$linha->id }}</td>
                            <td class="text-center">{{ $linha->nome_linha }}</td>
                            <td class="text-center">{{ $linha->grupo }}</td>
                            <td class="text-center"><button data-route="{{ route('linhas.seccionamentos', $linha->id) }}" class="btn btn-primary btn-sm loadPage"><i class="fas fa-route"></i></button></td>
                            <td class="text-center"><button data-route="{{ route('linhas.edit', $linha->id) }}" class="btn btn-secondary btn-sm loadPage"><i class="far fa-edit" aria-hidden="true"></i></button></td>
                            <td>                
                                <div class="text-center align-intens-center" style="">
                                    <a class="delete btn border text-danger ml-1 delete-conf" title="Excluir">
                                        <i class="delete far fa-trash-alt" aria-hidden="true"></i>                        
                                    </a>
                                    <div class="delete toolip-delete">
                                        <p class="delete" style="color: #000;">Excluir?</p>
                                        <div class="delete align-intens-center ">
                                            <button type="button" class="delete btn btn-success mx-2 actionId" data-route="{{ route('linhas.deletar', $linha->id) }}">SIM</button>
                                            <button type="button" class="delete btn btn-danger mx-2 delete-cancel">NÃO</button>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @php $total_linhas++; @endphp
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5" class="text-left"><strong>Quantidade total de linhas</strong></td>
                        <td class="text-center"><strong>{{ $total_linhas }}</strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>