<div class="main-area" id="nova_linha">
    <h1 class="text-center mb-4 border-blue-b">Editar seccionamento</h1>
    <form action="{{ route('linhas.seccionamento.update') }}" id="form_cad_linha" method="POST" class="w-100 mx-0 row justify-content-center text-2">
        @csrf
        <div class="col-md-6">
            <div class="form-group">
                <label for="ponto_saida">Ponto Saida</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <select name="ponto_saida" id="ponto_saida" class="form-control">
                    @foreach ($pontos as $ponto)
                        @if ($ponto->id == $seccionamento->id_ponto_saida)
                            <option value={{ $ponto->id }} selected>{{ $ponto->nome }}</option>
                        @else
                            <option value={{ $ponto->id }}>{{ $ponto->nome }}</option>
                        @endif
                    @endforeach
                </select>
            </div> 
            <div class="form-group">
                <label for="ponto_chegada">Ponto Chegada</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <select name="ponto_chegada" id="ponto_chegada" class="form-control">
                    @foreach ($pontos as $ponto)
                        @if ($ponto->id == $seccionamento->id_ponto_chegada)
                            <option value={{ $ponto->id }} selected>{{ $ponto->nome }}</option>
                        @else
                            <option value={{ $ponto->id }}>{{ $ponto->nome }}</option>
                        @endif
                    @endforeach
                </select>
            </div> 
            <div class="form-group">
                <label for="sentido">Sentido</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <select name="sentido" id="sentido" class="form-control">
                        @if ($seccionamento->sentido == "IDA")
                            <option value='IDA' selected>IDA</option>
                            <option value='VOLTA'>VOLTA</option>
                        @elseif ($seccionamento->sentido == "VOLTA")
                            <option value='IDA'>IDA</option>
                            <option value='VOLTA' selected>VOLTA</option>
                        @endif
                </select>
            </div> 
            <div class="form-group">
                <label for="valor_passagem">Valor da passagem</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <input type="text" class="form-control" name="valor_passagem" id="valor_passagem" value="{{ $seccionamento->valor_passagem }}" required>
            </div>
            <input type="hidden" name="seccionamento_id" value={{$seccionamento->id}}>
            <div class="form-group mt-2">
                <button type="submit" id="exception" class="btn btn-md btn-primary btn-block">SALVAR</button>
            </div>
        </div>
    </form>
</div>