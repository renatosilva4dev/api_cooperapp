<div class="main-area" id="nova_linha">
    <h1 class="text-center mb-4 border-blue-b">Novo seccionamento - Linha {{$linha->nome}}</h1>
    <form action="{{ route('linhas.seccionamentos.save') }}" id="form_cad_linha" method="POST" class="w-100 mx-0 row justify-content-center text-2">
        @csrf
        <div class="col-md-6">
            <div class="form-group">
                <label for="ponto_saida">Ponto Saida</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <select name="ponto_saida" id="ponto_saida" class="form-control">
                    @foreach ($pontos as $ponto)
                        <option value={{ $ponto->id }}>{{ $ponto->nome }}</option>
                    @endforeach
                </select>
            </div> 
            <div class="form-group">
                <label for="ponto_chegada">Ponto Chegada</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <select name="ponto_chegada" id="ponto_chegada" class="form-control">
                    @foreach ($pontos as $ponto)
                        <option value={{ $ponto->id }}>{{ $ponto->nome }}</option>
                    @endforeach
                </select>
            </div> 
            <div class="form-group">
                <label for="sentido">Sentido</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <select name="sentido" id="sentido" class="form-control">
                        <option value='IDA'>IDA</option>
                        <option value='VOLTA'>VOLTA</option>
                </select>
            </div> 
            <div class="form-group">
                <label for="valor_passagem">Valor da passagem</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <input type="text" class="form-control" name="valor_passagem" id="valor_passagem" required>
            </div>
            <input type="hidden" name="linha_id" value={{$linha->id}}>
            <div class="form-group mt-2">
                <button type="submit" id="exception" class="btn btn-md btn-primary btn-block">SALVAR</button>
            </div>
        </div>
    </form>
</div>