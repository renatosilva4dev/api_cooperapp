<div class="main-area">
    <div  class="w-100 rellot">
        <div class="w-100 d-flex justify-content-between align-items-center">
        </div>
        <div class="w-100">
            <table class="my-table mb-5">
                <thead>
                    <tr>
                        <th class="text-center">Linha</th>
                        <th class="text-center">Ponto Saída</th>
                        <th class="text-center">Ponto Chegada</th>
                        <th class="text-center">Sentido</th>
                        <th class="text-center text-success">Valor Passagem</th>
                        <th class="text-center">Editar</th>
                        <th class="text-center">Excluir</th>
                    </tr>
                </thead>
                <tbody>
                    @php $total_seccionamentos = 0; @endphp
                    @foreach($seccionamentos as $seccionamento)
                        <tr class="border-bottom border-gray">
                            <td class="text-center">{{$seccionamento->linha->nome_linha }}</td>
                            <td class="text-center">{{$seccionamento->ponto_saida->nome }}</td>
                            <td class="text-center">{{$seccionamento->ponto_chegada->nome }}</td>
                            <td class="text-center">{{$seccionamento->sentido }}</td>
                            <td class="text-center text-success"> {{ $seccionamento->valor_passagem }}</td>
                            <td class="text-center"><button data-route="{{ route('linhas.seccionamento.edit', $seccionamento->id) }}" class="btn btn-secondary btn-sm loadPage"><i class="far fa-edit" aria-hidden="true"></i></button></td>
                            <td>                
                                <div class="text-center align-intens-center" style="">
                                    <a class="delete btn border text-danger ml-1 delete-conf" title="Excluir">
                                        <i class="delete far fa-trash-alt" aria-hidden="true"></i>                        
                                    </a>
                                    <div class="delete toolip-delete">
                                        <p class="delete" style="color: #000;">Excluir?</p>
                                        <div class="delete align-intens-center ">
                                            <button type="button" class="delete btn btn-success mx-2 actionId" data-route="{{ route('linhas.seccionamento.deletar', $seccionamento->id) }}">SIM</button>
                                            <button type="button" class="delete btn btn-danger mx-2 delete-cancel">NÃO</button>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @php $total_seccionamentos++; @endphp
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6" class="text-left"><strong>Quantidade total de seccionamentos da linha {{ $linha->nome_linha }}</strong></td>
                        <td class="text-center"><strong>{{ $total_seccionamentos }}</strong></td>
                    </tr>
                </tfoot>
            </table>
            <div class="text-right">
                <button data-route="{{ route('linhas.seccionamentos.form', $linha->id) }}" class="btn btn-primary btn-sm loadPage">NOVO SECCIONAMENTO</button>
            </div>
        </div>
    </div>
</div>