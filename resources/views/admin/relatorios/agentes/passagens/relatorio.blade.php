<div class="main-area">
    <div  class="w-100 rellot">
        <div class="w-100 d-flex justify-content-between align-items-center">
            <div class="rellot-head">
                @if ($agente == "todos")
                    <p><strong>Agente: </strong>Todos</p> 
                @else
                    <p><strong>Agente: </strong>{{ $agente->id . ' - ' .$agente->nome }}</p> 
                @endif
                <p><strong>Data: </strong>{{ $data_in .' - '. $data_fi  }}</p>                
            </div>
        </div>
        <div class="w-100 border-bottom mb-4">
            <p class="m-0"><em style="font-size: .9em;">{{ $tipo }} em {{ date('d/m/Y') }}</em></p>
        </div>
        <div class="w-100">
            <table class="my-table mb-5">
                <thead>
                    <tr>
                        <th>Passagens por agente</th>
                    </tr>
                    <tr>
                        <th class="text-left">Agente</th>
                        <th class="text-center">Rota</th>
                        <th class="text-center">Data</th>
                        <th class="text-right">Valor</th>
                    </tr>
                </thead>
                <tbody>
                    @php 
                        $totalPassagens = 0; 
                        $valorTotalVendido = 0;
                    @endphp
                    @foreach($passagens as $passagem)
                        <tr class="border-bottom border-gray">
                            <td class="text-left">{{ $passagem->agente->id .' - ' .$passagem->agente->nome }}</td>
                            <td class="text-center">{{ $passagem->nome_passagem }}</td>
                            @if ($tipoData == "emissao")
                                <td class="text-center">{{ date("d/m/Y", strtotime($passagem->emissao)) }}</td> 
                            @else
                                <td class="text-center">{{ date("d/m/Y", strtotime($passagem->data_viagem)) }}</td> 
                            @endif
                            <td class="text-right text-success">{{ number_format($passagem->valor, 2, ',', '.') }}</td>
                        </tr>
                        @php 
                            $totalPassagens++; 
                            $valorTotalVendido = $valorTotalVendido + $passagem->valor;
                        @endphp
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" class="text-left"><strong>Total de passagens</strong></td>
                        <td class="text-right text-secondary"><strong>{{ $totalPassagens }}</strong></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-left"><strong>Valor total das passagens</strong></td>
                        <td class="text-right text-success"><strong>{{ number_format($valorTotalVendido, 2, ',', '.') }}</strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>