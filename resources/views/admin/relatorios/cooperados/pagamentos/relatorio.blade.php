<div class="main-area">
    <div  class="w-100 rellot">
        <div class="w-100 d-flex justify-content-between align-items-center">
            <div class="rellot-head">
                @if ($cooperado == "todos")
                    <p><strong>Cooperado: </strong>Todos</p> 
                @else
                    <p><strong>Cooperado: </strong>{{ $cooperado->id . ' - ' .$cooperado->nome }}</p> 
                @endif
                <p><strong>Data: </strong>{{ $data_in .' - '. $data_fi  }}</p>                
            </div>
        </div>
        <div class="w-100 border-bottom mb-4">
            <p class="m-0"><em style="font-size: .9em;">{{ $tipo }} em {{ date('d/m/Y') }}</em></p>
        </div>
        <div class="w-100">
            <table class="my-table mb-5">
                <thead>
                    <tr>
                        <th>Pagamento por cooperado</th>
                    </tr>
                    <tr>
                        <th class="text-left">Cooperado</th>
                        <th class="text-center">Data</th>
                        <th class="text-right">Valor</th>
                    </tr>
                </thead>
                <tbody>
                    @php 
                        $totalPagamentos = 0; 
                        $valorTotalPago= 0;
                    @endphp
                    @foreach($pagamentos as $pagamento)
                        <tr class="border-bottom border-gray">
                            <td class="text-left">{{ $pagamento->cooperado->id .' - ' .$pagamento->cooperado->nome }}</td>
                            <td class="text-center">{{ date("d/m/Y", strtotime($pagamento->data)) }}</td>
                            <td class="text-right text-success">{{ number_format($pagamento->valor, 2, ',', '.') }}</td>
                        </tr>
                        @php 
                            $totalPagamentos++; 
                            $valorTotalPago = $valorTotalPago + $pagamento->valor;
                        @endphp
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" class="text-left"><strong>Total de pagamentos</strong></td>
                        <td class="text-right text-secondary"><strong>{{ $totalPagamentos }}</strong></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-left"><strong>Valor total dos pagamentos</strong></td>
                        <td class="text-right text-success"><strong>{{ number_format($valorTotalPago, 2, ',', '.') }}</strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>