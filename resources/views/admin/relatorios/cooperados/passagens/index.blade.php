<div class="main-area" id="relatorio_cooperado">
    <h1 class="text-center mb-1 border-blue-b">Relatórios de passagens - cooperados</h1>
    <div class="w-100 mx-0 row justify-content-center mt-1">
        <div class="col-12 p-0">
            <form action="{{ route('relatorio.cooperado.consulta') }}" class="form-row justify-content-between border rounded m-0 p-2 form-rellot" method="POST" id="form_relatorio_filial">
                @csrf
                <div class="col-md-3 form-group mb-0">
                    <label for="cooperado_id">Cooperado</label> <i class="fas fa-asterisk text-danger text-1"></i>
                    <select class="form-control addPicker" data-live-search="true" id="cooperado_id" name="cooperado_id">
                        <option value="0">Todos</option>
                        @foreach($cooperados as $cooperado)
                        <option value="{{ $cooperado->id }}">{{ $cooperado->id }} - {{ $cooperado->nome }}</option>
                        @endforeach
                    </select>  
                </div> 

                <div class="col-md-3 form-group mb-0">
                    <label for="data_in">Data inicial</label> <i class="fas fa-asterisk text-danger text-1"></i>
                    <div class="input-group" id="data_in"> 
                        <input type="text" class="form-control" id="data_in_cal" name="data_in" required>
                        <div class="input-group-append">
                            <label class="input-group-text pointer" for="data_in_cal"><i class="far fa-calendar-alt" aria-hidden="true"></i></label>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 form-group mb-0">
                    <label for="data_fi">Data final</label> <i class="fas fa-asterisk text-danger text-1"></i>
                    <div class="input-group" id="data_fi"> 
                        <input type="text" class="form-control" id="data_fi_cal" name="data_fi" required>
                        <div class="input-group-append">
                            <label class="input-group-text pointer" for="data_fi_cal"><i class="far fa-calendar-alt" aria-hidden="true"></i></label>
                        </div>
                    </div> 
                </div> 
                <div class="col form-group d-flex align-items-end justify-content-end my-3">
                    <button id="submit-rellot" type="submit" class="btn btn-md btn-primary">CONSULTAR</button>
                </div>

                <div class="col-md-8 form-group mb-0" id="type-date">
                    <div class="form-group m-0 type-date">
                        <label for="tipo">Pequisar por</label> <i class="fas fa-asterisk text-danger text-1"></i> 
                    </div> 
                    <div class="d-flex" id="exception">         
                        <div class="form-check form-check-inline  p-0 type-date">
                            <input class="form-check-input" type="radio" value="1" id="data_viagem" name="data_ref" checked>
                            <label class="form-check-label" for="data_viagem">
                                Data da viagem
                            </label>
                        </div>
                        <div class="form-check  form-check-inline p-0 type-date" id="div-vencimento" >
                            <input class="form-check-input" type="radio" value="2" id="data_emissao" name="data_ref">
                            <label class="form-check-label" for="data_emissao">
                                Data da emissão
                            </label>
                        </div>   
                    </div>   
                </div> 
                
            </form>   
        </div>

        <div class="col-12 mt-2 px-0">
            <div class="w-100 border rounded" style="position: relative; height: 50vh;">
                <div class="align-intens-center loading" style="display: none;">  
                    <div class="lds-ring"><div></div><div></div><div></div><div></div></div> <!-- animação ajax -->
                </div> 
                <div class="w-100 load-rellot" style="position: absolute; height: 50vh; top: 0; overflow: auto;">

                </div>
            </div>
        </div>
        <div class="col-12 mt-2 text-right px-0">
            <button class="btn btn-md btn-secondary print">IMPRIMIR</button>
        </div>
    </div>
</div>