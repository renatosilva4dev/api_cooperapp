<div class="main-area" id="nova_linha">
    <h1 class="text-center mb-4 border-blue-b">Novo usuário Administrativo</h1>
    <form action="{{ route('users.save') }}" id="form_cad_linha" method="POST" class="w-100 mx-0 row justify-content-center text-2">
        @csrf
        <div class="col-md-6">
            <div class="form-group">
                <label for="nome">Nome</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <input type="text" id="nome" name="nome" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="telefone">Email</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <input type="text" class="form-control" name="email" id="email" required>
            </div>
            <div class="form-group">
                <label for="telefone">CPF</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <input type="text" class="form-control cpf" name="cpf" id="cpf" required>
            </div>
            <div class="form-group">
                <label for="telefone">Senha de Acesso</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <input type="password" class="form-control" name="senha" id="senha" required>
            </div>
            <div class="form-group">
                <label for="telefone">Confirme a senha</label> <i class="fas fa-asterisk text-danger text-1"></i>
                <input type="password" class="form-control" name="senha_confirm" id="senha_confirm" required>
            </div>
            <div class="form-group mt-2">
                <button type="submit" id="exception" class="btn btn-md btn-primary btn-block">SALVAR</button>
            </div>
        </div>
    </form>
</div>