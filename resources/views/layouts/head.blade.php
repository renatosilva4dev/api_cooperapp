<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Fonts -->
<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
<!-- Styles -->
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap-select.css') }}" rel="stylesheet">
<link href="{{ asset('css/helpers.css') }}" rel="stylesheet">
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/print.css') }}" rel="stylesheet">
<link href="{{ asset('css/menu.css') }}" rel="stylesheet">
<link href="{{ asset('css/crop.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/oficio.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.Jcrop.css') }}">
<link href="{{ asset('css/datepicker/jquery-ui.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css">
<link rel="icon" href="{{ asset('img/logo-coopersoft-fav.png') }}" sizes="256x256">