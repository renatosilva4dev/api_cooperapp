<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="{{ asset('js/bootstrap.js') }}" defer></script>
<script src="{{ asset('js/jquery.canvasjs.min.js') }}" defer></script>
<script src="{{ asset('js/jquery.maskinput.js') }}" defer></script>
<script src="{{ asset('js/datepicker/jquery-ui.min.js') }}" defer></script>
<script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js" defer></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js" defer></script>
<script src="{{ asset('js/bootstrap-select.js') }}" defer></script>
<script src="{{ asset('js/plugin.js') }}" defer></script>
<script src="{{ asset('js/config.js') }}" defer></script>
<script src="{{ asset('js/jquery.Jcrop.min.js') }}" defer></script>
<script src="{{ asset('js/crop.js') }}" defer></script>
<script src="{{ asset('js/printThis.js') }}" defer></script>
<script src="{{ asset('tinymce/tinymce.min.js') }}" type="text/javascript" defer></script>
<script src="{{ asset('js/editor.js') }}" defer></script>