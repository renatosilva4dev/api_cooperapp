<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <style>
        .registration-form .form-icon-sucess{
            text-align: center;
            background-color: white;
            border-radius: 50%;
            font-size: 40px;
            color: #196432;
            width: 80px;
            height: 80px;
            margin: auto;
            margin-bottom: 20px;
            line-height: 80px;
        }
        .centered {
            position: fixed;
            top: 50%;
            left: 40%;
            margin-top: -50px;
            margin-left: -100px;
        }
    </style>
</head>
    <body style="background-color:#196432; color: white">
        <div class="registration-form centered">
        <div class="form-icon-sucess">
            <span><i class="fas fa-check"></i></span> 
        </div>
        <h3>Passagem confirmada</h3>
        <center><h6>Retorne ao inicio</h6></center>
    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script src="assets/js/script.js"></script>
    </body>
</html>
