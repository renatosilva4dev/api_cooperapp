<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
    <script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#data_viagem" ).datepicker({
                minDate : 0,
                dateFormat: 'dd/mm/yy',
                closeText:"Fechar",
                prevText:"&#x3C;Anterior",
                nextText:"Próximo&#x3E;",
                currentText:"Hoje",
                monthNames: ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
                monthNamesShort:["Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"],
                    dayNames:["Domingo","Segunda-feira","Terça-feira","Quarta-feira","Quinta-feira","Sexta-feira","Sábado"],
                    dayNamesShort:["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
                dayNamesMin:["D","S","T","Q","Q","S","S"],
                weekHeader:"Sm",
                firstDay:1
            });
        });
    </script>
    <style>
      body{
        background-color:#1a237e;
      }

      .registration-form{
        padding: 50px 0;
      }

      .registration-form form{
          background-color: #fff;
          max-width: 90%;
          margin: auto;
          padding: 50px 70px;
          border-top-left-radius: 30px;
          border-top-right-radius: 30px;
          box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075);
      }

      .registration-form .form-icon{
        text-align: center;
          background-color: #1a237e;
          border-radius: 50%;
          font-size: 40px;
          color: white;
          width: 80px;
          height: 80px;
          margin: auto;
          margin-bottom: 50px;
          line-height: 80px;
      }

      .registration-form .item{
        border-radius: 20px;
          margin-bottom: 25px;
          padding: 10px 20px;
      }

      .registration-form .select{
        height: 45px !important;
        border-radius: 20px;
          margin-bottom: 25px;
          padding: 10px 20px;
      }

      .registration-form .create-account{
          border-radius: 30px;
          padding: 10px 20px;
          font-size: 18px;
          font-weight: bold;
          background-color: #1a237e;
          border: none;
          color: white;
          margin-top: 20px;
      }

      .registration-form .social-media{
          max-width: 600px;
          background-color: #fff;
          margin: auto;
          padding: 35px 0;
          text-align: center;
          border-bottom-left-radius: 30px;
          border-bottom-right-radius: 30px;
          color: #9fadca;
          border-top: 1px solid #dee9ff;
          box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075);
      }

      .registration-form .social-icons{
          margin-top: 30px;
          margin-bottom: 16px;
      }

      .registration-form .social-icons a{
          font-size: 23px;
          margin: 0 3px;
          color: #5691ff;
          border: 1px solid;
          border-radius: 50%;
          width: 45px;
          display: inline-block;
          height: 45px;
          text-align: center;
          background-color: #fff;
          line-height: 45px;
      }

      .registration-form .social-icons a:hover{
          text-decoration: none;
          opacity: 0.6;
      }

      @media (max-width: 576px) {
          .registration-form form{
              padding: 50px 20px;
          }

          .registration-form .form-icon{
              width: 70px;
              height: 70px;
              font-size: 30px;
              line-height: 70px;
          }
      }
    </style>
</head>
<body>
    <div class="registration-form">
        <form action="{{ route('passagem.inicio') }}" method="POST">
            @csrf
            <div class="form-icon">
                <span><i class="fas fa-ticket-alt"></i></span> 
            </div>
            <h5>INFORMAÇÕES PESSOAIS</h5><br>
            <div class="form-group">
                <input type="text" value="{{ $user->nome_completo}}" oninput="this.value = this.value.toUpperCase()" class="form-control item" name="nome_completo" id="nome_completo" placeholder="NOME" required>
            </div>
            <div class="form-group">
                <input type="text" value="{{ $user->CPF }}" class="form-control item" name="doc_passageiro" id="doc_passageiro" placeholder="N° DOCUMENTO" required>
            </div>
            <h6>ORIGEM - DESTINO</h6>
            <div class="form-group">
              <select name="seccionamento_id" id="seccionamento" class="form-control select">
                @foreach ($linhas as $linha)
                    <optgroup label={{$linha->nome_linha}}>
                    {{ $seccionamentos = $linha->seccionamentos }}
                    @foreach ($seccionamentos as $seccionamento)
                        <option value="{{ $seccionamento->id }}">{{ $seccionamento->ponto_saida->nome . ' - '. $seccionamento->ponto_chegada->nome }}</option>
                    @endforeach
                @endforeach
              </select>
            </div>
            <input type="hidden" name="user_id" value={{ $user->id }}>
            <input type="hidden" name="api_token" value={{ $user->api_token }}>
            <h6>DATA DA VIAGEM</h6>
            <div class="form-group">
                <input type="text" name="data_viagem" id="data_viagem" class="form-control item" placeholder="DD/MM/AAAA">
            </div>
            <div class="form-check form-switch">
                <input class="form-check-input" class="form-control item" type="checkbox" id="flexSwitchCheckDefault" name="meia"> 
                <label class="form-check-label" for="flexSwitchCheckDefault">Meia passagem</label>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-block create-account">Avançar</button>
            </div>
        </form>
    </div>
</body>
</html>
