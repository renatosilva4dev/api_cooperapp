<!DOCTYPE html>
<html>
    <?php
        // Configura credenciais
        MercadoPago\SDK::setAccessToken('APP_USR-6696593648823568-022318-97164e8025eb819aebb2241409f8dbe5-719432533');

        // Cria um objeto de preferência
        $preference = new MercadoPago\Preference();

        // Cria um item na preferência
        $item = new MercadoPago\Item();
        $item->title = 'RECARGA COOPERITA';
        $item->quantity = 1;
        $item->unit_price = $recarga->valor;
        $preference->items = array($item);
        $preference->back_urls = array(
            "success" => "http://api.cooperita.com.br/api/recarga/status?recarga_id=" . $recarga->id . "&api_token=". $user->api_token,
            "failure" => "http://api.cooperita.com.br/api/recarga/status?api_token=". $user->api_token,
            "pending" => "http://api.cooperita.com.br/api/recarga/status?recarga_id=" . $recarga->id . "&api_token=". $user->api_token
        );
        $preference->payment_methods = array(
            "excluded_payment_types" => array(
                array("id" => "ticket")
            )
        );
        $preference->binary_mode = true;
        $preference->save();
        
    ?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <style>
      body{
        background-color:#1a237e;
      }

      .registration-form{
        padding: 50px 0;
      }

      .registration-form form{
          background-color: #fff;
          max-width: 90%;
          margin: auto;
          padding: 50px 70px;
          border-top-left-radius: 30px;
          border-top-right-radius: 30px;
          box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075);
      }

      .registration-form .form-icon{
        text-align: center;
          background-color: #1a237e;
          border-radius: 50%;
          font-size: 40px;
          color: white;
          width: 80px;
          height: 80px;
          margin: auto;
          margin-bottom: 50px;
          line-height: 80px;
      }

      .registration-form .item{
        border-radius: 20px;
          margin-bottom: 25px;
          padding: 10px 20px;
      }

      .registration-form .select{
        height: 45px !important;
        border-radius: 20px;
          margin-bottom: 25px;
          padding: 10px 20px;
      }

      .registration-form .create-account{
          border-radius: 30px;
          padding: 10px 20px;
          font-size: 18px;
          font-weight: bold;
          background-color: #1a237e;
          border: none;
          color: white;
          margin-top: 20px;
      }

      .registration-form .social-media{
          max-width: 600px;
          background-color: #fff;
          margin: auto;
          padding: 35px 0;
          text-align: center;
          border-bottom-left-radius: 30px;
          border-bottom-right-radius: 30px;
          color: #9fadca;
          border-top: 1px solid #dee9ff;
          box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075);
      }

      .registration-form .social-icons{
          margin-top: 30px;
          margin-bottom: 16px;
      }

      .registration-form .social-icons a{
          font-size: 23px;
          margin: 0 3px;
          color: #5691ff;
          border: 1px solid;
          border-radius: 50%;
          width: 45px;
          display: inline-block;
          height: 45px;
          text-align: center;
          background-color: #fff;
          line-height: 45px;
      }

      .registration-form .social-icons a:hover{
          text-decoration: none;
          opacity: 0.6;
      }

      @media (max-width: 576px) {
          .registration-form form{
              padding: 50px 20px;
          }

          .registration-form .form-icon{
              width: 70px;
              height: 70px;
              font-size: 30px;
              line-height: 70px;
          }
      }
    </style>
</head>
<body>
    <div class="registration-form">
        <form  id="compra_passagem" method="POST">
            @csrf
            <div class="form-icon">
                <span><i class="fas fa-ticket-alt"></i></span> 
            </div>
            <h6>NOME</h6>
            <span>{{ $user['nome_completo'] }}</span><br><br>
            <h6>CPF</h6>
            <span>{{ $user['CPF'] }}</span><br><br>
            <h6>VALOR DA RECARGA</h6>
            <span class="text-success font-weight-bold">R$ {{ $recarga->valor }}</span><br><br>
            <h6 class="text-danger">**IMPORTANTE: Caso deseje pagar via PIX, após a geração da chave, copie e cole a mesma e clique em voltar para o site, 
                para que assim seu pagamento seja registrado, após isso você pode realizar a transferencia normalmente no seu banco e verificar o status do 
                pagamento no aplicativo.**  </h6>
            <script
                src="https://www.mercadopago.com.br/integrations/v1/web-payment-checkout.js"
                data-preference-id="<?php echo $preference->id; ?>">
            </script>
        </form>
    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script src="assets/js/script.js"></script>
</body>
</html>
