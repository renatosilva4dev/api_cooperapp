<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
    <script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.min.js"></script>

    <script>
        var emails = '<?php echo $emails; ?>';
    

        $(function() {
            $( "#data_viagem" ).datepicker({
                dateFormat: 'dd/mm/yy',
                closeText:"Fechar",
                prevText:"&#x3C;Anterior",
                nextText:"Próximo&#x3E;",
                currentText:"Hoje",
                monthNames: ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
                monthNamesShort:["Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"],
                    dayNames:["Domingo","Segunda-feira","Terça-feira","Quarta-feira","Quinta-feira","Sexta-feira","Sábado"],
                    dayNamesShort:["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
                dayNamesMin:["D","S","T","Q","Q","S","S"],
                weekHeader:"Sm",
                firstDay:1
            });
        });

        $(document).ready(function(){
            $('.date').mask('00/00/0000');
            $('.time').mask('00:00:00');
            $('.date_time').mask('00/00/0000 00:00:00');
            $('.cep').mask('00.000-000');
            $('.phone').mask('0000-0000');
            $('.phone_with_ddd').mask('(00) 0000-0000');
            var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

            $('.sp_celphones').mask(SPMaskBehavior, spOptions);
            $('.phone_us').mask('(000) 000-0000');
            $('.mixed').mask('AAA 000-S0S');
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
            $('.money').mask('000.000.000.000.000,00', {reverse: true});
            $('.money2').mask("#.##0,00", {reverse: true});
            $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
                translation: {
                'Z': {
                    pattern: /[0-9]/, optional: true
                }
                }
            });
            $('.ip_address').mask('099.099.099.099');
            $('.percent').mask('##0,00%', {reverse: true});
            $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
            $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
            $('.fallback').mask("00r00r0000", {
                translation: {
                    'r': {
                    pattern: /[\/]/,
                    fallback: '/'
                    },
                    placeholder: "__/__/____"
                }
                });
            $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
            
        });

        $(function(){
            //Executa a requisição quando o campo username perder o foco
            $('#cpf').blur(function()
            {
                var cpf = $('#cpf').val().replace(/[^0-9]/g, '').toString();

                if( cpf.length == 11 )
                {
                    var v = [];

                    //Calcula o primeiro dígito de verificação.
                    v[0] = 1 * cpf[0] + 2 * cpf[1] + 3 * cpf[2];
                    v[0] += 4 * cpf[3] + 5 * cpf[4] + 6 * cpf[5];
                    v[0] += 7 * cpf[6] + 8 * cpf[7] + 9 * cpf[8];
                    v[0] = v[0] % 11;
                    v[0] = v[0] % 10;

                    //Calcula o segundo dígito de verificação.
                    v[1] = 1 * cpf[1] + 2 * cpf[2] + 3 * cpf[3];
                    v[1] += 4 * cpf[4] + 5 * cpf[5] + 6 * cpf[6];
                    v[1] += 7 * cpf[7] + 8 * cpf[8] + 9 * v[0];
                    v[1] = v[1] % 11;
                    v[1] = v[1] % 10;

                    //Retorna Verdadeiro se os dígitos de verificação são os esperados.
                    if ( (v[0] != cpf[9]) || (v[1] != cpf[10]) )
                    {
                        $('#messagem_cpf').text('CPF inválido');
                        $("#cpf").addClass("is-invalid");
                        $("#button").prop("disabled",true);
                    }
                    else{
                        $('#messagem_cpf').text('');
                        $("#cpf").removeClass("is-invalid");
                        $("#button").prop("disabled",false);


                    }
                }
                else
                {
                    $('#messagem_cpf').text('CPF inválido');
                    $("#cpf").addClass("is-invalid");
                    $("#button").prop("disabled",true);


                }
            });
        });

        $(function(){
            //Executa a requisição quando o campo username perder o foco
            $('#cpf').blur(function()
            {
                var cpf = $('#cpf').val().replace(/[^0-9]/g, '').toString();

                if( cpf.length == 11 )
                {
                    if ( (v[0] != cpf[9]) || (v[1] != cpf[10]) )
                    {
                        $('#messagem_cpf').text('CPF inválido');
                        $("#cpf").addClass("is-invalid");
                        $("#button").prop("disabled",true);
                    }
                    else{
                        $('#messagem_cpf').text('');
                        $("#cpf").removeClass("is-invalid");
                        $("#button").prop("disabled",false);


                    }
                }
                else
                {
                    $('#messagem_cpf').text('CPF inválido');
                    $("#cpf").addClass("is-invalid");
                    $("#button").prop("disabled",true);


                }
            });
        });

        $(function(){
            //Executa a requisição quando o campo username perder o foco
            $('#senha').blur(function()
            {
                var senha = $('#senha').val().toString();

                if( senha.length >= 6 )
                {
                    $('#messagem_senha').text('');
                    $("#senha").removeClass("is-invalid");
                    $("#button").prop("disabled",false);

                    if(!alphanumeric(senha)){
                        $('#messagem_senha').text('A senha deve conter somente letras e números.');
                        $("#senha").addClass("is-invalid");
                        $("#button").prop("disabled",true);
                    }
                    else{
                        if(!valid(senha)){
                            $('#messagem_senha').text('A senha deve conter pelo menos uma letra e um número.');
                            $("#senha").addClass("is-invalid");
                            $("#button").prop("disabled",true);
                        }
                        else{
                            $('#messagem_senha').text('');
                            $("#senha").removeClass("is-invalid");
                            $("#button").prop("disabled",false); 
                        } 
                    }
                }
                else{
                    $('#messagem_senha').text('A senha precisa ter no mínimo 6 caracteres.');
                    $("#senha").addClass("is-invalid");
                    $("#button").prop("disabled",true);
                }
            });
        });

        $(function(){
            //Executa a requisição quando o campo username perder o foco
            $('#senha_confirm').blur(function()
            {
                var senha_confirm = $('#senha_confirm').val().toString();
                var senha = $('#senha').val().toString();

                if( senha_confirm == senha )
                {
                    $('#messagem_senha_confirm').text('');
                    $("#senha_confirm").removeClass("is-invalid");
                    $("#button").prop("disabled",false);

                }
                else{
                    $('#messagem_senha_confirm').text('As senhas precisam ser iguais');
                    $("#senha_confirm").addClass("is-invalid");
                    $("#button").prop("disabled",true);
                }
            });
        });

        function alphanumeric(inputtxt){ 
            var letters = /^[0-9a-zA-Z]+$/;
            if(inputtxt.match(letters))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        function valid(inputtxt){ 
            var letters = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/;
            if(inputtxt.match(letters))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
  
    </script>
    <style>
      body{
        background-color:#1a237e;
      }

      .registration-form{
        padding: 50px 0;
      }

      .registration-form form{
          background-color: #fff;
          max-width: 90%;
          margin: auto;
          padding: 50px 70px;
          border-top-left-radius: 30px;
          border-top-right-radius: 30px;
          box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075);
      }

      .registration-form .form-icon{
        text-align: center;
          background-color: #1a237e;
          border-radius: 50%;
          font-size: 40px;
          color: white;
          width: 80px;
          height: 80px;
          margin: auto;
          margin-bottom: 50px;
          line-height: 80px;
      }

      .registration-form .item{
        border-radius: 20px;
          margin-bottom: 25px;
          padding: 10px 20px;
      }

      .registration-form .select{
        height: 45px !important;
        border-radius: 20px;
          margin-bottom: 25px;
          padding: 10px 20px;
      }

      .registration-form .create-account{
          border-radius: 30px;
          padding: 10px 20px;
          font-size: 18px;
          font-weight: bold;
          background-color: #1a237e;
          border: none;
          color: white;
          margin-top: 20px;
      }

      .registration-form .social-media{
          max-width: 600px;
          background-color: #fff;
          margin: auto;
          padding: 35px 0;
          text-align: center;
          border-bottom-left-radius: 30px;
          border-bottom-right-radius: 30px;
          color: #9fadca;
          border-top: 1px solid #dee9ff;
          box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075);
      }

      .registration-form .social-icons{
          margin-top: 30px;
          margin-bottom: 16px;
      }

      .registration-form .social-icons a{
          font-size: 23px;
          margin: 0 3px;
          color: #5691ff;
          border: 1px solid;
          border-radius: 50%;
          width: 45px;
          display: inline-block;
          height: 45px;
          text-align: center;
          background-color: #fff;
          line-height: 45px;
      }

      .registration-form .social-icons a:hover{
          text-decoration: none;
          opacity: 0.6;
      }

      @media (max-width: 576px) {
          .registration-form form{
              padding: 50px 20px;
          }

          .registration-form .form-icon{
              width: 70px;
              height: 70px;
              font-size: 30px;
              line-height: 70px;
          }
      }
    </style>
</head>
<body>
    <div class="registration-form">
        <form action="{{ route('user.save') }}" method="POST">
            @csrf
            <div class="form-icon">
                <span><i class="fa fa-user" aria-hidden="true"></i></span> 
            </div>
            <h5>INFORMAÇÕES PESSOAIS</h5><br>

            <label for="nome">NOME</label>
            <div class="form-group">
                <input type="text" oninput="this.value = this.value.toUpperCase()" autocomplete="off" class="form-control item" name="nome" id="nome" placeholder="NOME" required>
            </div>

            <label for="sobrenome">SOBRENOME</label>
            <div class="form-group">
                <input type="text" oninput="this.value = this.value.toUpperCase()" autocomplete="off" class="form-control item" name="sobrenome" id="SOBRENOME" placeholder="SOBRENOME" required>
            </div>
            <h6>DATA DE NASCIMENTO</h6>
            <div class="form-group">
                <input type="text" name="data_nascimento" id="data_nascimento" autocomplete="off" class="form-control item date" placeholder="DD/MM/AAAA" required>
            </div>
            @if($aviso_email == '0')
                <label for="email">EMAIL</label>
                <div class="form-group">
                    <input type="email" oninput="this.value = this.value.toUpperCase()" class="form-control item" name="email" id="email" placeholder="EMAIL" required>
                </div>
            @else
                <label for="email" class="text-danger">EMAIL</label><br>
                <small id="passwordHelp" class="text-danger">
                    Email já cadastrado
                </small> 
                <div class="form-group">
                    <input type="email" oninput="this.value = this.value.toUpperCase()" class="form-control item is-invalid" name="email" id="email" placeholder="EMAIL" required>  
                </div>
            @endif
            
            <label for="cpf">CPF</label><br>
            <small id="messagem_cpf" class="text-danger">
            </small> 
            <div class="form-group">
                <input type="text" class="form-control item cpf" name="cpf" id="cpf" autocomplete="off" placeholder="CPF" required>
            </div>
            <label for="telefone">TELEFONE</label>
            <div class="form-group">
                <input type="text" class="form-control item sp_celphones" name="telefone" id="telefone" placeholder="TELEFONE" required>
            </div>
            <label for="rua">RUA</label>
            <div class="form-group">
                <input type="text" oninput="this.value = this.value.toUpperCase()" class="form-control item" name="rua" id="rua" placeholder="NOME DA RUA">
            </div>
            <label for="casa">NÚMERO</label>
            <div class="form-group">
                <input type="text" oninput="this.value = this.value.toUpperCase()" class="form-control item" name="casa" id="casa" placeholder="NÚMERO DA CASA">
            </div>
            <label for="bairro">BAIRRO</label>
            <div class="form-group">
                <input type="text" oninput="this.value = this.value.toUpperCase()" class="form-control item" name="bairro" id="bairro" placeholder="BAIRRO">
            </div>
            <label for="cidade">CIDADE</label>
            <div class="form-group">
                <input type="text" oninput="this.value = this.value.toUpperCase()" class="form-control item" name="cidade" id="cidade" placeholder="CIDADE">
            </div>
            <label for="cep">CEP</label>
            <div class="form-group">
                <input type="text" class="form-control item cep" name="cep" id="cep" placeholder="00.000-000">
            </div>
            <label for="cidade">SENHA</label>
            <small id="messagem_senha" class="text-danger"></small>
            <div class="form-group">
                <input type="password" class="form-control item" name="senha" id="senha" placeholder="SENHA" required>
            </div>
            <label for="cidade">CONFIRME SUA SENHA</label>
            <small id="messagem_senha_confirm" class="text-danger"></small>
            <div class="form-group">
                <input type="password" class="form-control item" name="senha_confirm" id="senha_confirm" placeholder="CONFIRME SUA SENHA" required minlength="6" required>
            </div>

            <div class="form-check form-switch">
                <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" name="termos" required> 
                <label class="form-check-label" for="flexSwitchCheckDefault">Eu li e concordo com os Termos de Uso e privacidade.</label>
            </div>
            <br>

            <button onclick="document.getElementById('id01').style.display='block';event.preventDefault();" class="btn btn-outline-info">Termos de uso</button>

            <div id="id01" class="w3-modal">
                <div class="w3-modal-content">
                    <div class="w3-container">
                    <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                    <h2><b> Termo de Consentimento para Tratamento de Dados Pessoais</b></h2>
                        <p> Este documento visa registrar a manifestação livre, informada e inequívoca pela qual o Titular concorda com o tratamento de seus dados pessoais para finalidade específica, em conformidade com a Lei nº 13.709 – Lei Geral de Proteção de Dados Pessoais (LGPD).
                        Ao declarar que concorda com o presente termo, o Titular consente que a empresa SYSTEM TIME, CNPJ nº 12.588.034/0001-28, com sede na Rua Manoel Bandeira, 58, Parque Soledade, Caucaia, Ceará, telefone (85) 3342-0231, e-mail systemtimece@gmail.com, doravante 
                        denominada Controlador, tome decisões referentes ao tratamento de seus dados pessoais, bem como realize o tratamento de seus dados pessoais, envolvendo operações como as que se referem a coleta, produção, recepção, classificação, utilização, acesso, 
                        reprodução, transmissão, distribuição, processamento, arquivamento, armazenamento, eliminação, avaliação ou controle da informação, modificação, comunicação, transferência, difusão ou extração.</p>

                        <h4>Dados Pessoais</h4>
                        <p> Controlador fica autorizado a tomar decisões referentes ao tratamento e a realizar o tratamento dos seguintes dados pessoais do Titular: </p>
                        <ul>
                            <li>Nome completo.</li>
                            <li>Número do Cadastro de Pessoas Físicas (CPF).</li>
                            <li>Endereço completo.</li>
                            <li>Números de telefone, WhatsApp e endereços de e-mail.</li>
                            <li>Nome de usuário e senha específicos para uso dos serviços do Controlador.</li>

                        </ul>

                        <h4>Finalidades do Tratamento dos Dados</h4>
                        <p> O tratamento dos dados pessoais listados neste termo tem as seguintes finalidades:</p>
                        <ul>
                            <li>Possibilitar que o Controlador identifique e entre em contato com o Titular para fins de relacionamento comercial.</li>
                            <li>Possibilitar que o Controlador envie ou forneça ao Titular seus produtos e serviços, de forma remunerada ou gratuita.</li>
                            <li>Possibilitar que o Controlador estruture, teste, promova e faça propaganda de produtos e serviços, personalizados ou não ao perfil do Titular.</li>
                        </ul>



                        <h4>Compartilhamento de Dados</h4>
                        <p>O Controlador fica autorizado a compartilhar os dados pessoais do Titular com outros agentes de tratamento de dados, caso seja necessário para as finalidades listadas neste termo, observados os princípios e as garantias estabelecidas pela Lei nº 13.709.</p>

                        <h4>Segurança dos Dados</h4>
                        <p>
                            O Controlador responsabiliza-se pela manutenção de medidas de segurança, técnicas e administrativas aptas a proteger os dados pessoais de acessos não autorizados e de situações acidentais ou ilícitas de destruição, perda, alteração, comunicação ou qualquer forma de tratamento inadequado ou ilícito.
                            Em conformidade ao art. 48 da Lei nº 13.709, o Controlador comunicará ao Titular e à Autoridade Nacional de Proteção de Dados (ANPD) a ocorrência de incidente de segurança que possa acarretar risco ou dano relevante ao Titular.
                        </p>


                        <h4>Término do Tratamento dos Dados</h4>
                        <p>
                            O Controlador poderá manter e tratar os dados pessoais do Titular durante todo o período em que estes forem pertinentes ao alcance das finalidades listadas neste termo. Dados pessoais anonimizados, sem possibilidade de associação ao indivíduo, poderão ser mantidos por período indefinido.
                            O Titular poderá solicitar via e-mail ou correspondência ao Controlador, a qualquer momento, que sejam eliminados os dados pessoais não anonimizados do Titular. O Titular fica ciente de que poderá ser inviável ao Controlador continuar o fornecimento de produtos ou serviços ao Titular a partir da eliminação dos dados pessoais.
                        </p>


                        <h4>Direitos do Titular</h4>
                        <p>O Titular tem direito a obter do Controlador, em relação aos dados por ele tratados, a qualquer momento e mediante requisição:</p>

                        <ol>
                            <li>confirmação da existência de tratamento;</li>
                            <li>acesso aos dados;</li>
                            <li>correção de dados incompletos, inexatos ou desatualizados;</li>
                            <li>anonimização, bloqueio ou eliminação de dados desnecessários, excessivos ou tratados em desconformidade com o disposto na Lei nº 13.709;</li>
                            <li>eliminação dos dados pessoais tratados com o consentimento do titular, exceto nas hipóteses previstas no art. 16 da Lei nº 13.709; VII – informação das entidades públicas e privadas com as quais o controlador realizou uso compartilhado de dados;</li>
                            <li>informação sobre a possibilidade de não fornecer consentimento e sobre as consequências da negativa;</li>
                            <li>revogação do consentimento, nos termos do § 5º do art. 8º da Lei nº 13.709.</li>
                        </ol>


                        <h4>Direito de Revogação do Consentimento</h4>
                        <p>Este consentimento poderá ser revogado pelo Titular, a qualquer momento, mediante solicitação via e-mail ou correspondência ao Controlador.</p>
                    </div>
                </div>
                </div>

            <div class="form-group">
                <button type="submit" id="button" class="btn btn-block create-account">Cadastrar</button>
            </div>
        </form>
    </div>
</body>
</html>
