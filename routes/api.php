<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LoginEmpresaController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\RegisterCooperadoController;
use App\Http\Controllers\Auth\RegisterEmpresaController;
use App\Http\Controllers\Auth\UsuarioController;
use App\Http\Controllers\CooperadoController;
use App\Http\Controllers\EmpresaController;
use App\Http\Controllers\HorarioController;
use App\Http\Controllers\ItinerarioController;
use App\Http\Controllers\LinhasController;
use App\Http\Controllers\NoticiasController;
use App\Http\Controllers\PagamentosDiretosController;
use App\Http\Controllers\PassagensController;
use App\Http\Controllers\PontosController;
use App\Http\Controllers\RecargaController;
use App\Http\Controllers\SeccionamentoController;
use Illuminate\Support\Facades\Route;

// User
Route::post('/register', [RegisterController::class, 'register'])->name('register');
Route::post('/empresa/login', [LoginEmpresaController::class, 'login'])->name('login');


Route::post('/login', [LoginController::class, 'login'])->name('login');
Route::get('/logout', [LogoutController::class, 'logout'])->name('logout');

Route::post('/user/save', [UsuarioController::class, 'create'])->name('user.save');
Route::get('/user/cadastro', [UsuarioController::class, 'form'])->name('user.form');


Route::get('/passagem/status_pagamento/{payment_id}', [PassagensController::class, 'verify_payment'])->name('payment.check');
Route::get('/recarga/status_pagamento/{payment_id}', [RecargaController::class, 'verify_payment'])->name('payment.check');

//User


Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user/{id}', [UsuarioController::class, 'find'])->name('user.show');
    Route::get('/user', [UsuarioController::class, 'index'])->name('index');

    //Empresa
    Route::post('/empresa', [RegisterEmpresaController::class, 'register'])->name('register');
    Route::get('/empresa', [EmpresaController::class, 'index'])->name('index');

    //Cooperado
    Route::post('/cooperado', [RegisterCooperadoController::class, 'register'])->name('register');
    Route::get('/cooperado', [CooperadoController::class, 'index'])->name('index');
    Route::get('/cooperado/{id}', [CooperadoController::class, 'find'])->name('cooperado.find');

    //Pontos
    Route::get('/pontos', [PontosController::class, 'index'])->name('index');
    Route::post('/pontos', [PontosController::class, 'store'])->name('store');

    //Linhas
    Route::post('/linha', [LinhasController::class, 'store'])->name('store');
    Route::get('/linha/{id}', [LinhasController::class, 'show'])->name('show');
    Route::put('/linha/{id}', [LinhasController::class, 'update'])->name('update');
    Route::delete('/linha/{id}', [LinhasController::class, 'destroy'])->name('destroy');
    Route::get('/linhas', [LinhasController::class, 'index'])->name('index');

    //Horario
    Route::post('/horario', [HorarioController::class, 'store'])->name('store');
    Route::get('/horarios/{id}', [HorarioController::class, 'horario'])->name('index');
    Route::get('/horario/{id}', [HorarioController::class, 'view'])->name('view');


    Route::get('/noticias', [NoticiasController::class, 'index'])->name('index');
    Route::post('/noticia', [NoticiasController::class, 'store'])->name('store');
    Route::get('/noticia/{id}', [NoticiasController::class, 'show'])->name('show');
    Route::put('/noticia/{id}', [NoticiasController::class, 'update'])->name('update');
    Route::delete('/noticia/{id}', [NoticiasController::class, 'destroy'])->name('destroy');

    //PassagemByUser
    Route::post('/passagem', [PassagensController::class, 'payment'])->name('passagem.store');
    Route::get('/pagamentos/user/{id}', [PassagensController::class, 'index'])->name('index');
    Route::get('/pagamento/{id}', [PassagensController::class, 'show'])->name('show');
    Route::get('/passagem/nova/{user_id}', [PassagensController::class, 'form'])->name('form');
    Route::post('/passagem/next', [PassagensController::class, 'formstep'])->name('passagem.inicio');
    Route::get('/passagem/status/', [PassagensController::class, 'status'])->name('passagem.status');
    Route::get('/passagem/use/', [PassagensController::class, 'use'])->name('passagem.use');
    Route::get('/passagem/pagar/{id}', [PassagensController::class, 'pay_after'])->name('passagem.pagar');

    //PassagemByAgente
    Route::get('/passagem/buscar/{id}', [PassagensController::class, 'find_user'])->name('passagem.agente.buscar');
    Route::post('/passagem/agente/nova/', [PassagensController::class, 'form_agente'])->name('form.agente');
    Route::post('/passagem/agente/next', [PassagensController::class, 'formstep_agente'])->name('passagem.agente.step');
    Route::post('/passagem/agente/save', [PassagensController::class, 'payment_money'])->name('passagem.agente.store');
    Route::post('/passagem/agente/status/', [PassagensController::class, 'status_agente'])->name('passagem.agente.status');

    //PassagemComSaldo
    Route::get('/passagem/saldo/nova/{user_id}', [PassagensController::class, 'form_saldo'])->name('form.saldo');
    Route::post('/passagem/saldo/next', [PassagensController::class, 'formstep_saldo'])->name('passagem.saldo.step');
    Route::post('/passagem/saldo/save', [PassagensController::class, 'payment_saldo'])->name('passagem.saldo.store');
    Route::post('/passagem/saldo/status/', [PassagensController::class, 'status_saldo'])->name('passagem.saldo.status');

    //PassagemCashback
    Route::get('/passagem/cashback/nova/{user_id}', [PassagensController::class, 'form_cashback'])->name('form.cashback');
    Route::post('/passagem/cashback/next', [PassagensController::class, 'formstep_cashback'])->name('passagem.cashback.step');
    Route::post('/passagem/cashback/save', [PassagensController::class, 'payment_cashback'])->name('passagem.cashback.store');
    Route::post('/passagem/cashback/status/', [PassagensController::class, 'status_cashback'])->name('passagem.cashback.status');

    Route::delete('/passagem/{id}', [PassagensController::class, 'delete'])->name('passagem.delete');

    //Recarga
    Route::get('/recargas/user/{id}', [RecargaController::class, 'index_user'])->name('recarga.user.index');
    Route::get('/recarga/user/{id}', [RecargaController::class, 'index'])->name('recarga.user.index');
    Route::post('/recarga/pay', [RecargaController::class, 'pay'])->name('recarga.pagar');
    Route::get('/recarga/status/', [RecargaController::class, 'status'])->name('recarga.status');

    
    Route::get('/recarga/agente/{id}', [RecargaController::class, 'indexagencia'])->name('recarga.agente.index');
    Route::post('/recarga/recebe', [RecargaController::class, 'receiveagencia'])->name('recarga.agente.receber');
    Route::post('/recarga/agente/status/', [RecargaController::class, 'statusagencia'])->name('recarga.agente.status');

    Route::delete('/recarga/{id}', [RecargaController::class, 'delete'])->name('recarga.delete');
    
    //PagamentoDireto
    Route::get('/pagar/user/{id}', [PagamentosDiretosController::class, 'index'])->name('pagar.index');
    Route::post('/pagar/checkout', [PagamentosDiretosController::class, 'checkout'])->name('pagar.checkout');
    Route::post('/pagar/save', [PagamentosDiretosController::class, 'save'])->name('pagar.save');


    Route::post('/seccionamento', [SeccionamentoController::class, 'store'])->name('seccionamento.store');
    Route::get('/seccionamentos', [SeccionamentoController::class, 'index'])->name('seccionamento.index');

});
