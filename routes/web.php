<?php

use App\Http\Controllers\AgentesController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\UsuarioController;
use App\Http\Controllers\CooperadoController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LinhasController;
use App\Http\Controllers\PassagensController;
use App\Http\Controllers\PontosController;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\CheckLogin;

Route::get('/', [HomeController::class, 'AdminHome'])->name('admin.home')->middleware('check');

Route::get('/admin/login', [LoginController::class, 'loginAdmView'])->name('admin.form');
Route::get('/logout', [LoginController::class, 'logoutAdmin'])->name('admin.logout');

Route::post('/admin/login', [LoginController::class, 'loginAdm'])->name('admin.login')->middleware('admin');

Route::get('/cooperados', [CooperadoController::class, 'indexAdm'])->name('cooperado.manter')->middleware('check');
Route::get('/cooperados/cadastro', [CooperadoController::class, 'formcadastro'])->name('cooperado.cadastrar')->middleware('check');
Route::post('/cooperados/save', [CooperadoController::class, 'save'])->name('cooperado.save')->middleware(('check'));
Route::get('/cooperados/editar/{id}', [CooperadoController::class, 'edit'])->name('cooperado.edit')->middleware('check');
Route::post('/cooperados/update', [CooperadoController::class, 'update'])->name('cooperado.update')->middleware('check');
Route::get('/cooperados/delete/{id}', [CooperadoController::class, 'delete'])->name('cooperado.deletar')->middleware('check');
Route::get('/cooperado/usuarios/{id}', [CooperadoController::class, 'usuarios'])->name('cooperado.usuarios')->middleware('check');
Route::get('/cooperado/cadastro/usuarios/{id}', [CooperadoController::class, 'form_usuarios'])->name('cooperado.usuarios.form')->middleware('check');
Route::post('/cooperados/usuario/save', [CooperadoController::class, 'saveuser'])->name('cooperado.usuario.save')->middleware(('check'));
Route::get('/cooperados/usuario/editar/{id}', [CooperadoController::class, 'edituser'])->name('cooperado.usuario.edit')->middleware('check');
Route::post('/cooperados/usuario/update', [CooperadoController::class, 'updateuser'])->name('cooperado.usuario.update')->middleware('check');
Route::get('/cooperados/usuario/delete/{id}', [CooperadoController::class, 'deleteuser'])->name('cooperado.usuario.deletar')->middleware('check');
Route::get('/cooperados/pagar/{id}', [CooperadoController::class, 'pay'])->name('cooperado.pagar')->middleware('check');
Route::post('/cooperados/pagar', [CooperadoController::class, 'confirm_pay'])->name('cooperado.pagar.confirm')->middleware('check');

Route::get('/agentes', [AgentesController::class, 'indexAdm'])->name('agente.manter')->middleware('check');
Route::get('/agentes/cadastro', [AgentesController::class, 'formcadastro'])->name('agente.cadastrar')->middleware('check');
Route::post('/agente/save', [AgentesController::class, 'save'])->name('agente.save')->middleware(('check'));
Route::get('/agente/editar/{id}', [AgentesController::class, 'edit'])->name('agente.edit')->middleware('check');
Route::post('/agente/update', [AgentesController::class, 'update'])->name('agente.update')->middleware('check');
Route::get('/agente/delete/{id}', [AgentesController::class, 'delete'])->name('agente.deletar')->middleware('check');
Route::get('/agente/usuarios/{id}', [AgentesController::class, 'usuarios'])->name('agente.usuarios')->middleware('check');
Route::get('/agentes/cadastro/usuarios/{id}', [AgentesController::class, 'form_usuarios'])->name('agente.usuarios.form')->middleware('check');
Route::post('/agentes/usuario/save', [AgentesController::class, 'saveuser'])->name('agente.usuario.save')->middleware('check');
Route::get('/agentes/usuario/editar/{id}', [AgentesController::class, 'edituser'])->name('agente.usuario.edit')->middleware('check');
Route::post('/agente/usuario/update', [AgentesController::class, 'updateuser'])->name('agente.usuario.update')->middleware('check');
Route::get('/agente/usuario/delete/{id}', [AgentesController::class, 'deleteuser'])->name('agente.usuario.deletar')->middleware('check');
Route::get('/agentes/pagar/{id}', [AgentesController::class, 'pay'])->name('agente.pagar')->middleware('check');
Route::post('/agentes/pagar', [AgentesController::class, 'confirm_pay'])->name('agente.pagar.confirm')->middleware('check');

Route::get('/linhas', [LinhasController::class, 'indexAdm'])->name('linhas.index')->middleware('check');
Route::get('/linhas/form', [LinhasController::class, 'form'])->name('linhas.form')->middleware('check');
Route::post('/linha/save', [LinhasController::class, 'save'])->name('linhas.save')->middleware('check');
Route::get('/linhas/edit/{id}', [LinhasController::class, 'edit'])->name('linhas.edit')->middleware('check');
Route::post('/linha/update', [LinhasController::class, 'update'])->name('linhas.update')->middleware('check');
Route::get('/linha/delete/{id}', [LinhasController::class, 'delete'])->name('linhas.deletar')->middleware('check');
Route::get('/linhas/seccionamentos/{id}', [LinhasController::class, 'seccionamentos'])->name('linhas.seccionamentos')->middleware('check');
Route::get('/linhas/cadastro/seccionamentos/{id}', [LinhasController::class, 'form_seccionamentos'])->name('linhas.seccionamentos.form')->middleware('check');
Route::post('/linhas/seccionamento/save', [LinhasController::class, 'saveseccionamento'])->name('linhas.seccionamentos.save')->middleware(('check'));
Route::get('/linhas/seccionamento/editar/{id}', [LinhasController::class, 'editseccionamento'])->name('linhas.seccionamento.edit')->middleware('check');
Route::post('/linhas/seccionamento/update', [LinhasController::class, 'updateseccionamento'])->name('linhas.seccionamento.update')->middleware('check');
Route::get('/linhas/seccionamento/delete/{id}', [LinhasController::class, 'deleteseccionamento'])->name('linhas.seccionamento.deletar')->middleware('check');


Route::get('/pontos', [PontosController::class, 'indexAdm'])->name('pontos.index')->middleware('check');
Route::get('/pontos/form', [PontosController::class, 'form'])->name('pontos.form')->middleware('check');
Route::post('/pontos/save', [PontosController::class, 'save'])->name('pontos.save')->middleware('check');
Route::get('/pontos/edit/{id}', [PontosController::class, 'edit'])->name('pontos.edit')->middleware('check');
Route::post('/pontos/update', [PontosController::class, 'update'])->name('pontos.update')->middleware('check');
Route::get('/pontos/delete/{id}', [PontosController::class, 'delete'])->name('pontos.deletar')->middleware('check');

Route::get('/users', [UsuarioController::class, 'indexAdm'])->name('users.index')->middleware('check');
Route::get('/users/form', [UsuarioController::class, 'formAdm'])->name('users.form')->middleware('check');
Route::post('/users/save', [UsuarioController::class, 'saveuser'])->name('users.save')->middleware('check');
Route::get('/users/edit/{id}', [UsuarioController::class, 'edituser'])->name('user.edit')->middleware('check');
Route::post('/users/update', [UsuarioController::class, 'updateuser'])->name('user.update')->middleware('check');
Route::get('/users/delete/{id}', [UsuarioController::class, 'deleteuser'])->name('user.deletar')->middleware('check');

Route::get('/relatorios/passagens/cooperado', [PassagensController::class, 'RelatorioCooperadoIndex'])->name('relatorio.cooperado.index')->middleware('check');
Route::post('/relatorios/passagens/cooperado/consultar', [PassagensController::class, 'RelatorioCooperadoConsulta'])->name('relatorio.cooperado.consulta')->middleware('check');

Route::get('/relatorios/passagens/linhas', [PassagensController::class, 'RelatorioLinhasIndex'])->name('relatorio.linha.index')->middleware('check');
Route::post('/relatorios/passagens/linhas/consultar', [PassagensController::class, 'RelatorioLinhasConsulta'])->name('relatorio.linha.consulta')->middleware('check');

Route::get('/relatorios/passagens/agentes', [PassagensController::class, 'RelatorioAgentesIndex'])->name('relatorio.agente.index')->middleware('check');
Route::post('/relatorios/passagens/agentes/consultar', [PassagensController::class, 'RelatorioAgentesConsulta'])->name('relatorio.agente.consulta')->middleware('check');

Route::get('/relatorios/pagamentos/cooperado', [CooperadoController::class, 'RelatorioPagamenoIndex'])->name('relatorio.cooperado.pagamento.index')->middleware('check');
Route::post('/relatorios/pagamentos/cooperado/consultar', [CooperadoController::class, 'RelatorioPagamentoConsulta'])->name('relatorio.cooperado.pagamento.consulta')->middleware('check');

Route::get('/relatorios/pagamentos/agentes', [AgentesController::class, 'RelatorioPagamenoIndex'])->name('relatorio.agente.pagamento.index')->middleware('check');
Route::post('/relatorios/pagamentos/agentes/consultar', [AgentesController::class, 'RelatorioPagamentoConsulta'])->name('relatorio.agente.pagamento.consulta')->middleware('check');