<?php

namespace Tests\Feature;

use Tests\TestCase;

class LinhasTest extends TestCase
{
  public function test_shouldBeAbleToGet()
  {
    $response = $this->get('/api/linhas');

    $response->assertStatus(200);
  }
}
